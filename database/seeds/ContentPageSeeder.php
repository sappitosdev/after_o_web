<?php

use Illuminate\Database\Seeder;
use App\ContentPage;
class ContentPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insert=new ContentPage;
        	$insert->cp_description='quantity_recommendations';
        	$insert->cp_content='5';
        $insert->save();
    }
}
