<?php

use Illuminate\Database\Seeder;
use App\Concepts;
class CreateCivilStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insert=new Concepts;
        	$insert->ct_description='ESTADO_CIVIL';
        	$insert->ct_concept='Soltero';
        $insert->save();

        $insert=new Concepts;
        	$insert->ct_description='ESTADO_CIVIL';
        	$insert->ct_concept='Casado';
        $insert->save();

        $insert=new Concepts;
        	$insert->ct_description='ESTADO_CIVIL';
        	$insert->ct_concept='Viudo';
        $insert->save();

        $insert=new Concepts;
        	$insert->ct_description='ESTADO_CIVIL';
        	$insert->ct_concept='Divorciado';
        $insert->save();

    }
}
