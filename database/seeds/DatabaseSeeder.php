<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,1)->create();
        $this->call(CitiesTableSeeder::class);
        $this->call(UniversitiesTableSeeder::class);
        $this->call(ContentPageSeeder::class);
        $this->call(CreateSexsSeeder::class);
        $this->call(CreateCivilStatusSeeder::class);
    }
}
