<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('g_id');
            $table->text('g_name')->nullable();
            $table->text('g_lastname')->nullable();
            $table->text('g_identification')->nullable();
            $table->text('g_email');
            $table->text('g_date_of_birth')->nullable();
            $table->text('g_sex')->nullable();
            $table->text('g_validation_code')->nullable();
            $table->text('citie_c_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
