<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('p_id');
            $table->text('p_identification')->nullable();
            $table->text('p_name')->nullable();
            $table->text('p_lastname')->nullable();
            $table->text('p_sex')->nullable();
            $table->text('p_date_of_birth')->nullable();
            $table->text('p_email')->nullable();;
            $table->integer('citie_c_id')->nullable();
            $table->text('p_citie_name')->nullable()
            $table->text('membership_m_id')->nullable();
            $table->text('p_membership_code')->nullable();
            $table->text('p_cellphone')->nullable();
            $table->text('p_school')->nullable();
            $table->text('p_civil_status')->nullable();
            $table->integer('universitie_u_id')->nullable();
            $table->text('p_university_name')->nullable();
            $table->text('p_profession')->nullable();
            $table->text('p_company')->nullable();
            $table->text('p_position')->nullable();
            $table->text('p_picture')->nullable();
            $table->text('p_status')->nullable();
            $table->text('p_activation_date')->nullable();
            $table->text('p_number_recommendations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
