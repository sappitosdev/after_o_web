<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function (Blueprint $table) {
            $table->increments('b_id');
            $table->text('b_name');
            $table->text('b_description');
            $table->text('b_short_description');
            $table->text('b_brand_logo');
            $table->text('b_promotional_picture');
            $table->text('b_validity')->nullable();
            $table->integer('benefit_categories_bc_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefits');
    }
}
