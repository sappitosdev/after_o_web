<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('e_id');
            $table->text('e_name');
            $table->integer('event_type_et_id');
            $table->integer('event_categorie_ec_id');
            $table->text('e_date_start');
            $table->time('e_time_start');
            $table->text('e_date_end');
            $table->time('e_time_end');
            $table->text('e_short_description');
            $table->text('e_description');
            $table->text('e_address_location');
            $table->text('e_latitud');
            $table->text('e_longitud');
            $table->text('e_total_guests');
            $table->text('e_guest_member_question')->nullable();
            $table->integer('e_minimum_age')->nullable();
            $table->text('e_number_guests_x_members')->nullable();
            $table->integer('e_minimum_age_x_members')->nullable();
            $table->text('e_picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
