<?php

namespace App\Http\Middleware;

use Closure;

class CheckEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario=\Auth::user();
        if($usuario->rol != 'EMPLOYEE'){
            //return view("messages.rejected")->with("message","Usted no posee permisos necesarios para esta acción");
            return redirect('permissions');
        }
        return $next($request);
    }
}
