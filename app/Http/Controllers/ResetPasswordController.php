<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use URL;
use GuzzleHttp\Client;
use Session;
use Redirect;
class ResetPasswordController extends Controller
{
    public function reset(Request $request){
    	$email=$request->email;
     	$user=User::where('email',$email)->get();

     	if(!$user->isEmpty()){


	     	$url=env("APP_URL").'/reset/'.$user[0]->remember_token;
	     	
	     	$client = new Client([
	                 'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
	                 'headers' => [ 'Accept' => 'application/json',
	                 'Authorization' => 'Bearer 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',
	                 'Content-Type' => 'application/json' ]
	        ]);


			$data = json_encode(
	          [
	             'from' => [
	                'email' => 'info@afteroclub.com',
	             ],
	             'personalizations' => [
	                  array("to" => array([
	                            'email' => $email
	                          ]),
	                      'dynamic_template_data' => [
	                            'name' => $user[0]->name,
	                            'url' => $url
	                  ])
	             ],
	             'template_id' => 'd-0f037ceb712146e2b94f80b3aca154a0',  
	          ]
	        );

	        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
	                ['body' => $data]
	        );

	        Session::flash('message', 'Su solicitud fue procesada, verifique su correo electronico.'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('/'); 

        }else{

        	Session::flash('message', 'Ocurrio un error inesperado.'); 
            Session::flash('alert-class', 'alert-grey');
            return redirect('/'); 

        }
    }

    public function reset_password_home($token){
    	return view('reset')
    	->with('token',$token);
    }

    public function reset_this_password(Request $request){

    	if($request->password != $request->confirm_password){

            Session::flash('message', 'Las dos contraseñas deben ser iguales.'); 
            Session::flash('alert-class', 'alert-success');
            return back();

        }elseif(strlen($request->password) < 8 || strlen($request->confirm_password) < 8){

          	Session::flash('message', 'La contraseña debe tener minimo "8" caracteres.'); 
          	Session::flash('alert-class', 'alert-success');
         	return back();

        }else{
        	$user=User::where('remember_token',$request->token_auth)
            ->update(['password' => bcrypt($request->password)]);

			Session::flash('message', 'Su contraseña fue reestablecida exitosamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('/'); 
        }
    }

}
