<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\ContentPage;
use Session;
use Redirect;
use GuzzleHttp\Client;
class AdminPageController extends Controller
{

    public function admlogo(){
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.admlogo')
        ->with('logo',$logo);
    }   

    public function administrationlogin(){
        $title=ContentPage::where('cp_description','the_login_title')->get();
        $text=ContentPage::where('cp_description','the_login_text')->get();
        $image=ContentPage::where('cp_description','the_login_image')->get();
        $link=ContentPage::where('cp_description','the_login_link_redirection')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.administrationlogin')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('link',$link)
        ->with('logo',$logo);
    }

    public function editLogo(Request $request){
            $logo=ContentPage::where('cp_description','logo')->get();

            if($logo->isEmpty()){
                $logo=$request->file('logo')->store('public');

                $insert = new ContentPage;
                $insert->cp_description='logo';
                $insert->cp_content=$logo;
                $save=$insert->save();

                if($save==1){
                    Session::flash('message', 'Logo Agregada correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect()->route('admlogo');
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('admlogo');
                } 
            }else{
                $datos=ContentPage::where('cp_description','logo')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $logo=$request->file('logo')->store('public');

                $update=ContentPage::where('cp_description','logo')
                ->update(['cp_content' => $logo]);


                if($update==true){
                    Session::flash('message', 'Logo Editado correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('admlogo'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('admlogo'); 
                }
            }
    }

    public function editLoginSection(Request $request){
        $text=ContentPage::where('cp_description','the_login_title')
        ->orWhere('cp_description','the_login_text')
        ->orWhere('cp_description','the_login_image')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_login_title';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_login_text';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_login_image';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            $insert4 = new ContentPage;
            $insert4->cp_description='the_login_link_redirection';
            $insert4->cp_content=$request->link;
            $save4=$insert4->save();

            if($save==1 || $save2==1 || $save3==1 || $save4==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('administrationlogin');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('administrationlogin');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_login_image')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_login_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_login_text')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_login_image')
                ->update(['cp_content' => $image]);
                $update4=ContentPage::where('cp_description','the_login_link_redirection')
                ->update(['cp_content' => $request->link]);

                if($update==true && $update2==true && $update3==true && $update4==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('administrationlogin'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('administrationlogin'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_login_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_login_text')
                ->update(['cp_content' => $request->text]);

                $update3=ContentPage::where('cp_description','the_login_link_redirection')
                ->update(['cp_content' => $request->link]);


                if($update==true || $update2==true || $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('administrationlogin'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('administrationlogin'); 
                }
            }
            
        }
    }

    public function slider(){
    	$sliders=ContentPage::where('cp_description','slider_principal')
    	->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
    	return view('admin.slider')
    	->with('sliders',$sliders)
        ->with('logo',$logo);
    }

    public function addBanner(Request $request){
    	$path = $request->file('banner')->store('public');
        
        $insert = new ContentPage;
            $insert->cp_description='slider_principal';
            $insert->cp_content=$path;
            $insert->cp_title=$request->title;
            $insert->cp_text=$request->text;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Banner agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('slider'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('slider'); 
        }
    }

    public function editBanner(Request $request){
        if($request->banner){
            $datos=ContentPage::where('cp_id',$request->cp_id)->get();

            $imagen = $datos[0]['cp_content'];

            Storage::delete($imagen);

            $path = $request->file('banner')->store('public');
            
            $update=ContentPage::where('cp_id',$request->cp_id)
            ->update(['cp_content' => $path,
                      'cp_title' => $request->title,
                      'cp_text' => $request->text]);

            if($update==1){
                Session::flash('message', 'Banner editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slider'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slider'); 
            } 
        }else{
            $update=ContentPage::where('cp_id',$request->cp_id)
            ->update(['cp_title' => $request->title,
                      'cp_text' => $request->text]);

            if($update==1){
                Session::flash('message', 'Banner editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slider'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slider'); 
            } 
        }
    }

    public function deleteBanner(Request $request){
        $slideractive=ContentPage::where('cp_description','slider_principal')
        ->first();
        $sliders=ContentPage::where('cp_description','slider_principal')
        ->where('cp_id','<>',$slideractive->cp_id)
        ->get();
        if(!$slideractive->isEmpty and $sliders->isEmpty()){
            Session::flash('message', 'El Banner no puede ser eliminado debido a ser el unico activo en el elemento.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('slider'); 
        }else{
            $datos=ContentPage::where('cp_id',$request->cp_id)->get();

            $imagen = $datos[0]['cp_content'];

            Storage::delete($imagen);
            
            $delete=ContentPage::where('cp_id',$request->cp_id)
            ->delete();

            if($delete==1){
                Session::flash('message', 'Banner eliminado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slider'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slider'); 
            }
        }
    }


    public function allies(){
    	$allies=ContentPage::where('cp_description','allie')
    	->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
    	return view('admin.allies')
    	->with('allies',$allies)
        ->with('logo',$logo);
    }

    public function addAllie(Request $request){
    	$path = $request->file('allie')->store('public');
        
        $insert = new ContentPage;
        $insert->cp_description='allie';
        $insert->cp_content=$path;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Aliado agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('allies'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('allies'); 
        }
    }

    public function editAllie(Request $request){
    	$datos=ContentPage::where('cp_id',$request->cp_id)->get();

        $imagen = $datos[0]['cp_content'];

        Storage::delete($imagen);

        $path = $request->file('allie')->store('public');
        
        $update=ContentPage::where('cp_id',$request->cp_id)
        ->update(['cp_content' => $path]);

        if($update==1){
            Session::flash('message', 'Aliado editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('allies'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('allies'); 
        }
    }

    public function deleteAllie(Request $request){
    	$datos=ContentPage::where('cp_id',$request->cp_id)->get();

        $imagen = $datos[0]['cp_content'];

        Storage::delete($imagen);
        
        $delete=ContentPage::where('cp_id',$request->cp_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Aliado eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('allies'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('allies'); 
        }
    }


    public function footertext(){
        $titlefooter=ContentPage::where('cp_description','footer_title')
        ->get();
    	$textfooter=ContentPage::where('cp_description','footer_text')
    	->get();
        $logo=ContentPage::where('cp_description','logo')->get();
    	return view('admin.footertext')
    	->with('titlefooter',$titlefooter)
        ->with('textfooter',$textfooter)
        ->with('logo',$logo);
    }

    public function editFooterText(Request $request){

        $text=ContentPage::where('cp_description','footer_text')->get();
        $title=ContentPage::where('cp_description','footer_title')->get();
        if($text->isEmpty() || $title->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='footer_text';
            $insert->cp_content=$request->footertext;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='footer_title';
            $insert2->cp_content=$request->title;
            $save2=$insert2->save();

            if($save==1){
                Session::flash('message', 'Pie de Pagina Editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('footertext');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('footertext');
            }
        }else{
            $update=ContentPage::where('cp_description','footer_text')
            ->update(['cp_content' => $request->footertext]);

            $update2=ContentPage::where('cp_description','footer_title')
            ->update(['cp_content' => $request->title]);

            if($update==true || $update2==true){
                Session::flash('message', 'Pie de Pagina Editado correctamente'); 
                Session::flash('alert-class', 'alert-info');
                return redirect()->route('footertext'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('footertext'); 
            }
        }
    }

    public function theclub(){
        $title=ContentPage::where('cp_description','the_club_title')->get();
        $text=ContentPage::where('cp_description','the_club_text')->get();
        $image=ContentPage::where('cp_description','the_club_image')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.theclub')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('logo',$logo);
    }

    public function editClubSection(Request $request){
        $text=ContentPage::where('cp_description','the_club_title')
        ->orWhere('cp_description','the_club_text')
        ->orWhere('cp_description','the_club_image')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_club_title';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_club_text';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_club_image';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            if($save==1 && $save2==1 && $save3==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('theclub');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('theclub');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_club_image')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_club_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_club_text')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_club_image')
                ->update(['cp_content' => $image]);

                if($update==true && $update2==true && $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('theclub'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('theclub'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_club_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_club_text')
                ->update(['cp_content' => $request->text]);


                if($update==true && $update2==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('theclub'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('theclub'); 
                }
            }
            
        }
    }

    public function theclubdescription(){
        $title=ContentPage::where('cp_description','the_club_title_description')->get();
        $text=ContentPage::where('cp_description','the_club_text_description')->get();
        $image=ContentPage::where('cp_description','the_club_image_description')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.theclubdescription')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('logo',$logo);
    }

    public function editClubSectionDescription(Request $request){
        $text=ContentPage::where('cp_description','the_club_title_description')
        ->orWhere('cp_description','the_club_text_description')
        ->orWhere('cp_description','the_club_image_description')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_club_title_description';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_club_text_description';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_club_image_description';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            if($save==1 && $save2==1 && $save3==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('theclubdescription');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('theclubdescription');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_club_image_description')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_club_title_description')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_club_text_description')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_club_image_description')
                ->update(['cp_content' => $image]);

                if($update==true && $update2==true && $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('theclubdescription'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('theclubdescription'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_club_title_description')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_club_text_description')
                ->update(['cp_content' => $request->text]);


                if($update==true && $update2==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('theclubdescription'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('theclubdescription'); 
                }
            }
            
        }
    }


    public function membershipspage(){
        $title=ContentPage::where('cp_description','the_membership_title')->get();
        $text=ContentPage::where('cp_description','the_membership_text')->get();
        $image=ContentPage::where('cp_description','the_membership_image')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.membershipspage')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('logo',$logo);
    }

    public function editMembershipSection(Request $request){
        $text=ContentPage::where('cp_description','the_membership_title')
        ->orWhere('cp_description','the_membership_text')
        ->orWhere('cp_description','the_membership_image')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_membership_title';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_membership_text';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_membership_image';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            if($save==1 && $save2==1 && $save3==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('membershipspage');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('membershipspage');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_membership_image')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_membership_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_membership_text')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_membership_image')
                ->update(['cp_content' => $image]);

                if($update==true && $update2==true && $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('membershipspage'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('membershipspage'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_membership_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_membership_text')
                ->update(['cp_content' => $request->text]);


                if($update==true && $update2==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('membershipspage'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('membershipspage'); 
                }
            }
            
        }
    }

    public function benefitspage(){
        $title=ContentPage::where('cp_description','the_benefit_title')->get();
        $text=ContentPage::where('cp_description','the_benefit_text')->get();
        $image=ContentPage::where('cp_description','the_benefit_image')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.benefitspage')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('logo',$logo);
    }

    public function editBenefitSection(Request $request){
        $text=ContentPage::where('cp_description','the_benefit_title')
        ->orWhere('cp_description','the_benefit_text')
        ->orWhere('cp_description','the_benefit_image')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_benefit_title';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_benefit_text';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_benefit_image';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            if($save==1 && $save2==1 && $save3==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('benefitspage');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('benefitspage');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_benefit_image')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_benefit_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_benefit_text')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_benefit_image')
                ->update(['cp_content' => $image]);

                if($update==true && $update2==true && $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('benefitspage'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('benefitspage'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_benefit_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_benefit_text')
                ->update(['cp_content' => $request->text]);


                if($update==true && $update2==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('benefitspage'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('benefitspage'); 
                }
            }
            
        }
    }

    public function ads(){
        $ads=ContentPage::where('cp_description','ad')
        ->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.ads')
        ->with('ads',$ads)
        ->with('logo',$logo);
    }

    public function addAd(Request $request){
        $path = $request->file('ad')->store('public');
        
        $insert = new ContentPage;
        $insert->cp_description='ad';
        $insert->cp_content=$path;
        $insert->cp_title=$request->link;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Anuncio agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('ads'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('ads'); 
        }
    }

    public function editAd(Request $request){
        if($request->file('ad')){
           $datos=ContentPage::where('cp_id',$request->ad_id)->get();

            $imagen = $datos[0]['cp_content'];

            Storage::delete($imagen);

            $path = $request->file('ad')->store('public');
            
            $update=ContentPage::where('cp_id',$request->ad_id)
            ->update(['cp_content' => $path,
                      'cp_title' => $request->link]);

            if($update==1){
                Session::flash('message', 'Anuncio editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('ads'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('ads'); 
            } 
        }else{
            $update=ContentPage::where('cp_id',$request->ad_id)
            ->update(['cp_title' => $request->link]);

            if($update==1){
                Session::flash('message', 'Anuncio editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('ads'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('ads'); 
            } 
        }     
    }

    public function deleteAd(Request $request){
        $datos=ContentPage::where('cp_id',$request->ad_id)->get();

        $imagen = $datos[0]['cp_content'];

        Storage::delete($imagen);
        
        $delete=ContentPage::where('cp_id',$request->ad_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Anuncio eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('ads'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('ads'); 
        }
    }

    public function adDetailEvent(){
        $ad=ContentPage::where('cp_description','ad_detail_event')
        ->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.adDetailEvent')
        ->with('ad',$ad)
        ->with('logo',$logo);
    }

    public function editAdDetailEvent(Request $request){
        $ad=ContentPage::where('cp_description','ad_detail_event')
        ->get();

        if($ad->isEmpty()){
            $path = $request->file('image')->store('public');

            $insert = new ContentPage;
                $insert->cp_description='ad_detail_event';
                $insert->cp_content=$path;
                $insert->cp_title=$request->link;
            $save=$insert->save();

            if($save==1){
                Session::flash('message', 'Anuncio agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('adDetailEvent'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('adDetailEvent'); 
            } 
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','ad_detail_event')
                ->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $path = $request->file('image')->store('public');

                $update=ContentPage::where('cp_description','ad_detail_event')
                ->update(['cp_content' => $path,
                          'cp_title' => $request->link]);

                if($update==1){
                    Session::flash('message', 'Anuncio editado correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('adDetailEvent'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('adDetailEvent'); 
                }
            }else{

                $update=ContentPage::where('cp_description','ad_detail_event')
                ->update(['cp_title' => $request->link]);

                if($update==1){
                    Session::flash('message', 'Anuncio editado correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('adDetailEvent'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('adDetailEvent'); 
                } 
            }
        }     
    }

    public function addBannerPrincipalEvent(){
        $banner=ContentPage::where('cp_description','banner_principal_event')
        ->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.addBannerPrincipalEvent')
        ->with('banner',$banner)
        ->with('logo',$logo);
    }

    public function editBannerPrincipalEvent(Request $request){
        $ad=ContentPage::where('cp_description','banner_principal_event')
        ->get();

        if($ad->isEmpty()){
            $path = $request->file('image')->store('public');

            $insert = new ContentPage;
                $insert->cp_description='banner_principal_event';
                $insert->cp_content=$path;
                $insert->cp_title=$request->link;
            $save=$insert->save();

            if($save==1){
                Session::flash('message', 'Banner agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('addBannerPrincipalEvent'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('addBannerPrincipalEvent'); 
            } 
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','banner_principal_event')
                ->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $path = $request->file('image')->store('public');

                $update=ContentPage::where('cp_description','banner_principal_event')
                ->update(['cp_content' => $path,
                          'cp_title' => $request->link]);

                if($update==1){
                    Session::flash('message', 'Banner editado correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('addBannerPrincipalEvent'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('addBannerPrincipalEvent'); 
                }
            }else{

                $update=ContentPage::where('cp_description','banner_principal_event')
                ->update(['cp_title' => $request->link]);

                if($update==1){
                    Session::flash('message', 'Banner editado correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('addBannerPrincipalEvent'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('addBannerPrincipalEvent'); 
                } 
            }
        }     
    }

}
