<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentPage;
use App\Concepts;
use App\Cities;
use App\Partners;
use App\Memberships;
use App\User;
use App\Universities;
use App\Guests;
use Session;
use Redirect;
use Mail;
use GuzzleHttp\Client;
class LandingController extends Controller
{  

    public function index(){
        $titlelogin=ContentPage::where('cp_description','the_login_title')->get();
        $textlogin=ContentPage::where('cp_description','the_login_text')->get();
        $imagelogin=ContentPage::where('cp_description','the_login_image')->get();
        $linklogin=ContentPage::where('cp_description','the_login_link_redirection')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        if(\Auth::user()){
            $usuario=\Auth::user();
            if ($usuario->rol == 'ADMIN') {// do your margic here
                return redirect('/admin');
            }else if ($usuario->rol == 'PARTNER'){
                return redirect('/partner');
            }else if ($usuario->rol == 'SECURITY'){
                return redirect('/security');
            }   
        }else{
            return view('welcome')
            ->with('titlelogin',$titlelogin)
            ->with('textlogin',$textlogin)
            ->with('imagelogin',$imagelogin)
            ->with('linklogin',$linklogin)
            ->with('logo',$logo); 
        }
    }

    public function page(){
        $logo=ContentPage::where('cp_description','logo')->get();

        $slideractive=ContentPage::where('cp_description','slider_principal')
        ->first();
        

        if($slideractive){
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->where('cp_id','<>',$slideractive->cp_id)
            ->get();
        }else{
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->get();
        }
    	
    	 $allies=ContentPage::where('cp_description','allie')
    	 ->get();
    	 $footer_text=ContentPage::where('cp_description','footer_text')
    	 ->get();

       $footer_title=ContentPage::where('cp_description','footer_title')
       ->get();

    	  $titleclub=ContentPage::where('cp_description','the_club_title')->get();
        $textclub=ContentPage::where('cp_description','the_club_text')->get();
        $imageclub=ContentPage::where('cp_description','the_club_image')->get();

        $titleclubdescription=ContentPage::where('cp_description','the_club_title_description')->get();
        $textclubdescription=ContentPage::where('cp_description','the_club_text_description')->get();
        $imageclubdescription=ContentPage::where('cp_description','the_club_image_description')->get();

        $titlemembership=ContentPage::where('cp_description','the_membership_title')->get();
        $textmembership=ContentPage::where('cp_description','the_membership_text')->get();
        $imagemembership=ContentPage::where('cp_description','the_membership_image')->get();

        $titlebenefit=ContentPage::where('cp_description','the_benefit_title')->get();
        $textbenefit=ContentPage::where('cp_description','the_benefit_text')->get();
        $imagebenefit=ContentPage::where('cp_description','the_benefit_image')->get();

    	  return view('page')
        ->with('slideractive',$slideractive)
    	  ->with('sliders',$sliders)
    	  ->with('allies',$allies)
    	  ->with('footer_text',$footer_text)
        ->with('footer_title',$footer_title)
    	  ->with('titleclub',$titleclub)
        ->with('textclub',$textclub)
        ->with('imageclub',$imageclub)
        ->with('titleclubdescription',$titleclubdescription)
        ->with('textclubdescription',$textclubdescription)
        ->with('imageclubdescription',$imageclubdescription)
        ->with('titlemembership',$titlemembership)
        ->with('textmembership',$textmembership)
        ->with('imagemembership',$imagemembership)
        ->with('titlebenefit',$titlebenefit)
        ->with('textbenefit',$textbenefit)
        ->with('imagebenefit',$imagebenefit)
        ->with('logo',$logo);
    }

    public function membersregistration(){
        $cities=Cities::all();
        $universities=Universities::all();
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $civil_status=Concepts::where('ct_description','ESTADO_CIVIL')->get();
        return view('membersregistration')
        ->with('cities',$cities)
        ->with('sexs',$sexs)
        ->with('universities',$universities)
        ->with('civil_status',$civil_status);
    }

    public function registerAccount(Request $request){
       $cedula=Partners::where('p_identification',$request->identification)->get();
       $email=Partners::where('p_email',$request->email)->get();
       $membership=Memberships::where('m_default','default')->get();
       $email_user=User::where('email',$request->email)->get();

       
       if(!$cedula->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con esta identificación.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('membersregistration'); 
       }elseif(!$email->isEmpty() || !$email_user->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con este correo electronico.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('membersregistration'); 
       }else{
            try {

                $codigo = str_random(25);
                $urlcorreo = url('/accountPartnerActivation/').'/'.$codigo;
                $to = $request->email;
                $recommendatios=ContentPage::where('cp_description','quantity_recommendations')->get();
                $client = new Client([
                      'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                      'headers' => [ 'Accept' => 'application/json',
                                     'Authorization' => 'Bearer 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',
                                     'Content-Type' => 'application/json' ]
                ]);


                $data = json_encode(
                  [
                     'from' => [
                        'email' => 'info@afteroclub.com',
                     ],
                     'personalizations' => [
                          array("to" => array([
                                    'email' => $to
                                  ]),
                              'dynamic_template_data' => [
                                    'url' => $urlcorreo,
                                    'name' => $request->name.' '.$request->lastname,
                                    'recommendatios' => $recommendatios[0]->cp_content
                          ])
                     ],
                     'template_id' => 'd-49416c476c6143e5bd73b72c3c0d9379',  
                  ]
                );

               $insert=new Partners;
                 $insert->p_identification=$request->identification;
                 $insert->p_name=ucfirst($request->name);
                 $insert->p_lastname=ucfirst($request->lastname);
                 $insert->p_sex=$request->sex;
                 $insert->p_date_of_birth=$request->dateofbirth;
                 $insert->p_email=$request->email;
                 $insert->citie_c_id=$request->citie;
                 $insert->p_citie_name=ucfirst($request->othercitie);
                 $insert->membership_m_id=$membership[0]->m_id;
                 $insert->p_membership_code=$request->identification;
                 $insert->p_cellphone=$request->telephone;
                 $insert->p_school=ucfirst($request->school);
                 $insert->p_civil_status=$request->civilstatus;
                 $insert->universitie_u_id=$request->university;
                 $insert->p_university_name=ucfirst($request->otheruniversitie);
                 $insert->p_profession=ucfirst($request->profession);
                 $insert->p_company=ucfirst($request->company);
                 $insert->p_position=ucfirst($request->position);
                 $insert->p_status='PENDIENTE_POR_ACTIVACION';
                 $insert->p_number_recommendations=0;
               $insert->save();

               $insertUser=new User;
                 $insertUser->name=$request->name;
                 $insertUser->email=$request->email;
                 $insertUser->rol='PARTNER';
                 $insertUser->partner_p_id=$insert->id;
                 $insertUser->validation_code=$codigo;
               $insertUser->save();

               $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                    ['body' => $data]
               );

               Session::flash('message', 'Su usuario fue registrado exitosamente, revise su bandeja de entrada y verifique el correo electronico con las instrucciones para seguir su proceso.'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('membersregistration'); 

            } catch (Exception $e) {
                Session::flash('message', 'Ocurrio un error inesperado'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('membersregistration'); 
            }
       }
    }

    public function accountPartnerActivation(Request $request,$code){
      $logo=ContentPage::where('cp_description','logo')->get();
      $user=User::select('*')
      ->join('partners','partners.p_id','=','partner_p_id')
      ->where('validation_code',$code)->get();
      return view('partner.partnerActivation')
      ->with('logo',$logo)
      ->with('user',$user);
    }

    public function confirmPasswordNewPartner(Request $request){
      $client = new Client([
                  'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                  'headers' => [ 'Accept' => 'application/json',
                  'Authorization' => 'Bearer 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',
                  'Content-Type' => 'application/json' ]
      ]);

      if($request->password != $request->password_confirmation){
        Session::flash('message', 'Las dos contraseñas deben ser iguales.'); 
        Session::flash('alert-class', 'alert-success');
        return redirect(redirect()->getUrlGenerator()->previous());
      }else{
        if(strlen($request->password) < 8 || strlen($request->password_confirmation) < 8){
          Session::flash('message', 'La contraseña debe tener minimo "8" caracteres.'); 
          Session::flash('alert-class', 'alert-success');
          return redirect(redirect()->getUrlGenerator()->previous());
        }else{
          if($request->identification){
            
              $partner=Partners::where('p_identification',$request->identification)->get();

            if($partner->isEmpty()){

              $update=Partners::where('p_id',$request->p_id)
              ->update(['p_identification'=> $request->identification]);

              $update2=User::where('id',$request->user_id)
              ->update(['first_login'=> '1',
                        'password'=> bcrypt($request->password),
                        'validation_code'=> '']);

              if($update==1 && $update2==1){
                  $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $partner[0]->p_email
                                        ]),
                                    'dynamic_template_data' => [
                                          'name' => $partner[0]->p_name.' '.$partner[0]->p_lastname
                                ])
                           ],
                           'template_id' => 'd-63ecb922cc3748ad9a0c0123d53b4a2e',  
                        ]
                  );

                  $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                          ['body' => $data]
                  );  

                  Session::flash('message', 'Su Usuario Fue Activado Correctamente'); 
                  Session::flash('alert-class', 'alert-success');
                  return redirect('/'); 
              }else{
                  Session::flash('message', 'Ocurrio un error'); 
                  Session::flash('alert-class', 'alert-danger');
                  return redirect('/'); 
              }
            }else{
              Session::flash('message', 'Ya existe un usuario registrado con esta cédula'); 
              Session::flash('alert-class', 'alert-success');
              return redirect(redirect()->getUrlGenerator()->previous());
            }
          }else{
            $user=User::where('id',$request->user_id)->get();
            $partner=Partners::where('p_email',$user[0]->email)->get();
            $update=User::where('id',$request->user_id)
            ->update(['first_login'=> '1',
                      'password'=> bcrypt($request->password),
                      'validation_code'=> '']);

            if($update==1){
                $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $user[0]->email
                                        ]),
                                    'dynamic_template_data' => [
                                          'name' => $partner[0]->p_name.' '.$partner[0]->p_lastname
                                ])
                           ],
                           'template_id' => 'd-63ecb922cc3748ad9a0c0123d53b4a2e',  
                        ]
                );

                $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                        ['body' => $data]
                );  

                Session::flash('message', 'Su Usuario Fue Activado Correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('/'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('/'); 
            }
          }
        }
      }
    }

    public function accountGuestComplete(Request $request,$code){
      $logo=ContentPage::where('cp_description','logo')->get();
      $guest=Guests::where('g_validation_code',$code)->get();
      $sexs=Concepts::where('ct_description','SEXO')->get();
      $cities=Cities::all();
      return view('accountGuestComplete')
      ->with('logo',$logo)
      ->with('guest',$guest)
      ->with('sexs',$sexs)
      ->with('cities',$cities);
    }

    public function confirmDataToGuest(Request $request){
      $update=Guests::where('g_id',$request->g_id)
      ->update(['g_name'=> ucfirst($request->name),
                'g_lastname'=> ucfirst($request->lastname),
                'g_identification'=> $request->identification,
                'g_date_of_birth'=> $request->dateofbirth,
                'g_sex'=> $request->sex,
                'g_validation_code'=> '',
                'citie_c_id'=> $request->citie]);

      if($update==1){
          Session::flash('message', 'Su Datos Fueron Cargados Exitosamente Correctamente'); 
          Session::flash('alert-class', 'alert-success');
          return redirect('public'); 
      }else{
          Session::flash('message', 'Ocurrio un error'); 
          Session::flash('alert-class', 'alert-danger');
          return redirect('public'); 
      }
    }

    public function contactEmail(Request $request){
      try {
        $client = new Client([
                        'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                        'headers' => [ 'Accept' => 'application/json',
                                       'Authorization' => 'Bearer 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',
                                       'Content-Type' => 'application/json' ]
        ]);
        

        $to = $request->email;

          $data = json_encode(
            [
               'from' => [
                  'email' => 'info@afteroclub.com',
               ],
               'personalizations' => [
                    array("to" => array([
                              'email' => $to
                            ]),
                        'dynamic_template_data' => [
                              'name' => $request->name,
                              'lastname' => $request->lastname,
                              'message' => $request->message
                    ])
               ],
               'template_id' => 'd-5554b5c1041141e7a3b316009fb868c8',  
            ]
          );

        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                        ['body' => $data]
        );

        Session::flash('message', 'Su mensaje se envio exitosamente'); 
        Session::flash('alert-class', 'alert-success');
        return redirect('/public');
      } catch (Exception $e) {
        Session::flash('message', 'Ocurrio un error inesperado.'); 
        Session::flash('alert-class', 'alert-danger');
        return redirect('/public');
      }
  }
}
