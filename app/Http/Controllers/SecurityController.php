<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentPage;
use App\Events;
use App\Guests;
use App\EventGuests;
use App\Partners;
use App\EventAssistants;
use App\EventSexs;
use App\EventCities;
use App\EventMembershipGuests;
use App\EventCitiesGuests;
use App\EventSexsGuests;
use Session;
use Redirect;
use DateTime;
use Mail;
use Carbon\Carbon;
use GuzzleHttp\Client;
class SecurityController extends Controller
{
   	public function security(){
   		$now = new DateTime();
		   $now = $now->format('m/d/Y');
   		$logo = ContentPage::where('cp_description','logo')->get();
   		$events=Events::where('e_date_start',$now)
    	   ->get();
   		return view('security')
   		->with('logo',$logo)
   		->with('events',$events);
   	}

   	public function checkGuests(Request $request){
   		$guest=Guests::where('g_identification',$request->identification)
   		->get();
   			
   		$guest_invited=EventGuests::select('*')
   		->join('partners','partners.p_id','=','event_guests.partner_p_id')
   		->join('guests','guests.g_id','=','event_guests.guest_g_id')
   		->where('g_identification',$request->identification)
   		->get();

   		$partner=Partners::where('p_identification',$request->identification)
   		->get();

   		$event=Events::where('e_id',$request->event)
   		->get();

   		$count_event_assitants=EventAssistants::where('event_e_id',$request->event)
   		->count();

   		if(!$partner->isEmpty()){
   			if ($request->ajax()) {
				  return view('renders.checkIncomePartner',
				 ['partner' => $partner,
				  'event' => $event,
				  'count_event_assitants' => $count_event_assitants])->render(); 
			   }
   		}elseif(!$guest->isEmpty()){
   			$event_guests=EventGuests::where('guest_g_id',$guest[0]->g_id)
   			->where('event_e_id',$request->event)
   			->get();

   			if ($request->ajax()) {
   				return view('renders.checkIncomeGuest', 
   					['guest' => $guest,
   					 'event_guests' => $event_guests,
   					 'event' => $event,
   					 'count_event_assitants' => $count_event_assitants,
   					 'guest_invited' => $guest_invited])->render(); 
   			}
   		}else{
            if ($request->ajax()) {
               return view('renders.notInvitedPartnerOrGuest')->render(); 
            }
         }
   	}

   	public function confirmAssitant(Request $request){
   		if($request->p_id){
          $event=Events::where('e_id',$request->e_id)
          ->get();

 			    $partner=Partners::where('p_id',$request->p_id)->get();
 			    $partner_assistant=EventAssistants::where('partner_p_id',$request->p_id)->get();
 			
          $sexs_events=EventSexs::where('event_e_id',$event[0]->e_id)->get();
          $cities_events=EventCities::where('event_e_id',$event[0]->e_id)->get();

        if($event[0]->e_minimum_age != null && Carbon::parse($partner[0]->p_date_of_birth)->age < $event[0]->e_minimum_age){
               Session::flash('message', 'Esta persona no puede ingresar debido una restricción de edad.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
        }elseif(json_encode($sexs_events->contains('sexo', $partner[0]->p_sex)) == 'false'){
               Session::flash('message', 'Esta persona no puede ingresar debido una restricción de sexo.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
        }elseif(json_encode($cities_events->contains('citie_c_id', $partner[0]->citie_c_id)) == 'false'){
               Session::flash('message', 'Esta persona no puede ingresar debido una restricción de ciudad.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
        }elseif($partner[0]->p_status != 'ACTIVO'){
   				    Session::flash('message', 'Este socio se encuentra inactivo.'); 
	            Session::flash('alert-class', 'alert-grey');
	            return redirect('security'); 
   			}elseif(!$partner_assistant->isEmpty()){
   				   Session::flash('message', 'Esta persona ya se encuentra dentro del evento.'); 
	            Session::flash('alert-class', 'alert-grey');
	            return redirect('security'); 
   			}else{
               if($request->emailguest){

                  try {
                      $guest=Guests::where('g_email',$request->emailguest)->get();

                      $codigo = str_random(25);
                      $urlcorreo = url('/accountGuestComplete/').'/'.$codigo;
                      $to = $request->emailguest;

                      $partner=Partners::where('p_id',$request->p_id)->get();

                      $insertGuest=new Guests;
                          $insertGuest->g_email=$request->emailguest;
                          $insertGuest->g_validation_code=$codigo;
                      $insertGuest->save();

                      $insertEA = new EventAssistants;
                        $insertEA->event_e_id=$request->e_id;
                        $insertEA->guest_g_id=$insertGuest->id;
                        $insertEA->partner_p_id=null;
                      $insertEA->save();

                      $insertEA = new EventAssistants;
                       $insertEA->event_e_id=$request->e_id;
                       $insertEA->guest_g_id=null;
                       $insertEA->partner_p_id=$request->p_id;
                      $insertEA->save();

                      $client = new Client([
                              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                              'headers' => [ 'Accept' => 'application/json',
                                             'Authorization' => 'Bearer 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',
                                             'Content-Type' => 'application/json' ]
                      ]);

                      $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $to
                                        ]),
                                    'dynamic_template_data' => [
                                          'name' =>  $partner[0]->p_name.' '.$partner[0]->p_lastname,
                                          'event' => $event[0]->e_name,
                                          'guest' => $guest[0]->g_name.' '.$guest[0]->g_lastname
                                ])
                           ],
                           'template_id' => 'd-82eb4965d60140699f45b84d2b4f058f',  
                        ]
                      );

                      $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                              ['body' => $data]
                      ); 

                      Session::flash('message', 'Acceso confirmado, se envio un correo electronico a su invitado para validar la su información'); 
                      Session::flash('alert-class', 'alert-success');
                      return redirect('security'); 
                  } catch (Exception $e) {
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('security'); 
                  }
                    
               }else{
                  try {
                    $insert = new EventAssistants;
                     $insert->event_e_id=$request->e_id;
                     $insert->guest_g_id=null;
                     $insert->partner_p_id=$request->p_id;
                    $save=$insert->save();
                     Session::flash('message', 'Acceso  confirmado'); 
                     Session::flash('alert-class', 'alert-success');
                     return redirect('security');
                  } catch (Exception $e) {
                     Session::flash('message', 'Ocurrio un error'); 
                     Session::flash('alert-class', 'alert-danger');
                     return redirect('security'); 
                  }
               }
   			}
   		}elseif($request->g_id){
            $guest = Guests::where('g_id',$request->g_id)->get();
            
            $event=Events::where('e_id',$request->e_id)
            ->get();

            $partner_invited=EventGuests::where('guest_g_id',$request->g_id)
            ->where('event_e_id',$request->e_id)
            ->get();

            $partner=Partners::where('p_id',$partner_invited[0]->partner_p_id)->get();

            $sexs_events=EventSexsGuests::where('event_e_id',$event[0]->e_id)->get();
            $cities_events=EventCitiesGuests::where('event_e_id',$event[0]->e_id)->get();

   			    $guest_assistant=EventAssistants::where('guest_g_id',$request->g_id)->get();

   			if($event[0]->e_minimum_age_x_members != null && Carbon::parse($guest[0]->g_date_of_birth)->age < $event[0]->e_minimum_age_x_members){
               Session::flash('message', 'Esta persona no puede ingresar debido a una restricción de edad.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
            }elseif(json_encode($sexs_events->contains('sexo', $guest[0]->g_sex)) == 'false'){
               Session::flash('message', 'Esta persona no puede ingresar debido una restricción de sexo.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
            }elseif(json_encode($cities_events->contains('citie_c_id', $guest[0]->citie_c_id)) == 'false'){
               Session::flash('message', 'Esta persona no puede ingresardebido una restricción de ciudad.'); 
               Session::flash('alert-class', 'alert-grey');
               return redirect('security');
            }elseif(!$guest_assistant->isEmpty()){
   				    Session::flash('message', 'Esta persona ya se encuentra dentro del evento.'); 
	            Session::flash('alert-class', 'alert-grey');
	            return redirect('security'); 
   			}else{
	   			$insert = new EventAssistants;
		   			$insert->event_e_id=$request->e_id;
		   			$insert->guest_g_id=$request->g_id;
		   			$insert->partner_p_id=null;
		   		$save=$insert->save();

		   		if($save==1){
              $client = new Client([
                      'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                      'headers' => [ 'Accept' => 'application/json',
                                     'Authorization' => 'Bearer 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',
                                     'Content-Type' => 'application/json' ]
              ]);

              $data = json_encode(
                [
                   'from' => [
                      'email' => 'info@afteroclub.com',
                   ],
                   'personalizations' => [
                        array("to" => array([
                                  'email' => $to
                                ]),
                            'dynamic_template_data' => [
                                  'name' => $partner[0]->p_name.' '.$partner[0]->p_lastname,
                                  'event' => $event[0]->e_name,
                                  'guest' => $guest[0]->g_name.' '.$guest[0]->g_lastname
                        ])
                   ],
                   'template_id' => 'd-82eb4965d60140699f45b84d2b4f058f',  
                ]
              );

              $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                      ['body' => $data]
              ); 

		          Session::flash('message', 'Acceso  confirmado'); 
		          Session::flash('alert-class', 'alert-success');
		          return redirect('security'); 
		        }else{
		            Session::flash('message', 'Ocurrio un error'); 
		            Session::flash('alert-class', 'alert-danger');
		            return redirect('security'); 
		        }
		   	}
   		}	
   	}
}
