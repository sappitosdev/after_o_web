<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\ContentPage;
use App\Partners;
use App\Benefits;
use App\BenefitsMemberships;
use App\PartnerRecomendations;
use App\BenefitCategories;
use App\Events;
use App\EventGuests;
use App\EventSexs;
use App\EventCities;
use App\EventAssistants;
use App\Guests;
use App\Cities;
use App\Concepts;
use App\Universities;
use App\Memberships;
use Auth;
use Mail;
use Session;
use Redirect;
use Response;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
class PartnerController extends Controller
{
    public function partner(){
    	$logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
       ->get();
    	$footer_text=ContentPage::where('cp_description','footer_text')
    	->get();

        $slideractive=ContentPage::where('cp_description','slider_principal')
        ->first();

        if($slideractive){
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->where('cp_id','<>',$slideractive->cp_id)
            ->get();
        }else{
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->get();
        }

        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')
        ->get();

        $partner=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('p_id',\Auth::user()->partner_p_id)
        ->get();

        $partners_cartelera=Partners::where('p_activation_date','<>',null)
        ->orderBy('p_activation_date','DESC')
        ->take(6)
        ->get();

        $benefits=Benefits::select('*')
        ->join('benefit_categories','benefits.benefit_categories_bc_id','=','benefit_categories.bc_id')
        ->join('benefits_memberships','benefits.b_id','=','benefits_memberships.benefit_b_id')
        ->join('memberships','memberships.m_id','=','benefits_memberships.membership_m_id')
        ->join('benefits_cities','benefits.b_id','=','benefits_cities.benefit_b_id')
        ->join('cities','cities.c_id','=','benefits_cities.citie_c_id')
        ->where('membership_m_id',$partner[0]->membership_m_id)
        ->where('citie_c_id',$partner[0]->citie_c_id)
        ->get();

        $partner_recomendations=PartnerRecomendations::select('*')
        ->join('partners','partners.p_id','=','partner_recomendations.partner_recommender_id')
        ->where('partner_recommended_id',\Auth::user()->partner_p_id)
        ->get();

        $events=Events::select('*')
        ->join('event_memberships','event_memberships.event_e_id','=','events.e_id')
        ->join('event_cities','event_cities.event_e_id','=','events.e_id')
        ->where('e_minimum_age_x_members','<=',Carbon::parse($partner[0]->p_date_of_birth)->age)
        ->where('membership_m_id',$partner[0]->membership_m_id)
        ->where('citie_c_id',$partner[0]->citie_c_id)
        ->get();

        $adactive=ContentPage::where('cp_description','ad')
        ->first();

        if($adactive){
            $ads=ContentPage::where('cp_description','ad')
            ->where('cp_id','<>',$adactive->cp_id)
            ->get();
        }else{
            $ads=ContentPage::where('cp_description','ad')
            ->get();
        }


    	return view('partner.partner')
    	->with('logo',$logo)
        ->with('footer_title',$footer_title)
    	->with('footer_text',$footer_text)
        ->with('partner',$partner)
        ->with('events',$events)
        ->with('quantity_recommendations',$quantity_recommendations)
        ->with('partner_recomendations',$partner_recomendations)
        ->with('slideractive',$slideractive)
        ->with('sliders',$sliders)
        ->with('benefits',$benefits)
        ->with('adactive',$adactive)
        ->with('ads',$ads)
        ->with('partners_cartelera',$partners_cartelera);
    }

    public function searchPartnerToRecommend(Request $request){
        if($request->cedula){
            $partner=Partners::where('p_identification',$request->cedula)
            ->where('p_status','PENDIENTE_POR_ACTIVACION')
            ->get();
        }elseif($request->correo){
            $partner=Partners::where('p_email',$request->correo)
            ->where('p_status','PENDIENTE_POR_ACTIVACION')
            ->get();
        }
        return $partner;
    }

    public function recomendedPartner (Request $request){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();
        $partner=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('p_id',\Auth::user()->partner_p_id)
        ->get();
        return view('partner.recomended')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('partner',$partner);
    }

    public function partner_recommended(Request $request){

        $client = new Client([
              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
              'headers' => [ 'Accept' => 'application/json',
                             'Authorization' => 'Bearer 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',
                             'Content-Type' => 'application/json' ]
        ]);

        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')->get();

        $partner_actual=Partners::where('p_id',\Auth::user()->partner_p_id)->get();

        $partner_recommended=Partners::where('p_identification',$request->recommendedPartnerData)
        ->orWhere('p_email',$request->recommendedPartnerData)
        ->get();

        if(!$partner_recommended->isEmpty()){
            $partner_recommendation=PartnerRecomendations::where('partner_recommended_id',$partner_recommended[0]->p_id)
            ->where('partner_recommender_id',\Auth::user()->partner_p_id)
            ->get();

            if($partner_actual[0]->p_id == $partner_recommended[0]->p_id){
              Session::flash('message', 'No puede recomendarse asi mismo dentro del portal.'); 
              Session::flash('alert-class', 'alert-grey');
              return redirect('recomendedPartner'); 
            }elseif(!$partner_recommendation->isEmpty()){
              Session::flash('message', 'Usted ya ha realizado una recomendación de este socio.'); 
              Session::flash('alert-class', 'alert-grey');
              return redirect('recomendedPartner'); 
            }else{
              $recommendatios=ContentPage::where('cp_description','quantity_recommendations')->get();

              $insert=new PartnerRecomendations;
                  $insert->partner_recommender_id=$partner_actual[0]->p_id;
                  $insert->partner_recommended_id=$partner_recommended[0]->p_id;
              $insert->save();

              $number_recommendations=$partner_recommended[0]->p_number_recommendations + 1;

              $update=Partners::where('p_id',$partner_recommended[0]->p_id)
              ->update(['p_number_recommendations' => $number_recommendations]);

              $partner=Partners::where('p_id',$partner_recommended[0]->p_id)->get();

              if(($partner[0]->p_number_recommendations == $quantity_recommendations[0]->cp_content) && $partner[0]->p_status == 'ACTIVO'){
                      $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $to
                                        ]),
                                    'dynamic_template_data' => [
                                          'name' => $partner_recommended[0]->p_name.' '.$partner_recommended[0]->p_lastname,
                                          'recommendatios' => $recommendatios[0]->cp_content,
                                          'partner_recommendatios' => $number_recommendations,
                                          'member' => $partner_actual[0]->p_name.' '.$partner_actual[0]->p_lastname
                                ])
                           ],
                           'template_id' => 'd-5fe6fd900d8d48c08ee59022ba22a07e',  
                        ]
                      );

                      $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                              ['body' => $data]
                      );    
              }elseif($partner[0]->p_status == 'PENDIENTE_POR_ACTIVACION'){

                  //EMAIL PARA EL RECOMENDADO
                  $to = $partner_recommended[0]->p_email;
                  $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $to
                                        ]),
                                    'dynamic_template_data' => [
                                          'name' => $partner_recommended[0]->p_name.' '.$partner_recommended[0]->p_lastname,
                                          'recommendatios' => $recommendatios[0]->cp_content,
                                          'partner_recommendatios' => $number_recommendations,
                                          'member' => $partner_actual[0]->p_name.' '.$partner_actual[0]->p_lastname
                                ])
                           ],
                           'template_id' => 'd-64e29e6fd58f4874a5d71f40cdda93cd',  
                        ]
                  );

                  $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                          ['body' => $data]
                  );  

                  //EMAIL PARA EL COMENDADOR
                  $to = $partner_actual[0]->p_email;
                  $data = json_encode(
                        [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $to
                                        ]),
                                    'dynamic_template_data' => [
                                          'recommendatios' => $recommendatios[0]->cp_content,
                                          'partner_recommendatios' => $number_recommendations,
                                          'member' => $partner_actual[0]->p_name.' '.$partner_actual[0]->p_lastname,
                                          'email_recommended' => $partner_recommended[0]->p_email
                                ])
                           ],
                           'template_id' => 'd-63dc4e2d8ad641a29dad689f35fa76a4',  
                        ]
                  );

                  $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                          ['body' => $data]
                  );

                  if($number_recommendations >= $quantity_recommendations[0]->cp_content){

                      $update=Partners::where('p_id',$partner_recommended[0]->p_id)
                      ->update(['p_status' => 'ACTIVO',
                      'p_activation_date'=> date('d/m/Y') ]);

                      //BIENVENIDA ACTIVACION
                      $to = $partner_recommended[0]->p_email;
                      $data = json_encode(
                            [
                               'from' => [
                                  'email' => 'info@afteroclub.com',
                               ],
                               'personalizations' => [
                                    array("to" => array([
                                              'email' => $to
                                            ]),
                                        'dynamic_template_data' => [
                                              'name' => $partner_recommended[0]->p_name.' '.$partner_recommended[0]->p_lastname,
                                    ])
                               ],
                               'template_id' => 'd-5fe6fd900d8d48c08ee59022ba22a07e',  
                            ]
                      );

                      $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                              ['body' => $data]
                      );
                  }
              }

              Session::flash('message', 'Usted ha Recomendado exitosamente al socio seleccionado. Su Solicitud fue procesada'); 
              Session::flash('alert-class', 'alert-grey');
              return redirect('recomendedPartner'); 
            }
        }else{
                $recommendatios=ContentPage::where('cp_description','quantity_recommendations')->get();
                 $codigo = str_random(25);
                 $urlcorreo = url('/accountPartnerData/').'/'.$codigo;

                 $to = $request->recommendedPartnerData;

                 $insert=new Partners;
                   $insert->p_email=$request->recommendedPartnerData;
                   $insert->p_status='PENDIENTE_POR_ACTIVACION';
                   $insert->p_number_recommendations=0;
                 $insert->save();

                 $insertUser=new User;
                   $insertUser->rol='PARTNER';
                   $insertUser->partner_p_id=$insert->id;
                   $insertUser->email=$request->recommendedPartnerData;
                   $insertUser->validation_code=$codigo;
                 $insertUser->save();

                 $data = json_encode(
                    [
                           'from' => [
                              'email' => 'info@afteroclub.com',
                           ],
                           'personalizations' => [
                                array("to" => array([
                                          'email' => $to
                                        ]),
                                    'dynamic_template_data' => [
                                          'recommendatios' => 0,
                                          'back_recommendatios' => $recommendatios[0]->cp_content,
                                          'member' => $partner_actual[0]->p_name.' '.$partner_actual[0]->p_lastname
                                ])
                           ],
                           'template_id' => 'd-c14cf882adc744c781a6297deaedeceb',  
                        ]
                  );

                  $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                          ['body' => $data]
                  ); 
              Session::flash('message', 'Usted ha Recomendado exitosamente al socio seleccionado. Su Solicitud fue procesada'); 
              Session::flash('alert-class', 'alert-grey');
              return redirect('recomendedPartner'); 
        }

        
    }

    public function invitedEvent(Request $request){
        $url=$request->url;

        $partner=Partners::where('p_email',$request->email)->get();
        $guest=Guests::where('g_email',$request->email)->get();

        $event=Events::where('e_id',$request->event)->get();

        $sexs_events=EventSexs::where('event_e_id',$event[0]->e_id)->get();
        $cities_events=EventCities::where('event_e_id',$event[0]->e_id)->get();

        $codigo = str_random(25);
        $urlcorreo = url('/accountGuestComplete/').'/'.$codigo;

        $partner_logged=Partners::where('p_id',\Auth::user()->partner_p_id)->get();

        $client = new Client([
            'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
            'headers' => [ 'Accept' => 'application/json',
            'Authorization' => 'Bearer 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',
            'Content-Type' => 'application/json' ]
        ]);


        if($event[0]->e_guest_member_question=='No'){
            if($url=='partner')
            {
                Session::flash('message', 'El evento no esta disponible para realizar ninguna invitación.'); 
                Session::flash('alert-class', 'alert-grey');
                return redirect('partner');
            }else{
                Session::flash('message', 'El evento no esta disponible para realizar ninguna invitación.'); 
                Session::flash('alert-class', 'alert-grey');
                return back();
            } 
        }elseif($partner_logged[0]->p_email==$request->email){
            if($url=='partner')
            {
                Session::flash('message', 'Usted no puede invitarse a si mismo al evento.'); 
                Session::flash('alert-class', 'alert-grey');
                return redirect('partner');
            }else{
                Session::flash('message', 'Usted no puede invitarse a si mismo al evento.'); 
                Session::flash('alert-class', 'alert-grey');
                return back();
            } 
        }else{
            try {

                if(!$partner->isEmpty()){

                    $guestInvited=EventGuests::where('partner_p_id',$partner[0]->p_id)
                    ->where('event_e_id',$request->event)
                    ->get();

                    if($event[0]->e_minimum_age_x_members != null && Carbon::parse($partner[0]->p_date_of_birth)->age < $event[0]->e_minimum_age_x_members){
                        
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de edad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de edad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 

                    }elseif(json_encode($sexs_events->contains('sexo', $partner[0]->p_sex)) == 'false'){
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a este socio al evento seleccionado debido  una restricción de sexo.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a este socio al evento seleccionado debido  una restricción de sexo.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }elseif(json_encode($cities_events->contains('citie_c_id', $partner[0]->citie_c_id)) == 'false'){
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a este socio al evento seleccionado debido  una restricción de ciudad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a este socio al evento seleccionado debido  una restricción de ciudad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }elseif(!$guestInvited->isEmpty()){
                        if($url=='partner')
                        {
                            Session::flash('message', 'Este socio ya fue invitada a este evento.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'Este socio ya fue invitada a este evento.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }else{
                    
                        $insertEG=new EventGuests;
                            $insertEG->guest_g_id=0;
                            $insertEG->event_e_id=$request->event;
                            $insertEG->partner_p_id=$partner[0]->p_id;
                            $insertEG->partner_invited_p_id=$partner_logged[0]->p_id;
                        $insertEG->save();

                        $to = $partner[0]->p_email;
                        
                        $data = json_encode(
                              [
                                 'from' => [
                                    'email' => 'info@afteroclub.com',
                                 ],
                                 'personalizations' => [
                                      array("to" => array([
                                                'email' => $to
                                              ]),
                                          'dynamic_template_data' => [
                                                'name_event' => $event[0]->e_name,
                                                'member' => $partner[0]->p_name.' '.$partner[0]->p_lastname,
                                                'partner' => $partner_logged[0]->p_name.' '.$partner_logged[0]->p_lastname,
                                      ])
                                 ],
                                 'template_id' => 'd-3c99ec9f8a144bfa87aeb040a611d74f',  
                              ]
                        );

                        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                                ['body' => $data]
                        ); 

                        if($url=='partner')
                        {
                            Session::flash('message', 'Invitacion enviada correctamente. 1'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'Invitacion enviada correctamente. 1'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }
                }
                elseif(!$guest->isEmpty()){

                    $guestInvited=EventGuests::where('guest_g_id',$guest[0]->g_id)
                    ->where('event_e_id',$request->event)
                    ->get();

                    if($event[0]->e_minimum_age_x_members != null && Carbon::parse($guest[0]->g_date_of_birth)->age < $event[0]->e_minimum_age_x_members){
                        
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de edad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de edad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 

                    }elseif(json_encode($sexs_events->contains('sexo', $guest[0]->g_sex)) == 'false'){
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de sexo.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de sexo.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }elseif(json_encode($cities_events->contains('citie_c_id', $guest[0]->citie_c_id)) == 'false'){
                        if($url=='partner')
                        {
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de ciudad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'No esta permitido invitar a esta persona al evento seleccionado debido  una restricción de ciudad.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }elseif(!$guestInvited->isEmpty()){
                        if($url=='partner')
                        {
                            Session::flash('message', 'Esta persona ya fue invitada a este evento.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'Esta persona ya fue invitada a este evento.'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }else{

                        $insertEG=new EventGuests;
                            $insertEG->guest_g_id=$guest[0]->g_id;
                            $insertEG->event_e_id=$request->event;
                            $insertEG->partner_p_id=0;
                            $insertEG->partner_invited_p_id=$partner_logged[0]->p_id;
                        $insertEG->save();

                        $to = $guest[0]->g_email;

                        $data = json_encode(
                              [
                                 'from' => [
                                    'email' => 'info@afteroclub.com',
                                 ],
                                 'personalizations' => [
                                      array("to" => array([
                                                'email' => $to
                                              ]),
                                          'dynamic_template_data' => [
                                                'name_event' => $event[0]->e_name,
                                                'member' => $guest[0]->g_name.' '.$guest[0]->g_lastname,
                                                'partner' => $partner_logged[0]->p_name.' '.$partner_logged[0]->p_lastname,
                                      ])
                                 ],
                                 'template_id' => 'd-3c99ec9f8a144bfa87aeb040a611d74f',  
                              ]
                        );

                        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                                ['body' => $data]
                        ); 

                        if($url=='partner')
                        {
                            Session::flash('message', 'Invitacion enviada correctamente. 2'); 
                            Session::flash('alert-class', 'alert-grey');
                            return redirect('partner');
                        }else{
                            Session::flash('message', 'Invitacion enviada correctamente. 2'); 
                            Session::flash('alert-class', 'alert-grey');
                            return back();
                        } 
                    }
                }else{

                    $insert=new Guests;
                        $insert->g_email=$request->email;
                        $insert->g_validation_code=$codigo;
                    $insert->save();

                    $insertEG=new EventGuests;
                        $insertEG->guest_g_id=$insert->id;
                        $insertEG->event_e_id=$request->event;
                        $insertEG->partner_p_id=0;
                        $insertEG->partner_invited_p_id=$partner_logged[0]->p_id;
                    $insertEG->save();

                    $to = $request->email;

                    $data = json_encode(
                          [
                             'from' => [
                                'email' => 'info@afteroclub.com',
                             ],
                             'personalizations' => [
                                  array("to" => array([
                                            'email' => $to
                                          ]),
                                      'dynamic_template_data' => [
                                          'name_event' => $event[0]->e_name,
                                          'member' => $partner_logged[0]->p_name.' '.$partner_logged[0]->p_lastname,
                                          'url' => $urlcorreo
                                  ])
                             ],
                             'template_id' => 'd-2e0d7126912e4cb8a8dc8bf6dad5fda3',  
                          ]
                    );

                    $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                            ['body' => $data]
                    ); 

                    if($url=='partner')
                    {
                        Session::flash('message', 'Invitacion enviada correctamente. 3'); 
                        Session::flash('alert-class', 'alert-grey');
                        return redirect('partner');
                    }else{
                        Session::flash('message', 'Invitacion enviada correctamente. 3'); 
                        Session::flash('alert-class', 'alert-grey');
                        return back();
                    } 
                }

            } catch (Exception $e) {
                if($url=='partner')
                {
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('partner');
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return back();
                } 
            }
        }
    }

    public function benefits(){
        $slideractive=ContentPage::where('cp_description','slider_principal')
        ->first();
        

        if($slideractive){
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->where('cp_id','<>',$slideractive->cp_id)
            ->get();
        }else{
            $sliders=ContentPage::where('cp_description','slider_principal')
            ->get();
        }

        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();

        $partner=Partners::where('p_id',Auth::user()->partner_p_id)->get();

        $categories=BenefitCategories::all();

        $benefits=Benefits::select('*')
        ->join('benefit_categories','benefits.benefit_categories_bc_id','=','benefit_categories.bc_id')
        ->join('benefits_memberships','benefits.b_id','=','benefits_memberships.benefit_b_id')
        ->join('benefits_cities','benefits.b_id','=','benefits_cities.benefit_b_id')
        ->join('memberships','memberships.m_id','=','benefits_memberships.membership_m_id')
        ->join('cities','cities.c_id','=','benefits_cities.citie_c_id')
        ->where('membership_m_id',$partner[0]->membership_m_id)
        ->where('citie_c_id',$partner[0]->citie_c_id)
        ->get();

        return view('partner.benefits')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('categories',$categories)
        ->with('benefits',$benefits)
        ->with('slideractive',$slideractive)
        ->with('sliders',$sliders);
    }

    public function benefitDetail($id){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();

        $benefit=Benefits::select('*')
        ->join('benefit_categories','benefits.benefit_categories_bc_id','=','benefit_categories.bc_id')
        ->where('b_id',$id)
        ->get();

        $cities=Cities::select('*')
        ->join('benefits_cities','cities.c_id','=','benefits_cities.citie_c_id')
        ->join('benefits','benefits.b_id','=','benefits_cities.benefit_b_id')
        ->where('benefit_b_id',$id)
        ->get();

        return view('partner.benefitDetail')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('benefit',$benefit)
        ->with('cities',$cities);
    }

    public function events(){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();
        $banner=ContentPage::where('cp_description','banner_principal_event')
        ->get();

        $partner=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('p_id',\Auth::user()->partner_p_id)
        ->get();

        $events=Events::select('*')
        ->join('event_memberships','event_memberships.event_e_id','=','events.e_id')
        ->join('event_cities','event_cities.event_e_id','=','events.e_id')
        ->join('event_sexs','event_sexs.event_e_id','=','events.e_id')
        ->where('e_minimum_age','<=',Carbon::parse($partner[0]->p_date_of_birth)->age)
        ->where('membership_m_id',$partner[0]->membership_m_id)
        ->where('citie_c_id',$partner[0]->citie_c_id)
        ->where('sexo',$partner[0]->p_sex)
        ->orderBy('e_date_start','desc')
        ->get();

        return view('partner.events')
        ->with('logo',$logo)
        ->with('banner',$banner)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('events',$events);
    }

    public function eventDetail($id){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();
        $event=Events::select('*')
        ->join('event_categories','event_categories.ec_id','=','events.event_categorie_ec_id')
        ->where('e_id',$id)
        ->get();

        $partner=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('p_id',\Auth::user()->partner_p_id)
        ->get();

        $ad=ContentPage::where('cp_description','ad_detail_event')
        ->get();

        return view('partner.eventDetail')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('event',$event)
        ->with('partner',$partner)
        ->with('ad',$ad);
    }

    public function profile(){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();
        $partner=Partners::select('*')
        ->join('cities','cities.c_id','=','partners.citie_c_id')
        ->join('universities','universities.u_id','=','partners.universitie_u_id')
        ->where('p_id',Auth::user()->partner_p_id)
        ->get();
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $civilstatus=Concepts::where('ct_description','ESTADO_CIVIL')->get();

        $cities=Cities::all();
        $universities=Universities::all();

        return view('partner.profile')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('partner',$partner)
        ->with('sexs',$sexs)
        ->with('civilstatus',$civilstatus)
        ->with('cities',$cities)
        ->with('universities',$universities);
    }

    public function editProfile(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['p_name' => ucfirst($request->names),
                  'p_lastname' => ucfirst($request->lastnames),
                  'p_sex' => $request->sex,
                  'p_date_of_birth' => $request->dateofbirth,
                  'p_civil_status' => $request->civil_status,
                  'p_cellphone' => $request->phone,
                  'p_school' => $request->school,
                  'p_profession' => $request->profession,
                  'p_company' => $request->company,
                  'p_position' => $request->position,
                  'citie_c_id' => $request->citie,
                  'p_citie_name' => $request->citie_name,
                  'universitie_u_id' => $request->universitie,
                  'p_university_name' => $request->university_name]);

        if($update==1){
            Session::flash('message', 'Perfil Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('profile'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('profile'); 
        }
    }

    public function editProfilePicture(Request $request){
        $partner=Partners::select('*')
        ->where('p_id',\Auth::user()->partner_p_id)
        ->get();

        if($partner[0]->p_picture == '' || $partner[0]->p_picture == null){
            $picture=$request->file('profilepicture')->store('public');

            $update = Partners::where('p_id',$request->p_id)
            ->update(['p_picture' => $picture]);

            if($update==1){
                Session::flash('message', 'Imagen Editada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('profile'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('profile'); 
            }
        }else{
            $imagen = $partner[0]->p_picture;

            Storage::delete($imagen);

            $picture=$request->file('profilepicture')->store('public');

            $update = Partners::where('p_id',$request->p_id)
            ->update(['p_picture' => $picture]);

            if($update==1){
                Session::flash('message', 'Imagen Editada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('profile'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('profile'); 
            }
        }
    }

    public function changepassword(Request $request){
        $logo = ContentPage::where('cp_description','logo')->get();
        $footer_title=ContentPage::where('cp_description','footer_title')
        ->get();
        $footer_text=ContentPage::where('cp_description','footer_text')
        ->get();
        $partner=Partners::where('p_id',Auth::user()->partner_p_id)
        ->get();
        return view('partner.changepassword')
        ->with('logo',$logo)
        ->with('footer_title',$footer_title)
        ->with('footer_text',$footer_text)
        ->with('partner',$partner);
    }

    public function changepasswordToPartner(Request $request){
        $partner=Partners::where('p_id',$request->p_id)->get();
        $to = $partner[0]->p_email;
        if($request->password != $request->confirm_password){

            Session::flash('message', 'Las dos contraseñas deben ser iguales.'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('changepassword'); 

        }elseif(strlen($request->password) < 8 || strlen($request->confirm_password) < 8){

          Session::flash('message', 'La contraseña debe tener minimo "8" caracteres.'); 
          Session::flash('alert-class', 'alert-success');
          return redirect('changepassword'); 
        }else{
            $update = User::where('partner_p_id',$request->p_id)
            ->update(['password' => bcrypt($request->password)]);

            if($update==1){

                $client = new Client([
                            'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                            'headers' => [ 'Accept' => 'application/json',
                            'Authorization' => 'Bearer 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',
                            'Content-Type' => 'application/json' ]
                ]);


                $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $to
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $partner[0]->p_name.' '.$partner[0]->p_lastname
                              ])
                         ],
                         'template_id' => 'd-cec2118a1b6d45d9b90ea92ddc5265b5',  
                      ]
                );

                $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                        ['body' => $data]
                );  

                Session::flash('message', 'Su contraseña fue cambiada exitosamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('changepassword'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('changepassword'); 
            }
       }
    }

    public function searchInvitedGuestsToEvent(Request $request){
        

        $partner=Partners::where('p_id',Auth::user()->partner_p_id)->get();

        if ($request->ajax()) {

            $gg= EventGuests::select('*')
            ->join('partners','partners.p_id','=','event_guests.partner_p_id')
            ->where('event_e_id',$request->id)
            ->where('partner_invited_p_id',$partner[0]->p_id)
            ->get();
             
            $gp=  EventGuests::select('*')
            ->join('guests','guests.g_id','=','event_guests.guest_g_id')
            ->where('event_e_id',$request->id)
            ->where('partner_invited_p_id',$partner[0]->p_id)
            ->get(); 
             
            foreach($gg as $g) {
                $gp->add($g);
            }
             
             
            $gp->toArray();
          

            return view('renders.InvitedGuestsToEvent', 
                    ['guests' => $gp])->render(); 
        }
    }

    public function deleteInvitationGuest(Request $request){
        $event=Events::where('e_id',$request->e_id)->get();

        $invitation=EventGuests::where('eg_id',$request->eg_id)
        ->get();

        if(!$invitation->isEmpty()){

          if($invitation[0]->partner_p_id != 0){
              $partner=Partners::where('p_id',$invitation[0]->partner_p_id)->get();
              $to=$partner[0]->p_email;
              $name=$partner[0]->p_name.' '.$partner[0]->p_lastname;
          }elseif($invitation[0]->guest_g_id != 0){
              $guest=Guests::where('g_id',$invitation[0]->guest_g_id)->get();
              $to=$guest[0]->g_email;
              $name=$guest[0]->g_name.' '.$guest[0]->g_lastname;
          }

        }
        
        $delete=EventGuests::where('eg_id',$request->eg_id)
        ->delete();

        if($delete==1){
            $client = new Client([
                  'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                  'headers' => [ 'Accept' => 'application/json',
                                 'Authorization' => 'Bearer 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',
                                 'Content-Type' => 'application/json' ]
            ]);

            $data = json_encode(
              [
                 'from' => [
                    'email' => 'info@afteroclub.com',
                 ],
                 'personalizations' => [
                      array("to" => array([
                                'email' => $to
                              ]),
                          'dynamic_template_data' => [
                                'name' => $name,
                                'event_name' => $event[0]->e_name
                      ])
                 ],
                 'template_id' => 'd-a329b5e5e92e42b1ae41a6e72cc2bec0',  
              ]
            );

            $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                    ['body' => $data]
            );   

            Session::flash('message', 'La invitación fue eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return Redirect::back();
        }
    }

    public function bookATable(Request $request){

        $to = $request->email;

        $client = new Client([
              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
              'headers' => [ 'Accept' => 'application/json',
                             'Authorization' => 'Bearer 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',
                             'Content-Type' => 'application/json' ]
        ]);


        $data = json_encode(
          [
             'from' => [
                'email' => 'info@afteroclub.com',
             ],
             'personalizations' => [
                  array("to" => array([
                            'email' => $to
                          ]),
                      'dynamic_template_data' => [
                            'membership' => $request->membership_code,
                            'numberpersons' => $request->numberpersons,
                            'wish' => ucfirst($request->wish)
                  ])
             ],
             'template_id' => 'd-90b34b8d8270498ab25fb009bd0e6f5f',  
          ]
        );
        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                ['body' => $data]
        );

        Session::flash('message', 'Su solicitud de reservación de mesa fue procesada y enviada a nuestro equipo de trabajo'); 
        Session::flash('alert-class', 'alert-grey');
        return Redirect::back();
    }

    public function accountPartnerData(Request $request,$code){
      $logo=ContentPage::where('cp_description','logo')->get();
      $user=User::select('*')
      ->join('partners','partners.p_id','=','partner_p_id')
      ->where('validation_code',$code)->get();
      $sexs=Concepts::where('ct_description','SEXO')->get();
      $cities=Cities::all();
      $civilstatus=Concepts::where('ct_description','ESTADO_CIVIL')->get();
      $universities=Universities::all();
      return view('partner.partnerActivation')
      ->with('logo',$logo)
      ->with('user',$user)
      ->with('sexs',$sexs)
      ->with('cities',$cities)
      ->with('civil_status',$civilstatus)
      ->with('universities',$universities);
    }

    public function confirmNewData(Request $request){
       $cedula=Partners::where('p_identification',$request->identification)->get();
       $membership=Memberships::where('m_default','default')->get();
       $email_user=User::where('email',$request->email)->get();

      if(!$cedula->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con esta identificación.'); 
            Session::flash('alert-class', 'alert-info');
             return back();
      }elseif($request->password != $request->password_confirmation){
            Session::flash('message', 'Las dos contraseñas deben ser iguales.'); 
            Session::flash('alert-class', 'alert-success');
            return back();
      }elseif(strlen($request->password) < 8 || strlen($request->password_confirmation) < 8){
          Session::flash('message', 'La contraseña debe tener minimo "8" caracteres.'); 
          Session::flash('alert-class', 'alert-success');
          return back();
      }else{
            try {

             $partner=Partners::where('p_id',$request->p_id)
             ->update([
                 'p_identification' => $request->identification,
                 'p_name' => ucfirst($request->name),
                 'p_lastname' => ucfirst($request->lastname),
                 'p_sex' => $request->sex,
                 'p_date_of_birth' => $request->dateofbirth,
                 'p_email' => $request->email,
                 'citie_c_id' => $request->citie,
                 'p_citie_name' => ucfirst($request->othercitie),
                 'membership_m_id' => $membership[0]->m_id,
                 'p_membership_code' => $request->identification,
                 'p_cellphone' => $request->telephone,
                 'p_school' => ucfirst($request->school),
                 'p_civil_status' => $request->civilstatus,
                 'universitie_u_id' => $request->university,
                 'p_university_name' => ucfirst($request->otheruniversitie),
                 'p_profession' => ucfirst($request->profession),
                 'p_company' => ucfirst($request->company),
                 'p_position' => ucfirst($request->position),
               ]);

              $user=User::where('id',$request->user_id)
              ->update([
                 'name' => ucfirst($request->name),
                 'validation_code' => NULL,
                 'password' => '111'
               ]);

               Session::flash('message', 'Su datos fueron cargados exitosamente'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('/'); 

            } catch (Exception $e) {
                Session::flash('message', 'Ocurrio un error inesperado'); 
                Session::flash('alert-class', 'alert-danger');
                 return back();
            }
       }
    }
}
