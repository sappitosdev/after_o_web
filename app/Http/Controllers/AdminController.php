<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Cities;
use App\Events;
use App\EventCategories;
use App\EventGuests;
use App\EventMemberships;
use App\EventMembershipGuests;
use App\EventCities;
use App\EventCitiesGuests;
use App\EventSexs;
use App\EventSexsGuests;
use App\EventTypes;
use App\Universities;
use App\Guests;
use App\Benefits;
use App\BenefitCategories;
use App\Partners;
use App\PartnerRecomendations;
use App\Memberships;
use App\ContentPage;
use App\BenefitsMemberships;
use App\Concepts;
use App\User;
use App\BenefitsCities;
use Session;
use Redirect;
use Mail;
use Excel;
use GuzzleHttp\Client;
use Carbon\Carbon;
class AdminController extends Controller
{
    public function admin(){
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin')
        ->with('logo',$logo);
    }

 	public function cities(){
 		$cities=Cities::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
 		return view('admin.cities')
 		->with('cities',$cities)
        ->with('logo',$logo);
 	}

 	public function addCitie(Request $request){
 		$insert = new Cities;
 		$insert->c_citie=ucfirst($request->citie);
 		$insert->c_status='active';
 		$save=$insert->save();

 		if($save==1){
            Session::flash('message', 'Ciudad agregada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('cities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('cities'); 
        }
 	}

 	public function disabledCitie(Request $request){
 		$update = Cities::where('c_id',$request->c_id)
 		->update(['c_status' => 'disabled']);

 		if($update==1){
            Session::flash('message', 'Ciudad desactivada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('cities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('cities'); 
        }
 	}

 	public function activeCitie(Request $request){
 		$update = Cities::where('c_id',$request->c_id)
 		->update(['c_status' => 'active']);

 		if($update==1){
            Session::flash('message', 'Ciudad Activada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('cities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('cities'); 
        }
 	}

 	public function editCitie(Request $request){
 		$update = Cities::where('c_id',$request->c_id)
 		->update(['c_citie' => ucfirst($request->citie)]);

 		if($update==1){
            Session::flash('message', 'Ciudad Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('cities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('cities'); 
        }
 	}

 	public function deleteCitie(Request $request){
 		$delete = Cities::where('c_id',$request->c_id)
 		->delete();

 		if($delete==1){
            Session::flash('message', 'Ciudad Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('cities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('cities'); 
        }
 	}

 	public function eventcategories(){
 		$eventcategories=EventCategories::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
 		return view('admin.eventcategories')
 		->with('eventcategories',$eventcategories)
        ->with('logo',$logo);
 	}

 	public function addCategorie(Request $request){
 		$insert = new EventCategories;
 		$insert->ec_categorie=ucfirst($request->categorie);
 		$save=$insert->save();

 		if($save==1){
            Session::flash('message', 'Categoria agregada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventcategories'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventcategories'); 
        }
 	}

	public function editCategorie(Request $request){
 		$update = EventCategories::where('ec_id',$request->ec_id)
 		->update(['ec_categorie' => ucfirst($request->categorie)]);

 		if($update==1){
            Session::flash('message', 'Categoria Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventcategories'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventcategories'); 
        }
 	}

	public function deleteCategorie(Request $request){
 		$delete = EventCategories::where('ec_id',$request->ec_id)
 		->delete();

 		if($delete==1){
            Session::flash('message', 'Categoria Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventcategories'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventcategories'); 
        }
 	}

 	public function evenTypes(){
 		$eventypes=EventTypes::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
 		return view('admin.eventypes')
 		->with('eventypes',$eventypes)
        ->with('logo',$logo);
 	}

 	public function addType(Request $request){
 		$insert = new EventTypes;
 		$insert->et_type=ucfirst($request->type);
 		$save=$insert->save();

 		if($save==1){
            Session::flash('message', 'Tipo de evento Agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventypes'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventypes'); 
        }
 	}

	public function editType(Request $request){
 		$update = EventTypes::where('et_id',$request->et_id)
 		->update(['et_type' => ucfirst($request->type)]);

 		if($update==1){
            Session::flash('message', 'Tipo de evento Editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventypes'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventypes'); 
        }
 	}

	public function deleteType(Request $request){
 		$delete = EventTypes::where('et_id',$request->et_id)
 		->delete();

 		if($delete==1){
            Session::flash('message', 'Tipo de evento Eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('eventypes'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('eventypes'); 
        }
 	}

    public function events(){
        $events=Events::select('*')
        ->join('event_types','event_types.et_id','=','events.event_type_et_id')
        ->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        $eventTypes=EventTypes::all();
        $eventCategories=EventCategories::all();
        $memberships=Memberships::all();
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $cities=Cities::all();
        return view('admin.events')
        ->with('events',$events)
        ->with('eventTypes',$eventTypes)
        ->with('eventCategories',$eventCategories)
        ->with('memberships',$memberships)
        ->with('sexs',$sexs)
        ->with('cities',$cities)
        ->with('logo',$logo);
    }

    public function addEvent(Request $request){
        $client = new Client([
                 'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                 'headers' => [ 'Accept' => 'application/json',
                 'Authorization' => 'Bearer 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',
                 'Content-Type' => 'application/json' ]
        ]);

        $partners=Partners::all();

        if($request->promotionalpicture){
            $promitionalPicture=$request->file('promotionalpicture')->store('public');

            $insertEvent = new Events;
                $insertEvent->e_name=ucfirst($request->name);
                $insertEvent->event_type_et_id=$request->eventType;
                $insertEvent->event_categorie_ec_id=$request->eventCategorie;
                $insertEvent->e_date_start=$request->dateStart;
                $insertEvent->e_time_start=$request->timeStart;
                $insertEvent->e_date_end=$request->dateEnd;
                $insertEvent->e_time_end=$request->timeEnd;
                $insertEvent->e_short_description=$request->shortdescription;
                $insertEvent->e_description=$request->description;
                $insertEvent->e_address_location=$request->address_location;
                $insertEvent->e_latitud=$request->latitud;
                $insertEvent->e_longitud=$request->longitud;
                $insertEvent->e_total_guests=$request->totalguests;
                $insertEvent->e_guest_member_question=$request->invxmiembros;
                $insertEvent->e_minimum_age=$request->minimumage;
                $insertEvent->e_number_guests_x_members=$request->numberGuestxMember;
                $insertEvent->e_minimum_age_x_members=$request->minimumagemembers;
                $insertEvent->e_picture=$promitionalPicture;
            $saveEvent=$insertEvent->save();
            $lastEventId=$insertEvent->id;

            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$lastEventId;
                    $insertEventMembership->save();
            }

            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$lastEventId;
                    $insertEventSexs->save();
            }

            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$lastEventId;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$lastEventId;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$lastEventId;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$lastEventId;
                        $insertEventCities->save();
                }

            }

            if($saveEvent==1){
                $date=Carbon::parse($insertEvent->e_date_start);
                $time=Carbon::parse($insertEvent->e_time_start);
                foreach ($partners as $key => $value) {

                    if($date->month==1 || $date->month =='1')
                        $month='ENE';
                    elseif($date->month== 2 || $date->month =='2')
                        $month='FEB';
                    elseif($date->month==3 || $date->month =='3')
                        $month='MAR';
                    elseif($date->month==4 || $date->month =='4')
                        $month='ABR';
                    elseif($date->month==5 || $date->month =='5')
                        $month='MAY';
                    elseif($date->month==6 || $date->month =='6')
                        $month='JUN';
                    elseif($date->month==7 || $date->month =='7')
                        $month='JUL';
                    elseif($date->month==8 || $date->month =='8')
                        $month='AGTO';
                    elseif($date->month==9 || $date->month =='9')
                        $month='SEPT';
                    elseif($date->month== 10 || $date->month =='10')
                        $month='OCT';
                    elseif($date->month== 11 || $date->month =='11')
                        $month='NOV';
                    elseif($date->month== 12 || $date->month =='12')
                        $month='DIC';

                    $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $value->p_email
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $value->p_name.' '.$value->p_lastname,
                                        'address' => $insertEvent->e_address_location,
                                        'month' => $date->month,
                                        'day' => $date->day,
                                        'time' => $time->format('g:i A')
                              ])
                         ],
                         'template_id' => 'd-25f6ce0cface4a2f9ac67e6242b27756',  
                      ]
                    );

                    $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                            ['body' => $data]
                    );

                }

                Session::flash('message', 'Evento Agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('events'); 

            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('events'); 
            }
        }else{
            $insertEvent = new Events;
                $insertEvent->e_name=ucfirst($request->name);
                $insertEvent->event_type_et_id=$request->eventType;
                $insertEvent->event_categorie_ec_id=$request->eventCategorie;
                $insertEvent->e_date_start=$request->dateStart;
                $insertEvent->e_time_start=$request->timeStart;
                $insertEvent->e_date_end=$request->dateEnd;
                $insertEvent->e_time_end=$request->timeEnd;
                $insertEvent->e_short_description=$request->shortdescription;
                $insertEvent->e_description=$request->description;
                $insertEvent->e_address_location=$request->address_location;
                $insertEvent->e_latitud=$request->latitud;
                $insertEvent->e_longitud=$request->longitud;
                $insertEvent->e_total_guests=$request->totalguests;
                if(!$request->minimumage){
                    $insertEvent->e_minimum_age=0;
                }else{
                    $insertEvent->e_minimum_age=$request->minimumage;
                }         
                $insertEvent->e_number_guests_x_members=$request->numberGuestxMember;
                if(!$request->minimumagemembers){
                    $insertEvent->e_minimum_age_x_members=0;
                }else{
                    $insertEvent->e_minimum_age_x_members=$request->minimumagemembers;
                }     
            $saveEvent=$insertEvent->save();
            $lastEventId=$insertEvent->id;

            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$lastEventId;
                    $insertEventMembership->save();
            }

            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$lastEventId;
                    $insertEventSexs->save();
            }

            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$lastEventId;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$lastEventId;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$lastEventId;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$lastEventId;
                        $insertEventCities->save();
                }

            }

            if($saveEvent==1){
                $date=Carbon::parse($insertEvent->e_date_start);
                $time=Carbon::parse($insertEvent->e_time_start);


                foreach ($partners as $key => $value) {

                    if($date->month==1 || $date->month =='1')
                        $month='ENE';
                    elseif($date->month== 2 || $date->month =='2')
                        $month='FEB';
                    elseif($date->month==3 || $date->month =='3')
                        $month='MAR';
                    elseif($date->month==4 || $date->month =='4')
                        $month='ABR';
                    elseif($date->month==5 || $date->month =='5')
                        $month='MAY';
                    elseif($date->month==6 || $date->month =='6')
                        $month='JUN';
                    elseif($date->month==7 || $date->month =='7')
                        $month='JUL';
                    elseif($date->month==8 || $date->month =='8')
                        $month='AGTO';
                    elseif($date->month==9 || $date->month =='9')
                        $month='SEPT';
                    elseif($date->month== 10 || $date->month =='10')
                        $month='OCT';
                    elseif($date->month== 11 || $date->month =='11')
                        $month='NOV';
                    elseif($date->month== 12 || $date->month =='12')
                        $month='DIC';

                    $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $value->p_email
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $value->p_name.' '.$value->p_lastname,
                                        'address' => $insertEvent->e_address_location,
                                        'month' => $month,
                                        'day' => $date->day,
                                        'time' => $time->format('g:i A')
                              ])
                         ],
                         'template_id' => 'd-25f6ce0cface4a2f9ac67e6242b27756',  
                      ]
                    );

                    $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                            ['body' => $data]
                    );

                }

                Session::flash('message', 'Evento Agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('events'); 

            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('events'); 
            }
        }       
    }

    public function editEventView(Request $request,$id){
        $event=Events::select('*')
        ->join('event_types','event_types.et_id','=','events.event_type_et_id')
        ->where('e_id',$id)
        ->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        $eventTypes=EventTypes::all();
        $eventCategories=EventCategories::all();

        //Membresias Del Evento
        $memberships_all=Memberships::all();
        $memberships_event=EventMemberships::select('*')
        ->join('memberships','event_memberships.membership_m_id','=','memberships.m_id')
        ->where('event_e_id',$id)
        ->get();

        foreach ($memberships_all as $key => $value) {
           foreach ($memberships_event as $key => $valor) {
               if($value->m_id == $valor->membership_m_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }

        //Membresias Por Miembros
        $memberships_all_guests=Memberships::all();
        $memberships_guests=EventMembershipGuests::select('*')
        ->join('memberships','event_membership_guests.membership_m_id','=','memberships.m_id')
        ->where('event_e_id',$id)
        ->get();

        foreach ($memberships_all_guests as $key => $value) {
           foreach ($memberships_guests as $key => $valor) {
               if($value->m_id == $valor->membership_m_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }


        //Sexos Del Evento
        $sexs_all=Concepts::where('ct_description','SEXO')->get();
        $sexs_event=EventSexs::where('event_e_id',$id)->get();
        foreach ($sexs_all as $key => $value) {
           foreach ($sexs_event as $key => $valor) {
               if($value->ct_concept == $valor->sexo){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }
        //Sexos Por Miembros
        $sexs_all_guests=Concepts::where('ct_description','SEXO')->get();
        $sexs_guests=EventSexsGuests::where('event_e_id',$id)->get();
        foreach ($sexs_all_guests as $key => $value) {
           foreach ($sexs_guests as $key => $valor) {
               if($value->ct_concept == $valor->sexo){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }
        
        
        $cities_all=Cities::all();
        $cities_event=EventCities::select('*')
        ->join('cities','event_cities.citie_c_id','=','cities.c_id')
        ->where('event_e_id',$id)
        ->get();

        foreach ($cities_all as $key => $value) {
           foreach ($cities_event as $key => $valor) {
               if($value->c_id == $valor->citie_c_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }

        $cities_all_guests=Cities::all();
        $cities_guests=EventCitiesGuests::select('*')
        ->join('cities','event_cities_guests.citie_c_id','=','cities.c_id')
        ->where('event_e_id',$id)
        ->get();

        foreach ($cities_all_guests as $key => $value) {
           foreach ($cities_guests as $key => $valor) {
               if($value->c_id == $valor->citie_c_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }

        return view('admin.editEventView')
        ->with('event',$event)
        ->with('eventTypes',$eventTypes)
        ->with('eventCategories',$eventCategories)
        ->with('memberships_all',$memberships_all)
        ->with('memberships_all_guests',$memberships_all_guests)
        ->with('sexs_all',$sexs_all)
        ->with('sexs_all_guests',$sexs_all_guests)
        ->with('cities_all',$cities_all)
        ->with('cities_all_guests',$cities_all_guests)
        ->with('logo',$logo);
    }

    public function editEvent(Request $request){

        if($request->file('promotionalpicture')){
            $datos=Events::where('e_id',$request->e_id)->get();

            $imagen = $datos[0]['e_picture'];

            Storage::delete($imagen);

            $path = $request->file('promotionalpicture')->store('public');

            $lastEventId=$datos[0]->e_id;

            $delete=EventMemberships::where('event_e_id',$request->e_id)->delete();
            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$lastEventId;
                    $insertEventMembership->save();
            }

            $delete=EventSexs::where('event_e_id',$request->e_id)->delete();

            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$lastEventId;
                    $insertEventSexs->save();
            }

            $delete=EventCities::where('event_e_id',$request->e_id)->delete();
            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$lastEventId;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                $delete=EventMembershipGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$lastEventId;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                $delete=EventSexsGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$lastEventId;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                $delete=EventCitiesGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$lastEventId;
                        $insertEventCities->save();
                }

            }

            $update = Events::where('e_id',$request->e_id)
            ->update(['e_name' => $request->name,
                      'event_type_et_id' => $request->eventType,
                      'event_categorie_ec_id' => $request->eventCategorie,
                      'e_date_start' => $request->dateStart,
                      'e_time_start' => $request->timeStart,
                      'e_date_end' => $request->dateEnd,
                      'e_time_end' => $request->timeEnd,
                      'e_short_description' => $request->shortdescription,
                      'e_description' => $request->description,
                      'e_latitud' => $request->latitud,
                      'e_longitud' => $request->longitud,
                      'e_total_guests' => $request->totalguests,
                      'e_number_guests_x_members' => $request->numberGuestxMember,
                      'e_picture' => $path]);

            if(!$request->minimumage){
                    $update = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age' => 0]);
            }else{
                    $update = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age' => $request->minimumage]);
            }

            if(!$request->minimumagemembers){
                    $update = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age_x_members' => 0]);
            }else{
                    $update = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age_x_members' => $request->minimumage]);
            }
        }else{
            $delete=EventMemberships::where('event_e_id',$request->e_id)->delete();
            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$request->e_id;
                    $insertEventMembership->save();
            }

            $delete=EventSexs::where('event_e_id',$request->e_id)->delete();
            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$request->e_id;
                    $insertEventSexs->save();
            }

            $delete=EventCities::where('event_e_id',$request->e_id)->delete();
            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$request->e_id;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                $delete=EventMembershipGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$request->e_id;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                $delete=EventSexsGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$request->e_id;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                $delete=EventCitiesGuests::where('event_e_id',$request->e_id)->delete();
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$request->e_id;
                        $insertEventCities->save();
                }

            }

            $update = Events::where('e_id',$request->e_id)
            ->update(['e_name' => $request->name,
                      'event_type_et_id' => $request->eventType,
                      'event_categorie_ec_id' => $request->eventCategorie,
                      'e_date_start' => $request->dateStart,
                      'e_time_start' => $request->timeStart,
                      'e_date_end' => $request->dateEnd,
                      'e_time_end' => $request->timeEnd,
                      'e_short_description' => $request->shortdescription,
                      'e_description' => $request->description,
                      'e_address_location' => $request->address_location,
                      'e_latitud' => $request->latitud,
                      'e_longitud' => $request->longitud,
                      'e_total_guests' => $request->totalguests,
                      'e_number_guests_x_members' => $request->numberGuestxMember]);

            if(!$request->minimumage){
                    $update2 = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age' => 0]);
            }else{
                    $update2 = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age' => $request->minimumage]);
            }

            if(!$request->minimumagemembers){
                    $update3 = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age_x_members' => 0]);
            }else{
                    $update3 = Events::where('e_id',$request->e_id)
                    ->update(['e_minimum_age_x_members' => $request->minimumagemembers]);
            }
        }

        if($update==1){
            Session::flash('message', 'Evento Editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('events'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('events'); 
        }
    }

    public function deleteEvent(Request $request){
        $event = Events::where('e_id',$request->e_id)->get();

        if(!$event->isEmpty()){
            if($event[0]['e_picture'] != null){
                $imagen=$event[0]['e_picture'];
                Storage::delete($imagen);
            }
            $delete = Events::where('e_id',$request->e_id)
            ->delete();

            $delete2 = EventMemberships::where('event_e_id',$request->e_id)
            ->delete();

            $delete3 = EventSexs::where('event_e_id',$request->e_id)
            ->delete();

            $delete4 = EventCities::where('event_e_id',$request->e_id)
            ->delete();

            $delete5 = EventMembershipGuests::where('event_e_id',$request->e_id)
            ->delete();

            $delete6 = EventSexsGuests::where('event_e_id',$request->e_id)
            ->delete();

            $delete7 = EventCitiesGuests::where('event_e_id',$request->e_id)
            ->delete();
        }

        if($delete==1){
            Session::flash('message', 'Evento Eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('events'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('events'); 
        }
    }

    public function confirmassitence(){
        $events=Events::all();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.confirmassitence')
        ->with('events',$events)
        ->with('logo',$logo);
    }


    public function checkConfirmAssitence(Request $request){
        $guest=Guest::where('g_identification',$request->identification)->get();
        $invitation=EventGuests::select('*')
        ->join('partners','partners.p_id','=','event_guests.partner_p_id')
        ->join('guests','guests.g_id','=','event_guests.guest_g_id')
        ->where('guest_g_id',$guest[0]->g_identification)
        ->where('event_e_id',$request->event)
        ->get();
        if ($request->ajax()) {
            return view('renders.checkConfirmAssitence',
                 ['invitation' => $invitation])->render();  
        }
    }

    public function universities(){
        $universities=Universities::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.universities')
        ->with('universities',$universities)
        ->with('logo',$logo);
    }

    public function addUniversitie(Request $request){
        $insert = new Universities;
        $insert->u_universitie=ucfirst($request->universitie);
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Universidad Agregada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('universities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('universities'); 
        }
    }

    public function editUniversitie(Request $request){
        $update = Universities::where('u_id',$request->u_id)
        ->update(['u_universitie' => ucfirst($request->universitie)]);

        if($update==1){
            Session::flash('message', 'Universidad Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('universities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('universities'); 
        }
    }

    public function deleteUniversitie(Request $request){
        $delete = Universities::where('u_id',$request->u_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Universidad Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('universities'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('universities'); 
        }
    }

    public function benefitcategories(){
        $benefitcategories=BenefitCategories::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.benefitcategories')
        ->with('benefitcategories',$benefitcategories)
        ->with('logo',$logo);
    }

    public function addBenefitCategorie(Request $request){
        $icon = $request->file('icon')->store('public');

        $insert = new BenefitCategories;
        $insert->bc_benefit_categorie=ucfirst($request->categorie);
        $insert->bc_icon=$icon;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Categoria de Beneficio Agregada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefitcategories'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefitcategories'); 
        }
    }

    public function editBenefitCategorie(Request $request){
        if($request->icon){
            $datos=BenefitCategories::where('bc_id',$request->bc_id)->get();

            $imagen = $datos[0]['bc_icon'];

            Storage::delete($imagen);

            $path = $request->file('icon')->store('public');
            
            $update=BenefitCategories::where('bc_id',$request->bc_id)
            ->update(['bc_benefit_categorie' => ucfirst($request->categorie),
                      'bc_icon' => $path]);

            if($update==1){
                Session::flash('message', 'Categoria de Beneficio Editada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('benefitcategories'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('benefitcategories'); 
            }
        }else{
            $update=BenefitCategories::where('bc_id',$request->bc_id)
            ->update(['bc_benefit_categorie' => ucfirst($request->categorie)]);

            if($update==1){
                Session::flash('message', 'Categoria de Beneficio Editada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('benefitcategories'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('benefitcategories'); 
            }
        }
    }

    public function deleteBenefitCategorie(Request $request){
        $datos=BenefitCategories::where('bc_id',$request->bc_id)
        ->get();

        $imagen = $datos[0]['bc_icon'];

        Storage::delete($imagen);

        $delete = BenefitCategories::where('bc_id',$request->bc_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Categoria de Beneficio Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefitcategories'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefitcategories'); 
        }
    }

    public function benefits(){
        $benefits=Benefits::select('*')
        //->join('benefits_cities', 'benefits_cities.benefit_b_id', '=', 'benefits.b_id')
        //->join('cities', 'cities.c_id', '=', 'benefits_cities.citie_c_id')
        ->join('benefit_categories', 'benefit_categories.bc_id', '=', 'benefits.benefit_categories_bc_id')
        ->paginate('5');
        $categories=BenefitCategories::all();
        $cities=Cities::all();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.benefits')
        ->with('benefits',$benefits)
        ->with('categories',$categories)
        ->with('cities',$cities)
        ->with('logo',$logo);
    }

    public function addBenefit(Request $request){
        $brandlogo = $request->file('brandlogo')->store('public');
        $promotionalpicture = $request->file('promotionalpicture')->store('public');

        $insert = new Benefits;
            $insert->b_name=ucfirst($request->name);
            $insert->b_description=$request->descripction;
            $insert->b_short_description=$request->shortDescripction;
            $insert->b_brand_logo=$brandlogo;
            $insert->b_promotional_picture=$promotionalpicture;
            $insert->b_validity=$request->validity;
            $insert->benefit_categories_bc_id=$request->categorie;
        $save=$insert->save();

        foreach ($request->cities as $key => $value) {
            $insert2 = new BenefitsCities;
                $insert2->citie_c_id=$value;
                $insert2->benefit_b_id=$insert->id;
            $save2=$insert2->save();
        }
        
        if($save==1){
            Session::flash('message', 'Beneficio Agregado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefits');
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefits'); 
        }
    }

    public function citiesToBenefit (Request $request){
        $benefits_cities=BenefitsCities::where('benefit_b_id',$request->id)->get();
        return $benefits_cities;
    }

    public function editBenefit(Request $request){
        try {
            $update=Benefits::where('b_id',$request->b_id)
            ->update(['benefit_categories_bc_id' => $request->categorie,
                      'b_name' => ucfirst($request->name),
                      'b_description' => $request->descripction,
                      'b_short_description' => $request->shortDescripction,
                      'b_validity' => $request->validity]);

            $delete=BenefitsCities::where('benefit_b_id',$request->b_id)->delete();

            foreach ($request->cities as $key => $value) {
                $insert=new BenefitsCities;
                    $insert->benefit_b_id=$request->b_id;
                    $insert->citie_c_id=$value;
                $insert->save();
            }

            Session::flash('message', 'Beneficio Editado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefits'); 
        } catch (Exception $e) {
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefits'); 
        }
    }

    public function deleteBenefit(Request $request){
        $datos=Benefits::where('b_id',$request->b_id)
        ->get();

        $imagen = $datos[0]['b_brand_logo'];
        $imagen2 = $datos[0]['b_promotional_picture'];

        Storage::delete($imagen);
        Storage::delete($imagen2);

        $delete = Benefits::where('b_id',$request->b_id)
        ->delete();

        $delete2 = BenefitsMemberships::where('benefit_b_id',$request->b_id)
        ->delete();

        $delete3 = BenefitsCities::where('benefit_b_id',$request->b_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Beneficio Eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefits'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefits'); 
        }
    }

    public function editBrandLogo(Request $request){
        $datos=Benefits::where('b_id',$request->b_id)->get();

        $imagen = $datos[0]['b_brand_logo'];

        Storage::delete($imagen);

        $path = $request->file('brandlogo')->store('public');
        
        $update=Benefits::where('b_id',$request->b_id)
        ->update(['b_brand_logo' => $path]);

        if($update==1){
            Session::flash('message', 'Logo de Marca Editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefits'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefits'); 
        }
    }

    public function editPromotionalImage(Request $request){
        $datos=Benefits::where('b_id',$request->b_id)->get();

        $imagen = $datos[0]['b_promotional_picture'];

        Storage::delete($imagen);

        $path = $request->file('promotionalpicture')->store('public');
        
        $update=Benefits::where('b_id',$request->b_id)
        ->update(['b_promotional_picture' => $path]);

        if($update==1){
            Session::flash('message', 'Imagen Promocional Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('benefits'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('benefits'); 
        }
    }

    public function partners(){
        $partners=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->paginate('10');
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $civil_status=Concepts::where('ct_description','ESTADO_CIVIL')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        $cities=Cities::all();
        $universities=Universities::all();
        $memberships=Memberships::all();
        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')->get();
        return view('admin.partners')
        ->with('partners',$partners)
        ->with('logo',$logo)
        ->with('sexs',$sexs)
        ->with('civil_status',$civil_status)
        ->with('cities',$cities)
        ->with('memberships',$memberships)
        ->with('universities',$universities)
        ->with('quantity_recommendations',$quantity_recommendations);
    }

    public function addPartner(Request $request){
       $cedula=Partners::where('p_identification',$request->identification)->get();
       $email=Partners::where('p_email',$request->email)->get();
       $membership=Memberships::where('m_default','default')->get();
       $email_user=User::where('email',$request->email)->get();
       if(!$cedula->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con esta identificación.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partners'); 
       }elseif(!$email->isEmpty() || !$email_user->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con este correo electronico.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partners'); 
       }elseif($membership->isEmpty()){
            Session::flash('message', 'Debe cargar una Membresía y colocarla como "DEFAULT" para poder realizar la creacion de un socio.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partners');
       }else{
            try {

               $codigo = str_random(25);
               $urlcorreo = url('/accountPartnerActivation/').'/'.$codigo;
               $to = $request->email;
               $insert=new Partners;
                 $insert->p_identification=$request->identification;
                 $insert->p_name=ucfirst($request->name);
                 $insert->p_lastname=ucfirst($request->lastname);
                 $insert->p_sex=$request->sex;
                 $insert->p_date_of_birth=$request->dateofbirth;
                 $insert->p_email=$request->email;
                 $insert->citie_c_id=$request->citie;
                 $insert->p_citie_name=ucfirst($request->othercitie);
                 $insert->membership_m_id=$membership[0]->m_id;
                 $insert->p_membership_code=$request->identification;
                 $insert->p_cellphone=$request->telephone;
                 $insert->p_school=ucfirst($request->school);
                 $insert->p_civil_status=$request->civilstatus;
                 $insert->universitie_u_id=$request->university;
                 $insert->p_university_name=ucfirst($request->otheruniversitie);
                 $insert->p_profession=ucfirst($request->profession);
                 $insert->p_company=ucfirst($request->company);
                 $insert->p_position=ucfirst($request->position);
                 $insert->p_status='PENDIENTE_POR_ACTIVACION';
                 $insert->p_number_recommendations=0;
               $insert->save();

               $insertUser=new User;
                 $insertUser->name=$request->name;
                 $insertUser->email=$request->email;
                 $insertUser->rol='PARTNER';
                 $insertUser->partner_p_id=$insert->id;
                 $insertUser->validation_code=$codigo;
               $insertUser->save();

               $client = new Client([
                      'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                      'headers' => [ 'Accept' => 'application/json',
                                     'Authorization' => 'Bearer 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',
                                     'Content-Type' => 'application/json' ]
                ]);


                $data = json_encode(
                  [
                     'from' => [
                        'email' => 'info@afteroclub.com',
                     ],
                     'personalizations' => [
                          array("to" => array([
                                    'email' => $to
                                  ]),
                              'dynamic_template_data' => [
                                    'url' => $urlcorreo,
                                    'name' => $request->name.' '.$request->lastname,
                                    'user' => $request->email
                          ])
                     ],
                     'template_id' => 'd-b1cb62e637164d368cfb65b3132186ed',  
                  ]
                );
                
                $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                    ['body' => $data]
               );

               Session::flash('message', 'Su usuario fue registrado exitosamente, revise su bandeja de entrada y verifique el correo electronico con las instrucciones para seguir su proceso.'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('partners'); 

            } catch (Exception $e) {
                Session::flash('message', 'Ocurrio un error inesperado'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partners'); 
            }
        }
    }

    public function addPatnerToExcel(Request $request){
        $client = new Client([
              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
              'headers' => [ 'Accept' => 'application/json',
                             'Authorization' => 'Bearer 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',
                             'Content-Type' => 'application/json' ]
        ]);

        $countInserteds=0;
        $countNotInserteds=0;
        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = Excel::load($path,function($reader){})->get();

            if (!$data->isEmpty() && $data->count()){
                $citie_other=Cities::where('c_citie','Otra')->get();
                $universitie_other=Universities::where('u_universitie','Otra')->get();
                $membership=Memberships::where('m_default','default')->get();
                    foreach ($data as $key => $value) {
                        $codigo = str_random(25);
                        $urlcorreo = url('/accountPartnerActivation/').'/'.$codigo;
                        $to = $value->email;
                        $citie=Cities::where('c_citie',$value->residencia_actual)->get();
                        $universitie=Universities::where('u_universitie',$value->nombre_de_la_universidad_donde_te_graduaste)->get();

                        $partner=Partners::where('p_identification',$value->numero_de_cedula)
                        ->orWhere('p_email',$value->email)
                        ->get();

                        if($partner->isEmpty()){
                            $insert=new Partners;
                             $insert->p_identification=$value->numero_de_cedula;
                             $insert->p_name=ucfirst($value->nombre);
                             $insert->p_lastname=ucfirst($value->apellido);
                             
                                 if($value->sexo == 'Hombre'){
                                    $insert->p_sex='Masculino';
                                 }elseif($value->sexo == 'Mujer'){
                                    $insert->p_sex='Femenino';
                                 }
                                 
                             $insert->p_date_of_birth=$value->fecha_de_nacimiento->format('m/d/Y');
                             $insert->p_email=$value->email;

                                 if($citie->isEmpty()){
                                    $insert->citie_c_id=$citie_other[0]->c_id;
                                    $insert->p_citie_name=ucfirst($value->residencia_actual);
                                 }else{
                                    $insert->citie_c_id=$citie[0]->c_id;
                                    $insert->p_citie_name=null;
                                 }
                             
                             $insert->membership_m_id=$membership[0]->m_id;
                             $insert->p_membership_code=$value->numero_de_cedula;
                             $insert->p_cellphone=$value->telephone;
                             $insert->p_school=ucfirst($value->nombre_del_colegio_donde_te_graduaste);
                             $insert->p_civil_status=$value->estado_civil;

                                if($universitie->isEmpty()){
                                    $insert->universitie_u_id=$universitie_other[0]->u_id;
                                    $insert->p_university_name=ucfirst($value->nombre_de_la_universidad_donde_te_graduaste);
                                 }else{
                                    $insert->universitie_u_id=$universitie[0]->u_id;
                                    $insert->p_university_name=null;
                                 }
                             
                             $insert->p_profession=ucfirst($value->profesion);
                             $insert->p_company=ucfirst($value->tienes_carrera_universitaria);
                             $insert->p_position=ucfirst($value->cargo_que_desempenas_actualmente
                             );
                             $insert->p_status='PENDIENTE_POR_ACTIVACION';
                             $insert->p_number_recommendations=0;
                            $insert->save();

                            $insertUser=new User;
                             $insertUser->name=$value->nombre;
                             $insertUser->email=$value->email;
                             $insertUser->rol='PARTNER';
                             $insertUser->partner_p_id=$insert->id;
                             $insertUser->validation_code=$codigo;
                            $insertUser->save();

                            $countInserteds=$countInserteds+1;
                        }else{
                            $countNotInserteds=$countNotInserteds+1;
                        }

                        $data = json_encode(
                              [
                                 'from' => [
                                    'email' => 'info@afteroclub.com',
                                 ],
                                 'personalizations' => [
                                      array("to" => array([
                                                'email' => $to
                                              ]),
                                          'dynamic_template_data' => [
                                                'name' => ucfirst($value->nombre).' '.ucfirst($value->apellido),
                                                'url' => $urlcorreo,
                                                'user' => $value->email
                                      ])
                                 ],
                                 'template_id' => 'd-b1cb62e637164d368cfb65b3132186ed',  
                              ]
                        );

                        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                                ['body' => $data]
                        );  
                    }

                Session::flash('message', 'De un total de "'.$data->count().'" fueron insertados "'.$countInserteds.'" y "'.$countNotInserteds.'" no fueron agregados debido a que algun Socio ya se encuentra registrado con la misma Cedula o Correo Electronico.'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('partners'); 
            }
        }
    }

    public function searchRecommenders(Request $request){
        $partners=PartnerRecomendations::select('*')
        ->join('partners','partners.p_id','partner_recomendations.partner_recommender_id')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('partner_recommended_id',$request->id)
        ->get();

        if ($request->ajax()) {
            return view('renders.searchRecommenders',['partners' => $partners])->render(); 
        }
    }

    public function editPartner(Request $request){
        try {

           $update=Partners::where('p_id',$request->p_id)
           ->update(['p_identification' => $request->identification,
                     'p_name' => ucfirst($request->name),
                     'p_lastname' => ucfirst($request->lastname),
                     'p_sex' => $request->sex,
                     'p_date_of_birth' => $request->dateofbirth,
                     'p_email' => $request->email,
                     'citie_c_id' => $request->citieEdit,
                     'p_citie_name' => $request->othercitie,
                     'p_membership_code' => $request->identification,
                     'p_cellphone' => $request->telephone,
                     'p_school' => $request->school,
                     'p_civil_status' => $request->civilstatus,
                     'universitie_u_id' => $request->universityEdit,
                     'p_university_name' => $request->otheruniversitie,
                     'p_profession' => $request->profession,
                     'p_company' => $request->company,
                     'p_position' => $request->position]);

           Session::flash('message', 'Socio Editado Correctamente'); 
           Session::flash('alert-class', 'alert-success');
           return redirect('partners'); 

        } catch (Exception $e) {
            Session::flash('message', 'Ocurrio un error inesperado'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partners'); 
        }
    }

    public function editMembershipToPartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['membership_m_id'=>$request->membership]);

        if($update==1){
            Session::flash('message', 'Membresia Editada Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partners'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partners'); 
        }
    }

    public function activePartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['p_status'=>'ACTIVO',
                  'p_activation_date'=>date('d/m/Y')]);

        if($update==1){
            Session::flash('message', 'Socio Activado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partners'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partners'); 
        }
    }

    public function disabledPartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['p_status'=>'PENDIENTE_POR_ACTIVACION']);

        if($update==1){
            Session::flash('message', 'Socio Desactivado Correctamente'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partners'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partners'); 
        }
    }

    public function deletePartner(Request $request){
        $delete = Partners::where('p_id',$request->p_id)
        ->delete();

        $delete2 = User::where('partner_p_id',$request->p_id)
        ->delete();

        $delete3 = PartnerRecomendations::where('partner_recommended_id',$request->p_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Socio Eliminado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partners'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partners'); 
        }
    }

    public function editQuiantityRecommendations(Request $request){
        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')->get();

        if($quantity_recommendations->isEmpty()){
            $insert = new ContentPage;
            $insert->cp_description='quantity_recommendations';
            $insert->cp_content=$request->quantity;
            $save=$insert->save();

            if($save==1){
                Session::flash('message', 'Cantidad Agregada Correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('partners'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partners'); 
            }
        }else{
            $update=ContentPage::where('cp_description','quantity_recommendations')
            ->update(['cp_content' => $request->quantity]);

            if($update==1){
                Session::flash('message', 'Cantidad Editada Correctamente'); 
                Session::flash('alert-class', 'alert-info');
                return redirect('partners'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partners'); 
            }
        }
    }

    public function memberships(){
        $memberships=Memberships::paginate('5');

        $benefits=Benefits::select('*')
        ->join('benefit_categories','benefit_categories.bc_id','=','benefits.benefit_categories_bc_id')
        ->get();

        $categories=BenefitCategories::all();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.memberships')
        ->with('memberships',$memberships)
        ->with('benefits',$benefits)
        ->with('categories',$categories)
        ->with('logo',$logo);
    }

    public function addMembership(Request $request){
        $datos=Memberships::all();
        if($datos->isEmpty()){
            $datos=Memberships::where('m_name',$request->name)->get();

            if($datos->isEmpty()){
                $insert = new Memberships;
                $insert->m_name=ucfirst($request->name);
                $insert->m_default='default';
                $save=$insert->save();
                $membership_id=$insert->id;

                foreach ($request->benefits as $key => $value) {
                    $insertbm = new BenefitsMemberships;
                    $insertbm->membership_m_id=$membership_id;
                    $insertbm->benefit_b_id=$value;
                    $insertbm->save();
                }

                if($save==1){
                    Session::flash('message', 'Membresia Agregada Correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('memberships'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('memberships'); 
                }
            }else{
                Session::flash('message', 'Ya existe una Membresia cargada con este nombre.');
                Session::flash('alert-class', 'alert-danger');
                return redirect('memberships'); 
            }
        }
        else{
            $datos=Memberships::where('m_name',$request->name)->get();
            if($datos->isEmpty()){
                $insert = new Memberships;
                $insert->m_name=ucfirst($request->name);
                $save=$insert->save();
                $membership_id=$insert->id;

                foreach ($request->benefits as $key => $value) {
                    $insertbm = new BenefitsMemberships;
                    $insertbm->membership_m_id=$membership_id;
                    $insertbm->benefit_b_id=$value;
                    $insertbm->save();
                }

                if($save==1){
                    Session::flash('message', 'Membresia Agregada Correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('memberships'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('memberships'); 
                }
            }else{
                Session::flash('message', 'Ya existe una Membresia cargada con este nombre.');
                Session::flash('alert-class', 'alert-danger');
                return redirect('memberships'); 
            }
        }
    }

    public function editMembership(Request $request){
        $update = Memberships::where('m_id',$request->m_id)
        ->update(['m_name'=>ucfirst($request->name)]);

        if($update==1){
            Session::flash('message', 'Membresía Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('memberships'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('memberships'); 
        }
    }

    public function benefitsToAdd(Request $request){
        $benefits_memberships=BenefitsMemberships::where('membership_m_id',$request->id)->get();

        $benefits=Benefits::all();

        foreach ($benefits as $key => $value) {
           foreach ($benefits_memberships as $key => $valor) {
               if($value->b_id == $valor->benefit_b_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }

        return $benefits;
    }

    public function setDefaultMembership(Request $request){
        $update = Memberships::where('m_id','<>',$request->m_id)
        ->update(['m_default'=> null]);

        $update = Memberships::where('m_id',$request->m_id)
        ->update(['m_default'=> "default"]);

        if($update==1){
            Session::flash('message', 'Membresía Colocada por Default Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('memberships'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('memberships'); 
        }
    }

    public function editBenefitMembership(Request $request){
        $partners=Partners::select('*')
        ->join('memberships','partners.membership_m_id','=','memberships.m_id')
        ->where('memberships.m_id',$request->m_id)
        ->get();
        $benefits=collect();

        try {
            $delete = BenefitsMemberships::where('membership_m_id',$request->m_id)
            ->delete();

            foreach ($request->benefits as $key => $value) {

                $benefit=Benefits::where('b_id',$value)
                ->first();
                
                $benefits->push($benefit);

                $insertbm = new BenefitsMemberships;
                $insertbm->membership_m_id=$request->m_id;
                $insertbm->benefit_b_id=$value;
                $insertbm->save();

            }

            $client = new Client([
              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
              'headers' => [ 'Accept' => 'application/json',
                             'Authorization' => 'Bearer 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',
                             'Content-Type' => 'application/json' ]
            ]);

            foreach ($partners as $key => $value) {
                $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $value->p_email
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $value->p_name.' '.$value->p_lastname,
                                        'benefits' => $benefits->all()
                              ])
                         ],
                         'template_id' => 'd-d823e9f40b174c58a43943c7631a6860',  
                      ]
                );

                $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                        ['body' => $data]
                );  
            }

            Session::flash('message', 'Beneficios Administrados correctamente'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('memberships'); 

        } catch (Exception $e) {
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('memberships');
        }
    }

    public function deleteMembership(Request $request){
        $delete = Memberships::where('m_id',$request->m_id)
        ->delete();

        $delete2 = BenefitsMemberships::where('membership_m_id',$request->m_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Membresía Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('memberships'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('memberships'); 
        }
    }

    public function concepts(){
        $concepts=Concepts::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.concepts')
        ->with('concepts',$concepts)
        ->with('logo',$logo);
    }

    public function addConcept(Request $request){
        $insert = new Concepts;
        $insert->ct_description=$request->description;
        $insert->ct_concept=ucfirst($request->concept);
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Concepto agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('concepts'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('concepts'); 
        }
    }

    public function deleteConcept(Request $request){
        $delete = Concepts::where('ct_id',$request->ct_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Categoria Eliminada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('concepts'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('concepts'); 
        }
    }

    public function users(){
        /*$users=User::where('rol','ADMIN')
        ->where('id','<>',\Auth::user()->id)
        ->orWhere('rol','SECURITY')
        ->paginate('5');*/
        $users=User::paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('admin.users')
        ->with('users',$users)
        ->with('logo',$logo);
    }

    public function addUser(Request $request){
        $user=User::where('email',$request->email)->get();
        if(!$user->isEmpty()){
            Session::flash('message', 'Ya existe un usuario con este correo electronico registrado.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('users'); 
        }elseif(strlen($request->password) < 8){
          Session::flash('message', 'La contraseña debe tener minimo "8" caracteres.'); 
          Session::flash('alert-class', 'alert-info');
          return redirect('users'); 
        }else{
            $insert = new User;
                $insert->name=ucfirst($request->name);
                $insert->email=$request->email;
                $insert->password=bcrypt($request->password);
                $insert->rol=$request->rol;
            $save=$insert->save();

            if($save==1){
                Session::flash('message', 'Usuario Agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('users'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('users'); 
            }
        }
    }

    public function editUser(Request $request){
        $user=User::where('email',$request->email)->get();

        if($user[0]->rol=='PARTNER'){
            $update = User::where('id',$request->u_id)
            ->update(['name' => $request->name,
                  'email' => $request->email,
                  'rol' => 'PARTNER']);
        }else{
            $update = User::where('id',$request->u_id)
            ->update(['name' => $request->name,
                  'email' => $request->email,
                  'rol' => $request->rol]);
        }

        

        if($update==1){
            Session::flash('message', 'Usuario Editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('users'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('users'); 
        }

    }

    public function deleteUser(Request $request){
        $delete = User::where('id',$request->id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Usuario Eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('users'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('users'); 
        }
    }
}
