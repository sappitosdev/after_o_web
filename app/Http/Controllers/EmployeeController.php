<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Cities;
use App\Events;
use App\EventCategories;
use App\EventGuests;
use App\EventMemberships;
use App\EventMembershipGuests;
use App\EventCities;
use App\EventCitiesGuests;
use App\EventSexs;
use App\EventSexsGuests;
use App\EventTypes;
use App\Universities;
use App\Guests;
use App\Benefits;
use App\BenefitCategories;
use App\Partners;
use App\PartnerRecomendations;
use App\Memberships;
use App\ContentPage;
use App\BenefitsMemberships;
use App\Concepts;
use App\User;
use App\BenefitsCities;
use Session;
use Redirect;
use Mail;
use Excel;
use GuzzleHttp\Client;
class EmployeeController extends Controller
{
    public function employee(){
    	$logo=ContentPage::where('cp_description','logo')->get();
        return view('employee')
        ->with('logo',$logo);
    }

    public function administrationlogin(){
        $title=ContentPage::where('cp_description','the_login_title')->get();
        $text=ContentPage::where('cp_description','the_login_text')->get();
        $image=ContentPage::where('cp_description','the_login_image')->get();
        $link=ContentPage::where('cp_description','the_login_link_redirection')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('employee.administrationlogin')
        ->with('title',$title)
        ->with('text',$text)
        ->with('image',$image)
        ->with('link',$link)
        ->with('logo',$logo);
    }

	public function editLoginSection(Request $request){
        $text=ContentPage::where('cp_description','the_login_title')
        ->orWhere('cp_description','the_login_text')
        ->orWhere('cp_description','the_login_image')
        ->get();
        if($text->isEmpty()){

            $insert = new ContentPage;
            $insert->cp_description='the_login_title';
            $insert->cp_content=$request->title;
            $save=$insert->save();

            $insert2 = new ContentPage;
            $insert2->cp_description='the_login_text';
            $insert2->cp_content=$request->text;
            $save2=$insert2->save();

            $image=$request->file('image')->store('public');

            $insert3 = new ContentPage;
            $insert3->cp_description='the_login_image';
            $insert3->cp_content=$image;
            $save3=$insert3->save();

            $insert4 = new ContentPage;
            $insert4->cp_description='the_login_link_redirection';
            $insert4->cp_content=$request->link;
            $save4=$insert4->save();

            if($save==1 || $save2==1 || $save3==1 || $save4==1){
                Session::flash('message', 'Sección Agregada correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('administrationlogine');
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('administrationlogine');
            }
        }else{
            if($request->file('image')){
                $datos=ContentPage::where('cp_description','the_login_image')->get();

                $imagen = $datos[0]['cp_content'];

                Storage::delete($imagen);

                $image=$request->file('image')->store('public');

                $update=ContentPage::where('cp_description','the_login_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_login_text')
                ->update(['cp_content' => $request->text]);
                $update3=ContentPage::where('cp_description','the_login_image')
                ->update(['cp_content' => $image]);
                $update4=ContentPage::where('cp_description','the_login_link_redirection')
                ->update(['cp_content' => $request->link]);

                if($update==true && $update2==true && $update3==true && $update4==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('administrationlogine'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('administrationlogine'); 
                }
            }else{
                $update=ContentPage::where('cp_description','the_login_title')
                ->update(['cp_content' => $request->title]);
                $update2=ContentPage::where('cp_description','the_login_text')
                ->update(['cp_content' => $request->text]);

                $update3=ContentPage::where('cp_description','the_login_link_redirection')
                ->update(['cp_content' => $request->link]);


                if($update==true || $update2==true || $update3==true){
                    Session::flash('message', 'Sección Editada correctamente'); 
                    Session::flash('alert-class', 'alert-info');
                    return redirect()->route('administrationlogine'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->route('administrationlogine'); 
                }
            }
            
        }
    }	

    public function slider(){
        $sliders=ContentPage::where('cp_description','slider_principal')
        ->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('employee.slider')
        ->with('sliders',$sliders)
        ->with('logo',$logo);
    }

    public function addBanner(Request $request){
        $path = $request->file('banner')->store('public');
        
        $insert = new ContentPage;
            $insert->cp_description='slider_principal';
            $insert->cp_content=$path;
            $insert->cp_title=$request->title;
            $insert->cp_text=$request->text;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Banner agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('slidere'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('slidere'); 
        }
    }

    public function editBanner(Request $request){
        if($request->banner){
            $datos=ContentPage::where('cp_id',$request->cp_id)->get();

            $imagen = $datos[0]['cp_content'];

            Storage::delete($imagen);

            $path = $request->file('banner')->store('public');
            
            $update=ContentPage::where('cp_id',$request->cp_id)
            ->update(['cp_content' => $path,
                      'cp_title' => $request->title,
                      'cp_text' => $request->text]);

            if($update==1){
                Session::flash('message', 'Banner editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slidere'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slidere'); 
            } 
        }else{
            $update=ContentPage::where('cp_id',$request->cp_id)
            ->update(['cp_title' => $request->title,
                      'cp_text' => $request->text]);

            if($update==1){
                Session::flash('message', 'Banner editado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slidere'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slidere'); 
            } 
        }
    }

    public function deleteBanner(Request $request){
        $slideractive=ContentPage::where('cp_description','slider_principal')
        ->first();
        $sliders=ContentPage::where('cp_description','slider_principal')
        ->where('cp_id','<>',$slideractive->cp_id)
        ->get();
        if(!$slideractive->isEmpty and $sliders->isEmpty()){
            Session::flash('message', 'El Banner no puede ser eliminado debido a ser el unico activo en el elemento.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('slidere'); 
        }else{
            $datos=ContentPage::where('cp_id',$request->cp_id)->get();

            $imagen = $datos[0]['cp_content'];

            Storage::delete($imagen);
            
            $delete=ContentPage::where('cp_id',$request->cp_id)
            ->delete();

            if($delete==1){
                Session::flash('message', 'Banner eliminado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('slidere'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('slidere'); 
            }
        }
    }

    public function allies(){
        $allies=ContentPage::where('cp_description','allie')
        ->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('employee.allies')
        ->with('allies',$allies)
        ->with('logo',$logo);
    }

    public function addAllie(Request $request){
        $path = $request->file('allie')->store('public');
        
        $insert = new ContentPage;
        $insert->cp_description='allie';
        $insert->cp_content=$path;
        $save=$insert->save();

        if($save==1){
            Session::flash('message', 'Aliado agregado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('alliese'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('alliese'); 
        }
    }

    public function editAllie(Request $request){
        $datos=ContentPage::where('cp_id',$request->cp_id)->get();

        $imagen = $datos[0]['cp_content'];

        Storage::delete($imagen);

        $path = $request->file('allie')->store('public');
        
        $update=ContentPage::where('cp_id',$request->cp_id)
        ->update(['cp_content' => $path]);

        if($update==1){
            Session::flash('message', 'Aliado editado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('alliese'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('alliese'); 
        }
    }

    public function deleteAllie(Request $request){
        $datos=ContentPage::where('cp_id',$request->cp_id)->get();

        $imagen = $datos[0]['cp_content'];

        Storage::delete($imagen);
        
        $delete=ContentPage::where('cp_id',$request->cp_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Aliado eliminado correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('alliese'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('alliese'); 
        }
    }

    public function memberships(){
        $memberships=Memberships::paginate('5');

        $benefits=Benefits::select('*')
        ->join('benefit_categories','benefit_categories.bc_id','=','benefits.benefit_categories_bc_id')
        ->get();

        $categories=BenefitCategories::all();
        $logo=ContentPage::where('cp_description','logo')->get();
        return view('employee.memberships')
        ->with('memberships',$memberships)
        ->with('benefits',$benefits)
        ->with('categories',$categories)
        ->with('logo',$logo);
    }

    public function addMembership(Request $request){
        $datos=Memberships::all();
        if($datos->isEmpty()){
            $datos=Memberships::where('m_name',$request->name)->get();

            if($datos->isEmpty()){
                $insert = new Memberships;
                $insert->m_name=ucfirst($request->name);
                $insert->m_default='default';
                $save=$insert->save();
                $membership_id=$insert->id;

                foreach ($request->benefits as $key => $value) {
                    $insertbm = new BenefitsMemberships;
                    $insertbm->membership_m_id=$membership_id;
                    $insertbm->benefit_b_id=$value;
                    $insertbm->save();
                }

                if($save==1){
                    Session::flash('message', 'Membresia Agregada Correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('membershipse'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('membershipse'); 
                }
            }else{
                Session::flash('message', 'Ya existe una Membresia cargada con este nombre.');
                Session::flash('alert-class', 'alert-danger');
                return redirect('membershipse'); 
            }
        }
        else{
            $datos=Memberships::where('m_name',$request->name)->get();
            if($datos->isEmpty()){
                $insert = new Memberships;
                $insert->m_name=ucfirst($request->name);
                $save=$insert->save();
                $membership_id=$insert->id;

                foreach ($request->benefits as $key => $value) {
                    $insertbm = new BenefitsMemberships;
                    $insertbm->membership_m_id=$membership_id;
                    $insertbm->benefit_b_id=$value;
                    $insertbm->save();
                }

                if($save==1){
                    Session::flash('message', 'Membresia Agregada Correctamente'); 
                    Session::flash('alert-class', 'alert-success');
                    return redirect('membershipse'); 
                }else{
                    Session::flash('message', 'Ocurrio un error'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect('membershipse'); 
                }
            }else{
                Session::flash('message', 'Ya existe una Membresia cargada con este nombre.');
                Session::flash('alert-class', 'alert-danger');
                return redirect('membershipse'); 
            }
        }
    }

    public function editMembership(Request $request){
        $update = Memberships::where('m_id',$request->m_id)
        ->update(['m_name'=>ucfirst($request->name)]);

        if($update==1){
            Session::flash('message', 'Membresía Editada correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('membershipse'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('membershipse'); 
        }
    }

    public function benefitsToAdd(Request $request){
        $benefits_memberships=BenefitsMemberships::where('membership_m_id',$request->id)->get();

        $benefits=Benefits::all();

        foreach ($benefits as $key => $value) {
           foreach ($benefits_memberships as $key => $valor) {
               if($value->b_id == $valor->benefit_b_id){
                    $value->checked=true;
                    break;
               }else{
                    $value->checked=false;
               }
           }
        }

        return $benefits;
    }

    public function editBenefitMembership(Request $request){
        try {
            $delete = BenefitsMemberships::where('membership_m_id',$request->m_id)
            ->delete();

            foreach ($request->benefits as $key => $value) {
                $insertbm = new BenefitsMemberships;
                $insertbm->membership_m_id=$request->m_id;
                $insertbm->benefit_b_id=$value;
                $insertbm->save();
            }

            Session::flash('message', 'Beneficios Administrados correctamente'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('membershipse'); 
        } catch (Exception $e) {
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('membershipse');
        }
    }

    public function partners(){
        $partners=Partners::select('*')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->paginate('10');
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $civil_status=Concepts::where('ct_description','ESTADO_CIVIL')->get();
        $logo=ContentPage::where('cp_description','logo')->get();
        $cities=Cities::all();
        $universities=Universities::all();
        $memberships=Memberships::all();
        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')->get();
        return view('employee.partners')
        ->with('partners',$partners)
        ->with('logo',$logo)
        ->with('sexs',$sexs)
        ->with('civil_status',$civil_status)
        ->with('cities',$cities)
        ->with('memberships',$memberships)
        ->with('universities',$universities)
        ->with('quantity_recommendations',$quantity_recommendations);
    }

    public function addPartner(Request $request){
       $cedula=Partners::where('p_identification',$request->identification)->get();
       $email=Partners::where('p_email',$request->email)->get();
       $membership=Memberships::where('m_default','default')->get();
       $email_user=User::where('email',$request->email)->get();
       if(!$cedula->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con esta identificación.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partnerse'); 
       }elseif(!$email->isEmpty() || !$email_user->isEmpty()){
            Session::flash('message', 'Ya existe un usuario registrado con este correo electronico.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partnerse'); 
       }elseif($membership->isEmpty()){
            Session::flash('message', 'Debe cargar una Membresía y colocarla como "DEFAULT" para poder realizar la creacion de un socio.'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partnerse');
       }else{
            try {

               $codigo = str_random(25);
               $urlcorreo = url('/accountPartnerActivation/').'/'.$codigo;
               $to = $request->email;
               $insert=new Partners;
                 $insert->p_identification=$request->identification;
                 $insert->p_name=ucfirst($request->name);
                 $insert->p_lastname=ucfirst($request->lastname);
                 $insert->p_sex=$request->sex;
                 $insert->p_date_of_birth=$request->dateofbirth;
                 $insert->p_email=$request->email;
                 $insert->citie_c_id=$request->citie;
                 $insert->p_citie_name=ucfirst($request->othercitie);
                 $insert->membership_m_id=$membership[0]->m_id;
                 $insert->p_membership_code=$request->identification;
                 $insert->p_cellphone=$request->telephone;
                 $insert->p_school=ucfirst($request->school);
                 $insert->p_civil_status=$request->civilstatus;
                 $insert->universitie_u_id=$request->university;
                 $insert->p_university_name=ucfirst($request->otheruniversitie);
                 $insert->p_profession=ucfirst($request->profession);
                 $insert->p_company=ucfirst($request->company);
                 $insert->p_position=ucfirst($request->position);
                 $insert->p_status='PENDIENTE_POR_ACTIVACION';
                 $insert->p_number_recommendations=0;
               $insert->save();

               $insertUser=new User;
                 $insertUser->name=$request->name;
                 $insertUser->email=$request->email;
                 $insertUser->rol='PARTNER';
                 $insertUser->partner_p_id=$insert->id;
                 $insertUser->validation_code=$codigo;
               $insertUser->save();

               Mail::send('emails.passwordToPartner',['url' => $urlcorreo],function($message) use ($to){
                   $message->subject('Confirmación de Email');
                   $message->to( $to, 'After O');
               });

               Session::flash('message', 'Su usuario fue registrado exitosamente, revise su bandeja de entrada y verifique el correo electronico con las instrucciones para seguir su proceso.'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('partnerse'); 

            } catch (Exception $e) {
                Session::flash('message', 'Ocurrio un error inesperado'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partnerse'); 
            }
        }
    }

    public function addPatnerToExcel(Request $request){
        $client = new Client([
              'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
              'headers' => [ 'Accept' => 'application/json',
                             'Authorization' => 'Bearer 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',
                             'Content-Type' => 'application/json' ]
        ]);

        $countInserteds=0;
        $countNotInserteds=0;
        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = Excel::load($path,function($reader){})->get();

            if (!$data->isEmpty() && $data->count()){
                $citie_other=Cities::where('c_citie','Otra')->get();
                $universitie_other=Universities::where('u_universitie','Otra')->get();
                $membership=Memberships::where('m_default','default')->get();
                    foreach ($data as $key => $value) {
                        $codigo = str_random(25);
                        $urlcorreo = url('/accountPartnerActivation/').'/'.$codigo;
                        $to = $value->email;
                        $citie=Cities::where('c_citie',$value->residencia_actual)->get();
                        $universitie=Universities::where('u_universitie',$value->nombre_de_la_universidad_donde_te_graduaste)->get();

                        $partner=Partners::where('p_identification',$value->numero_de_cedula)
                        ->orWhere('p_email',$value->email)
                        ->get();

                        if($partner->isEmpty()){
                            $insert=new Partners;
                             $insert->p_identification=$value->numero_de_cedula;
                             $insert->p_name=ucfirst($value->nombre);
                             $insert->p_lastname=ucfirst($value->apellido);
                             
                                 if($value->sexo == 'Hombre'){
                                    $insert->p_sex='Masculino';
                                 }elseif($value->sexo == 'Mujer'){
                                    $insert->p_sex='Femenino';
                                 }
                                 
                             $insert->p_date_of_birth=$value->fecha_de_nacimiento->format('m/d/Y');
                             $insert->p_email=$value->email;

                                 if($citie->isEmpty()){
                                    $insert->citie_c_id=$citie_other[0]->c_id;
                                    $insert->p_citie_name=ucfirst($value->residencia_actual);
                                 }else{
                                    $insert->citie_c_id=$citie[0]->c_id;
                                    $insert->p_citie_name=null;
                                 }
                             
                             $insert->membership_m_id=$membership[0]->m_id;
                             $insert->p_membership_code=$value->numero_de_cedula;
                             $insert->p_cellphone=$value->telephone;
                             $insert->p_school=ucfirst($value->nombre_del_colegio_donde_te_graduaste);
                             $insert->p_civil_status=$value->estado_civil;

                                if($universitie->isEmpty()){
                                    $insert->universitie_u_id=$universitie_other[0]->u_id;
                                    $insert->p_university_name=ucfirst($value->nombre_de_la_universidad_donde_te_graduaste);
                                 }else{
                                    $insert->universitie_u_id=$universitie[0]->u_id;
                                    $insert->p_university_name=null;
                                 }
                             
                             $insert->p_profession=ucfirst($value->profesion);
                             $insert->p_company=ucfirst($value->tienes_carrera_universitaria);
                             $insert->p_position=ucfirst($value->cargo_que_desempenas_actualmente
                             );
                             $insert->p_status='PENDIENTE_POR_ACTIVACION';
                             $insert->p_number_recommendations=0;
                            $insert->save();

                            $insertUser=new User;
                             $insertUser->name=$value->nombre;
                             $insertUser->email=$value->email;
                             $insertUser->rol='PARTNER';
                             $insertUser->partner_p_id=$insert->id;
                             $insertUser->validation_code=$codigo;
                            $insertUser->save();

                            $countInserteds=$countInserteds+1;
                        }else{
                            $countNotInserteds=$countNotInserteds+1;
                        }

                        $data = json_encode(
                              [
                                 'from' => [
                                    'email' => 'info@afteroclub.com',
                                 ],
                                 'personalizations' => [
                                      array("to" => array([
                                                'email' => $to
                                              ]),
                                          'dynamic_template_data' => [
                                                'name' => ucfirst($value->nombre).' '.ucfirst($value->apellido),
                                                'url' => $urlcorreo,
                                                'user' => $value->email
                                      ])
                                 ],
                                 'template_id' => 'd-b1cb62e637164d368cfb65b3132186ed',  
                              ]
                        );

                        $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                                ['body' => $data]
                        );  
                    }

                Session::flash('message', 'De un total de "'.$data->count().'" fueron insertados "'.$countInserteds.'" y "'.$countNotInserteds.'" no fueron agregados debido a que algun Socio ya se encuentra registrado con la misma Cedula o Correo Electronico.'); 
               Session::flash('alert-class', 'alert-success');
               return redirect('partnerse'); 
            }
        }
    }

    public function searchRecommenders(Request $request){
        $partners=PartnerRecomendations::select('*')
        ->join('partners','partners.p_id','partner_recomendations.partner_recommender_id')
        ->join('memberships','memberships.m_id','=','partners.membership_m_id')
        ->where('partner_recommended_id',$request->id)
        ->get();

        if ($request->ajax()) {
            return view('renders.searchRecommenders',['partners' => $partners])->render(); 
        }
    }

    public function editPartner(Request $request){
        try {

           $update=Partners::where('p_id',$request->p_id)
           ->update(['p_identification' => $request->identification,
                     'p_name' => ucfirst($request->name),
                     'p_lastname' => ucfirst($request->lastname),
                     'p_sex' => $request->sex,
                     'p_date_of_birth' => $request->dateofbirth,
                     'p_email' => $request->email,
                     'citie_c_id' => $request->citieEdit,
                     'p_citie_name' => $request->othercitie,
                     'p_membership_code' => $request->identification,
                     'p_cellphone' => $request->telephone,
                     'p_school' => $request->school,
                     'p_civil_status' => $request->civilstatus,
                     'universitie_u_id' => $request->universityEdit,
                     'p_university_name' => $request->otheruniversitie,
                     'p_profession' => $request->profession,
                     'p_company' => $request->company,
                     'p_position' => $request->position]);

           Session::flash('message', 'Socio Editado Correctamente'); 
           Session::flash('alert-class', 'alert-success');
           return redirect('partnerse'); 

        } catch (Exception $e) {
            Session::flash('message', 'Ocurrio un error inesperado'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partnerse'); 
        }
    }

    public function editMembershipToPartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['membership_m_id'=>$request->membership]);

        if($update==1){
            Session::flash('message', 'Membresia Editada Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partnerse'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partnerse'); 
        }
    }

    public function activePartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['p_status'=>'ACTIVO',
                  'p_activation_date'=>date('d/m/Y')]);

        if($update==1){
            Session::flash('message', 'Socio Activado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partnerse'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partnerse'); 
        }
    }

    public function disabledPartner(Request $request){
        $update = Partners::where('p_id',$request->p_id)
        ->update(['p_status'=>'PENDIENTE_POR_ACTIVACION']);

        if($update==1){
            Session::flash('message', 'Socio Desactivado Correctamente'); 
            Session::flash('alert-class', 'alert-info');
            return redirect('partnerse'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partnerse'); 
        }
    }

    public function deletePartner(Request $request){
        $delete = Partners::where('p_id',$request->p_id)
        ->delete();

        $delete2 = User::where('partner_p_id',$request->p_id)
        ->delete();

        $delete3 = PartnerRecomendations::where('partner_recommended_id',$request->p_id)
        ->delete();

        if($delete==1){
            Session::flash('message', 'Socio Eliminado Correctamente'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('partnerse'); 
        }else{
            Session::flash('message', 'Ocurrio un error'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('partnerse'); 
        }
    }

    public function editQuiantityRecommendations(Request $request){
        $quantity_recommendations=ContentPage::where('cp_description','quantity_recommendations')->get();

        if($quantity_recommendations->isEmpty()){
            $insert = new ContentPage;
            $insert->cp_description='quantity_recommendations';
            $insert->cp_content=$request->quantity;
            $save=$insert->save();

            if($save==1){
                Session::flash('message', 'Cantidad Agregada Correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('partnerse'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partnerse'); 
            }
        }else{
            $update=ContentPage::where('cp_description','quantity_recommendations')
            ->update(['cp_content' => $request->quantity]);

            if($update==1){
                Session::flash('message', 'Cantidad Editada Correctamente'); 
                Session::flash('alert-class', 'alert-info');
                return redirect('partnerse'); 
            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('partnerse'); 
            }
        }
    }

    public function events(){
        $events=Events::select('*')
        ->join('event_types','event_types.et_id','=','events.event_type_et_id')
        ->paginate('5');
        $logo=ContentPage::where('cp_description','logo')->get();
        $eventTypes=EventTypes::all();
        $eventCategories=EventCategories::all();
        $memberships=Memberships::all();
        $sexs=Concepts::where('ct_description','SEXO')->get();
        $cities=Cities::all();
        return view('admin.events')
        ->with('events',$events)
        ->with('eventTypes',$eventTypes)
        ->with('eventCategories',$eventCategories)
        ->with('memberships',$memberships)
        ->with('sexs',$sexs)
        ->with('cities',$cities)
        ->with('logo',$logo);
    }

    public function addEvent(Request $request){
        $client = new Client([
                 'base_uri' => 'https://api.sendgrid.com/v3/mail/send',
                 'headers' => [ 'Accept' => 'application/json',
                 'Authorization' => 'Bearer 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',
                 'Content-Type' => 'application/json' ]
        ]);

        $partners=Partners::all();

        if($request->promotionalpicture){
            $promitionalPicture=$request->file('promotionalpicture')->store('public');

            $insertEvent = new Events;
                $insertEvent->e_name=ucfirst($request->name);
                $insertEvent->event_type_et_id=$request->eventType;
                $insertEvent->event_categorie_ec_id=$request->eventCategorie;
                $insertEvent->e_date_start=$request->dateStart;
                $insertEvent->e_time_start=$request->timeStart;
                $insertEvent->e_date_end=$request->dateEnd;
                $insertEvent->e_time_end=$request->timeEnd;
                $insertEvent->e_short_description=$request->shortdescription;
                $insertEvent->e_description=$request->description;
                $insertEvent->e_address_location=$request->address_location;
                $insertEvent->e_latitud=$request->latitud;
                $insertEvent->e_longitud=$request->longitud;
                $insertEvent->e_total_guests=$request->totalguests;
                $insertEvent->e_guest_member_question=$request->invxmiembros;
                $insertEvent->e_minimum_age=$request->minimumage;
                $insertEvent->e_number_guests_x_members=$request->numberGuestxMember;
                $insertEvent->e_minimum_age_x_members=$request->minimumagemembers;
                $insertEvent->e_picture=$promitionalPicture;
            $saveEvent=$insertEvent->save();
            $lastEventId=$insertEvent->id;

            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$lastEventId;
                    $insertEventMembership->save();
            }

            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$lastEventId;
                    $insertEventSexs->save();
            }

            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$lastEventId;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$lastEventId;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$lastEventId;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$lastEventId;
                        $insertEventCities->save();
                }

            }

            if($saveEvent==1){
                $date=Carbon::parse($insertEvent->e_date_start);
                $time=Carbon::parse($insertEvent->e_time_start);
                foreach ($partners as $key => $value) {

                    if($date->month==1 || $date->month =='1')
                        $month='ENE';
                    elseif($date->month== 2 || $date->month =='2')
                        $month='FEB';
                    elseif($date->month==3 || $date->month =='3')
                        $month='MAR';
                    elseif($date->month==4 || $date->month =='4')
                        $month='ABR';
                    elseif($date->month==5 || $date->month =='5')
                        $month='MAY';
                    elseif($date->month==6 || $date->month =='6')
                        $month='JUN';
                    elseif($date->month==7 || $date->month =='7')
                        $month='JUL';
                    elseif($date->month==8 || $date->month =='8')
                        $month='AGTO';
                    elseif($date->month==9 || $date->month =='9')
                        $month='SEPT';
                    elseif($date->month== 10 || $date->month =='10')
                        $month='OCT';
                    elseif($date->month== 11 || $date->month =='11')
                        $month='NOV';
                    elseif($date->month== 12 || $date->month =='12')
                        $month='DIC';

                    $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $value->p_email
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $value->p_name.' '.$value->p_lastname,
                                        'address' => $insertEvent->e_address_location,
                                        'month' => $date->month,
                                        'day' => $date->day,
                                        'time' => $time->format('g:i A')
                              ])
                         ],
                         'template_id' => 'd-25f6ce0cface4a2f9ac67e6242b27756',  
                      ]
                    );

                    $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                            ['body' => $data]
                    );

                }

                Session::flash('message', 'Evento Agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('eventse'); 

            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('eventse'); 
            }
        }else{
            $insertEvent = new Events;
                $insertEvent->e_name=ucfirst($request->name);
                $insertEvent->event_type_et_id=$request->eventType;
                $insertEvent->event_categorie_ec_id=$request->eventCategorie;
                $insertEvent->e_date_start=$request->dateStart;
                $insertEvent->e_time_start=$request->timeStart;
                $insertEvent->e_date_end=$request->dateEnd;
                $insertEvent->e_time_end=$request->timeEnd;
                $insertEvent->e_short_description=$request->shortdescription;
                $insertEvent->e_description=$request->description;
                $insertEvent->e_address_location=$request->address_location;
                $insertEvent->e_latitud=$request->latitud;
                $insertEvent->e_longitud=$request->longitud;
                $insertEvent->e_total_guests=$request->totalguests;
                if(!$request->minimumage){
                    $insertEvent->e_minimum_age=0;
                }else{
                    $insertEvent->e_minimum_age=$request->minimumage;
                }         
                $insertEvent->e_number_guests_x_members=$request->numberGuestxMember;
                if(!$request->minimumagemembers){
                    $insertEvent->e_minimum_age_x_members=0;
                }else{
                    $insertEvent->e_minimum_age_x_members=$request->minimumagemembers;
                }     
            $saveEvent=$insertEvent->save();
            $lastEventId=$insertEvent->id;

            foreach ($request->memberships as $key => $value) {
                $insertEventMembership = new EventMemberships;
                    $insertEventMembership->membership_m_id=$value;
                    $insertEventMembership->event_e_id=$lastEventId;
                    $insertEventMembership->save();
            }

            foreach ($request->sexs as $key => $value) {
                $insertEventSexs = new EventSexs;
                    $insertEventSexs->sexo=$value;
                    $insertEventSexs->event_e_id=$lastEventId;
                    $insertEventSexs->save();
            }

            foreach ($request->cities as $key => $value) {
                $insertEventCities = new EventCities;
                    $insertEventCities->citie_c_id=$value;
                    $insertEventCities->event_e_id=$lastEventId;
                    $insertEventCities->save();
            }

            if($request->membershipsguest){
                foreach ($request->membershipsguest as $key => $value) {
                    $insertEventMembership = new EventMembershipGuests;
                        $insertEventMembership->membership_m_id=$value;
                        $insertEventMembership->event_e_id=$lastEventId;
                        $insertEventMembership->save();
                }
            }

            if($request->sexsmembers){
                foreach ($request->sexsmembers as $key => $value) {
                    $insertEventSexs = new EventSexsGuests;
                        $insertEventSexs->sexo=$value;
                        $insertEventSexs->event_e_id=$lastEventId;
                        $insertEventSexs->save();
                }

            }

            if($request->citiesmembers){
                foreach ($request->citiesmembers as $key => $value) {
                    $insertEventCities = new EventCitiesGuests;
                        $insertEventCities->citie_c_id=$value;
                        $insertEventCities->event_e_id=$lastEventId;
                        $insertEventCities->save();
                }

            }

            if($saveEvent==1){
                $date=Carbon::parse($insertEvent->e_date_start);
                $time=Carbon::parse($insertEvent->e_time_start);


                foreach ($partners as $key => $value) {

                    if($date->month==1 || $date->month =='1')
                        $month='ENE';
                    elseif($date->month== 2 || $date->month =='2')
                        $month='FEB';
                    elseif($date->month==3 || $date->month =='3')
                        $month='MAR';
                    elseif($date->month==4 || $date->month =='4')
                        $month='ABR';
                    elseif($date->month==5 || $date->month =='5')
                        $month='MAY';
                    elseif($date->month==6 || $date->month =='6')
                        $month='JUN';
                    elseif($date->month==7 || $date->month =='7')
                        $month='JUL';
                    elseif($date->month==8 || $date->month =='8')
                        $month='AGTO';
                    elseif($date->month==9 || $date->month =='9')
                        $month='SEPT';
                    elseif($date->month== 10 || $date->month =='10')
                        $month='OCT';
                    elseif($date->month== 11 || $date->month =='11')
                        $month='NOV';
                    elseif($date->month== 12 || $date->month =='12')
                        $month='DIC';

                    $data = json_encode(
                      [
                         'from' => [
                            'email' => 'info@afteroclub.com',
                         ],
                         'personalizations' => [
                              array("to" => array([
                                        'email' => $value->p_email
                                      ]),
                                  'dynamic_template_data' => [
                                        'name' => $value->p_name.' '.$value->p_lastname,
                                        'address' => $insertEvent->e_address_location,
                                        'month' => $month,
                                        'day' => $date->day,
                                        'time' => $time->format('g:i A')
                              ])
                         ],
                         'template_id' => 'd-25f6ce0cface4a2f9ac67e6242b27756',  
                      ]
                    );

                    $response = $client->post('https://api.sendgrid.com/v3/mail/send',
                            ['body' => $data]
                    );

                }

                Session::flash('message', 'Evento Agregado correctamente'); 
                Session::flash('alert-class', 'alert-success');
                return redirect('eventse'); 

            }else{
                Session::flash('message', 'Ocurrio un error'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect('eventse'); 
            }
        }       
    }
}
