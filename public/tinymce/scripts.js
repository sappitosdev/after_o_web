tinymce.init({ 
    selector:'.editorHTML',
    branding:false,
    color_picker_callback: function(callback, value) {
      callback('#FF00FF');
    },
    height:300,
    max_height: 300,
    plugins: "textcolor",
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
});