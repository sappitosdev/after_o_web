$('input:radio[name=universidad]').click(function(){
    if(this.value == 'Si'){
      $('#university').removeAttr('disabled');
      $('#profession').removeAttr('disabled');
    }else{
      $('#university').attr('disabled','disabled');
      $('#profession').attr('disabled','disabled');
      $('#university').val('');
      $('#profession').val('');
    }
});

$('input:radio[name=trabaja]').click(function(){
    if(this.value == 'Si'){
      $('#company').removeAttr('disabled');
      $('#position').removeAttr('disabled');
    }else{
      $('#company').attr('disabled','disabled');
      $('#position').attr('disabled','disabled');
      $('#company').val('');
      $('#position').val('');
    }
});

$('#citie').change(function(){
    var selected = $('select[name="citie"] option:selected').text();
    if(selected=='Otra'){
      $('#writeCitie').removeClass('hidden');
      $('#writeCitie input').attr('required','required');
    }else{
      $('#writeCitie').addClass('hidden');
      $('#writeCitie input').removeAttr('required');
      $('#writeCitie input').val('');
    }
});

$('#university').change(function(){
    var selected = $('select[name="university"] option:selected').text();
    if(selected=='Otra'){
      $('#writeUniversitie').removeClass('hidden');
      $('#writeUniversitie input').attr('required','required');
    }else{
      $('#writeUniversitie').addClass('hidden');
      $('#writeUniversitie input').removeAttr('required');
      $('#writeUniversitie input').val('');
    }
});