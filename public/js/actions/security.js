$('#checkGuests').submit(function(e){
	e.preventDefault();
	$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
	var data = $('#checkGuests').serializeArray();
	$.ajax(
	{
		method:"POST",
	    url: "checkGuests", 
	    beforeSend:function(){
	    	$('.loader').removeAttr('hidden');
	    },
	    success: function(data){
	    	//console.log(data);
	    	$('#data').html(data);
	    	$('.loader').attr('hidden','hidden');
	    },
	    data:data
	});
});
