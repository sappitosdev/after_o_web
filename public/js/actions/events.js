$('input:radio[name=redad]').click(function(){
    if(this.value == 'Si'){
      $('#minimumage').removeAttr('disabled');
      $('#minimumage').attr('required','required');
    }else{
      $('#minimumage').attr('disabled','disabled');
      $('#minimumage').val('');
      $('#minimumage').removeAttr('required');
    }
});

$('input:radio[name=invxmiembros]').click(function(){
    if(this.value == 'Si'){
    	$('#numberGuestXMember').removeAttr('disabled');
    	$('#membersguests').removeAttr('disabled');
    	$('#sexsmembers').removeAttr('disabled');
    	$('#citiesmembers').removeAttr('disabled');
    	$('#minimumagemembers').removeAttr('disabled');

    	$('#numberGuestXMember').attr('required','required');
    	$('#membersguests').attr('required','required');
    	$('#sexsmembers').attr('required','required');
    	$('#citiesmembers').attr('required','required');
    	$('#minimumagemembers').attr('required','required');

    	$('.selectpicker').selectpicker('refresh');
    }else{
    	$('#numberGuestXMember').attr('disabled','disabled');
    	$('#membersguests').attr('disabled','disabled');
    	$('#sexsmembers').attr('disabled','disabled');
    	$('#citiesmembers').attr('disabled','disabled');
    	$('#minimumagemembers').attr('disabled','disabled');

    	$('#numberGuestXMember').val('');
    	$('#membersguests').val('');
    	$('#sexsmembers').val('');
    	$('#citiesmembers').val('');
    	$('#minimumagemembers').val('');

    	$('#numberGuestXMember').removeAttr('required');
    	$('#membersguests').removeAttr('required');
    	$('#sexsmembers').removeAttr('required');
    	$('#citiesmembers').removeAttr('required');
    	$('#minimumagemembers').removeAttr('required');

    	$('.selectpicker').selectpicker('refresh');
    }
});

$('#deleteEvent').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #e_id').val(recipient)
});


$('#firmarInvitados').on('show.bs.modal', function (event) {
  var id=$('#e_id').val();
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax(
    {
      method:"POST",
      url: "/searchInvitedGuestsToEvent", 
      success: function(data){
        console.log(data);
        $('#guests').html(data)
        $('#e_id').val(id)
      },
      data:{id:id},
    });
});


