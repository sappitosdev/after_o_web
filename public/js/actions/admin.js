$('#disabledCitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #c_id').val(recipient)
});

$('#activeCitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #c_id').val(recipient)
});

$('#editCitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #citie').val(recipient.c_citie)
  modal.find('.modal-body #c_id').val(recipient.c_id)
});

$('#deleteCitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #c_id').val(recipient)
});

$('#editCategorie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #categorie').val(recipient.ec_categorie)
  modal.find('.modal-body #ec_id').val(recipient.ec_id)
});

$('#deleteCategorie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #ec_id').val(recipient)
});


$('#editType').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #type').val(recipient.et_type)
  modal.find('.modal-body #et_id').val(recipient.et_id)
});

$('#deleteType').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #et_id').val(recipient)
});


$('#editUniversitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #universitie').val(recipient.u_universitie)
  modal.find('.modal-body #u_id').val(recipient.u_id)
});

$('#deleteUniversitie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #u_id').val(recipient)
});


$('#editBenefitCategorie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')

  var image = recipient.bc_icon;
  var currentImage = image.replace("public/","")
  var modal = $(this)
  modal.find('.modal-body #categorie').val(recipient.bc_benefit_categorie)
  modal.find('.modal-body #currentIcon').attr('src','/storage/' + currentImage);
  modal.find('.modal-body #bc_id').val(recipient.bc_id)
});

$('#deleteBenefitCategorie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #bc_id').val(recipient)
});


$('#editBenefit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  //console.log(recipient);
  var modal = $(this)
  modal.find('.modal-body #b_id').val(recipient.b_id)
  modal.find('.modal-body #categorie').val(recipient.benefit_categories_bc_id)
  modal.find('.modal-body #name').val(recipient.b_name)
  modal.find('.modal-body #descripction').val(recipient.b_description)
  modal.find('.modal-body #shortDescripction').val(recipient.b_short_description)
  modal.find('.modal-body #citie').val(recipient.cities_c_id)
  modal.find('.modal-body #validity').val(recipient.b_validity)

  $.ajax(
  {
    url: "citiesToBenefit", 
    success: function(data){
      var test=[];
      $.each(data,function(e,v){
        test.push(v.citie_c_id);
      });
      modal.find('#cities').selectpicker('val', test);
    },
    data:{id:recipient.b_id},
  });

});

$('#deleteBenefit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #b_id').val(recipient)
});

$('#editBrandLogo').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #b_id').val(recipient)
});

$('#editPromotionalImage').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #b_id').val(recipient)
});


$('input:radio[name=universidad]').click(function(){
    if(this.value == 'Si'){
      $('#university').removeAttr('disabled');
      $('#profession').removeAttr('disabled');
    }else{
      $('#university').attr('disabled','disabled');
      $('#profession').attr('disabled','disabled');
      $('#university').val('');
      $('#profession').val('');
    }
});

$('input:radio[name=trabaja]').click(function(){
    if(this.value == 'Si'){
      $('#company').removeAttr('disabled');
      $('#position').removeAttr('disabled');
    }else{
      $('#company').attr('disabled','disabled');
      $('#position').attr('disabled','disabled');
      $('#company').val('');
      $('#position').val('');
    }
});
$('#editMembership').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #m_id').val(recipient.m_id)
  modal.find('.modal-body #name').val(recipient.m_name)
});

$('#addBenefit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #m_id').val(recipient);
  $.ajax(
  {
    url: "benefitsToAdd", 
    success: function(data){
      $('#benefitsToAdd').empty();
      $.each(data,function(e,v){
        if(v.checked==true){
          $("#benefitsToAdd").append('<option value='+v.b_id+' selected>'+v.b_name+'</option>');
          $("#benefitsToAdd").selectpicker("refresh");
        }else{
          $("#benefitsToAdd").append('<option value='+v.b_id+'>'+v.b_name+'</option>');
          $("#benefitsToAdd").selectpicker("refresh");
        }
      });
    },
    data:{id:recipient},
  });
});

$('#deleteMembership').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #m_id').val(recipient)
});

$('#deleteConcept').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #ct_id').val(recipient)
});

$('#editUser').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #u_id').val(recipient.id)
  modal.find('.modal-body #name').val(recipient.name)
  modal.find('.modal-body #email').val(recipient.email)
  modal.find('.modal-body #rol').val(recipient.rol)

  if(recipient.rol=='PARTNER'){
    modal.find('.modal-body #rol').attr('disabled','disabled');
  }else{
    modal.find('.modal-body #rol').removeAttr('disabled');
  }
});

$('#deleteUser').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #id').val(recipient)
});