$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$('#filter').change(function(){
	if(this.value=='Cedula'){
		$('#filterCedula').removeClass('hidden');
		$('#filterCorreo').addClass('hidden');
		$('#email').val('');
	}else if(this.value=='Email'){
		$('#filterCorreo').removeClass('hidden');
		$('#filterCedula').addClass('hidden');
		$('#identification').val('');
	}
});

$('#formFilterCedula').submit(function(e){
	  e.preventDefault();
	  var cedula=$('#identification').val();
	  
	  $.ajax(
	  {
	  	method:"POST",
	    url: "searchPartnerToRecommend", 
	    success: function(data){
	      if(data.length==0){
	      	$('#alerta').empty();
	      	$('#alerta').removeClass('hidden');
          $('#partner').empty();
          $('#filter').val('');
          $('#identification').val('');
          $('#filterCedula').addClass('hidden');
	      	$('#alerta').append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Eres el primer Miembro en recomendar a esta persona con este correo electrónico. Haz click en “Recomendar” para confirmar tu recomendación.</strong>');
	      	$('#btnRecomendar').addClass('hidden');
          $('#recommendedPartnerData').val(cedula);
          $('#type_data').val('cedula');
	      }else{
          $('#alerta').empty();
          $('#alerta').addClass('hidden');
	      	$('#partner').empty();
	      	$('#filter').val('');
	      	$('#identification').val('');
	      	$('#filterCedula').addClass('hidden');
	      	$('#btnRecomendar').removeClass('hidden');
	      	$('#partner').append('<hr><h4 class="text-center">'+data[0].p_name+' '+data[0].p_lastname+'</h4> <h5 class="text-center">identificación: '+data[0].p_identification+'</h5>');
	      	$('#recommendedPartnerData').val(data[0].p_identification);
          $('#type_data').val('cedula');
	      }

	    },
	    data:{cedula:cedula},
	  });
});

$('#filterCorreo').submit(function(e){
	  e.preventDefault();
	  var correo=$('#email').val();
	  $.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
	  $.ajax(
	  {
	  	method:"POST",
	    url: "searchPartnerToRecommend", 
	    success: function(data){
	      if(data.length==0){
	      	$('#alerta').empty();
          $('#alerta').removeClass('hidden');
          $('#partner').empty();
          $('#filter').val('');
          $('#email').val('');
          $('#filterCorreo').addClass('hidden');
	      	$('#alerta').append('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Eres el primer Miembro en recomendar a esta persona con este correo electrónico. Haz click en “Recomendar” para confirmar tu recomendación.</strong>');
	      	$('#btnRecomendar').removeClass('hidden');
          $('#recommendedPartnerData').val(correo);
          $('#type_data').val('correo');
	      }else{
	      	$('#alerta').empty();
          $('#alerta').addClass('hidden');
          $('#partner').empty();
	      	$('#filter').val('');
	      	$('#email').val('');
	      	$('#filterCorreo').addClass('hidden');
	      	$('#btnRecomendar').removeClass('hidden');
          if(data[0].p_name!=null && data[0].p_lastname!=null){
            $('#partner').append('<hr><h4 class="text-center">'+data[0].p_name+' '+data[0].p_lastname+'</h4> <h5 class="text-center">Correo Electronico: '+data[0].p_email+'</h5>');
          }else{
            $('#partner').append('<hr><h4 class="text-center">Usuario no ha completado sus datos.</h4> <h5 class="text-center">Correo Electronico: '+data[0].p_email+'</h5>');
          }
	      	
	      	$('#recommendedPartnerData').val(data[0].p_email);
          $('#type_data').val('correo');
	      }
	    },
	    data:{correo:correo},
	  });
});

$('input:radio[name=universidad]').click(function(){
    if(this.value == 'Si'){
      $('#university').removeAttr('disabled');
      $('#profession').removeAttr('disabled');
    }else{
      $('#university').attr('disabled','disabled');
      $('#profession').attr('disabled','disabled');
      $('#university').val('');
      $('#profession').val('');
    }
});

$('input:radio[name=trabaja]').click(function(){
    if(this.value == 'Si'){
      $('#company').removeAttr('disabled');
      $('#position').removeAttr('disabled');
    }else{
      $('#company').attr('disabled','disabled');
      $('#position').attr('disabled','disabled');
      $('#company').val('');
      $('#position').val('');
    }
});

$('#citie').change(function(){
    var selected = $('select[name="citie"] option:selected').text();
    if(selected=='Otra'){
      $('#writeCitie').removeClass('hidden');
      $('#writeCitie input').attr('required','required');
    }else{
      $('#writeCitie').addClass('hidden');
      $('#writeCitie input').removeAttr('required');
      $('#writeCitie input').val('');
    }
});

$('#university').change(function(){
    var selected = $('select[name="university"] option:selected').text();
    if(selected=='Otra'){
      $('#writeUniversitie').removeClass('hidden');
      $('#writeUniversitie input').attr('required','required');
    }else{
      $('#writeUniversitie').addClass('hidden');
      $('#writeUniversitie input').removeAttr('required');
      $('#writeUniversitie input').val('');
    }
});


$('#editPartner').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)

  modal.find('.modal-body #p_id').val(recipient.p_id)
  modal.find('.modal-body #identification').val(recipient.p_identification)
  modal.find('.modal-body #name').val(recipient.p_name)
  modal.find('.modal-body #lastname').val(recipient.p_lastname)
  modal.find('.modal-body #sex').val(recipient.p_sex)
  modal.find('.modal-body #lastname').val(recipient.p_lastname)
  modal.find('.modal-body #dateofbirth').val(recipient.p_date_of_birth)
  modal.find('.modal-body #email').val(recipient.p_email)
  modal.find('.modal-body #citieEdit').val(recipient.citie_c_id)
  modal.find('.modal-body #othercitie').val(recipient.p_citie_name)
  modal.find('.modal-body #civilstatus').val(recipient.p_civil_status)
  modal.find('.modal-body #telephone').val(recipient.p_cellphone)
  modal.find('.modal-body #civilstatus').val(recipient.p_civil_status)
  modal.find('.modal-body #school').val(recipient.p_school)
  modal.find('.modal-body #otheruniversitie').val(recipient.p_university_name)

  if(recipient.universitie_u_id != null && recipient.universitie_u_id != ''){
  	$('input[name=universidad]').filter('[value=Si]').prop('checked', true);
  	modal.find('.modal-body #universityEdit').val(recipient.universitie_u_id);
    modal.find('.modal-body #universityEdit').removeAttr('disabled');
  	modal.find('.modal-body #professionEdit').val(recipient.p_profession);
  	modal.find('.modal-body #professionEdit').removeAttr('disabled');
  }else{
  	$('input[name=universidad]').filter('[value=No]').prop('checked', true);
  	modal.find('.modal-body #universityEdit').val(recipient.universitie_u_id);
  	modal.find('.modal-body #professionEdit').val(recipient.p_profession);
  	modal.find('.modal-body #professionEdit').attr('disabled','disabled');
  	modal.find('.modal-body #universityEdit').attr('disabled','disabled');
  }

  if(recipient.p_company != null && recipient.p_company != ''){
  	$('input[name=trabaja]').filter('[value=Si]').prop('checked', true);
  	modal.find('.modal-body #companyEdit').val(recipient.p_company);
  	modal.find('.modal-body #companyEdit').removeAttr('disabled');
  	modal.find('.modal-body #positionEdit').val(recipient.p_position);
  	modal.find('.modal-body #positionEdit').removeAttr('disabled');
  }else{
  	$('input[name=trabaja]').filter('[value=No]').prop('checked', true);
  	modal.find('.modal-body #companyEdit').val(recipient.p_company);
  	modal.find('.modal-body #companyEdit').attr('disabled','disabled');
  	modal.find('.modal-body #positionEdit').val(recipient.p_position);
  	modal.find('.modal-body #positionEdit').attr('disabled','disabled');
  }

  $('input:radio[name=universidad]').change(function(){
      if(this.value == 'Si'){
        $('#universityEdit').val('');
        $('#professionEdit').val('');
        $('#universityEdit').removeAttr('disabled');
        $('#professionEdit').removeAttr('disabled');
      }else{
       $('#universityEdit').val('');
       $('#professionEdit').val('');
       $('#universityEdit').attr('disabled','disabled');
       $('#professionEdit').attr('disabled','disabled');
      }
  });

  $('input:radio[name=trabaja]').change(function(){
      if(this.value == 'Si'){
        $('#companyEdit').val('');
        $('#positionEdit').val('');
        $('#companyEdit').removeAttr('disabled');
        $('#positionEdit').removeAttr('disabled');
      }else{
       $('#companyEdit').val('');
       $('#positionEdit').val('');
       $('#companyEdit').attr('disabled','disabled');
       $('#positionEdit').attr('disabled','disabled');
      }
  });

  var selectedCitie = $('select[name="citieEdit"] option:selected').text();
    if(selectedCitie=='Otra'){
      $('#writeCitieEdit').removeClass('hidden');
      $('#writeCitieEdit input').attr('required','required');
    }else{
      $('#writeCitieEdit').addClass('hidden');
      $('#writeCitieEdit input').removeAttr('required');
      $('#writeCitieEdit input').val('');
    }

	var selectedUniversity = $('select[name="universityEdit"] option:selected').text();
    if(selectedUniversity=='Otra'){
      $('#writeUniversitieEdit').removeClass('hidden');
      $('#writeUniversitieEdit input').attr('required','required');
    }else{
      $('#writeUniversitieEdit').addClass('hidden');
      $('#writeUniversitieEdit input').removeAttr('required');
      $('#writeUniversitieEdit input').val('');
    }

});

$('#deletePartner').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #p_id').val(recipient)
});

$('#editMembershipToPartner').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  modal.find('.modal-body #p_id').val(recipient.p_id)
  modal.find('.modal-body #membership').val(recipient.m_id)
});

$('#searchRecommenders').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('whatever')
  var modal = $(this)
  	$.ajax(
	  {
	  	method:"POST",
	    url: "searchRecommenders", 
	    success: function(data){
	      $('#searchRecommendersTable').html(data);
	    },
	    data:{id:recipient},
	});
});