function readURL(input,idImg) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#'+idImg).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#banner").change(function() {
  readURL(this,'preview');
});
$("#banner2").change(function() {
	readURL(this,'preview2');
});

$("#allie").change(function() {
  readURL(this,'preview');
});

$("#allie2").change(function() {
  readURL(this,'preview2');
});


$('#deleteBanner').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #cp_id').val(recipient)
});

$('#editBanner').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var image = recipient.cp_content;
  var currentImage = image.replace("public/","")
  var modal = $(this)
  modal.find('.modal-body #cp_id').val(recipient.cp_id)
  modal.find('.modal-body #title').val(recipient.cp_title)
  modal.find('.modal-body #text').val(recipient.cp_text)
  modal.find('.modal-body #banneractual').attr('src','/storage/' + currentImage);
});


 $('#deleteAllie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #cp_id').val(recipient)
});

$('#editAllie').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var image = recipient.cp_content;
  var currentImage = image.replace("public/","")
  var modal = $(this);
  modal.find('.modal-body #cp_id').val(recipient.cp_id)
  modal.find('.modal-body #allieactual').attr('src','/storage/' + currentImage);
});

$('#editAd').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var image = recipient.cp_content;
  var currentImage = image.replace("public/","")
  var modal = $(this);
  modal.find('.modal-body #ad_id').val(recipient.cp_id)
  modal.find('.modal-body #link').val(recipient.cp_title)
  modal.find('.modal-body #adactual').attr('src','/storage/' + currentImage);
});

$('#deleteAd').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #ad_id').val(recipient)
});