$(document).ready(function(){
	$('#formConfirmAssitence').submit(function(e){
		e.preventDefault();
		var data=$('#formConfirmAssitence').serializeArray();
		$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});
		$.ajax(
		{
			method:"POST",
		    url: "checkConfirmAssitence", 
		    beforeSend:function(){
		    	$('.loader').removeAttr('hidden');
		    },
		    success: function(data){
		      $('#data').html(data);
	    	  $('.loader').attr('hidden','hidden');
		    },
		    data:data
		});
	});
});