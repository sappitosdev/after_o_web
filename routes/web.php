<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/email', function () {return view('emails.bookATable');});
Route::get('test', 'SecurityController@test')->name('test');

Route::get('/', 'LandingController@index')->name('/');

Route::get('/reset/{token}', 'ResetPasswordController@reset_password_home')->name('reset');

Route::post('/reset_password', ['uses' => 'ResetPasswordController@reset']);

Route::post('/reset_this_password', 'ResetPasswordController@reset_this_password')->name('reset_this_password');

Route::get('/public', 'LandingController@page')->name('public');
Route::post('contactEmail', ['uses' => 'LandingController@contactEmail']);
Route::get('/permissions', function () {return view('permissions');})->name('permissions');

Route::get('/membersregistration', 'LandingController@membersregistration')->name('membersregistration');
Route::post('register_account', ['uses' => 'LandingController@registerAccount']);

Route::get('/accountPartnerActivation/{code}', 'LandingController@accountPartnerActivation')->name('accountPartnerActivation');

Route::get('/accountPartnerData/{code}', 'PartnerController@accountPartnerData')->name('accountPartnerData');

Route::post('confirm_data_to_new_partner', ['uses' => 'LandingController@confirmPasswordNewPartner']);
Route::post('confirm_data_to_new_partner_recommended', ['uses' => 'PartnerController@confirmNewData']);

Route::get('/accountGuestComplete/{code}', 'LandingController@accountGuestComplete')->name('accountGuestComplete');
Route::post('confirmDataToGuest', ['uses' => 'LandingController@confirmDataToGuest']);

Route::middleware(['auth','CheckAdmin'])->group(function () {
	Route::get('/admin', 'AdminController@admin')->name('admin');
	/* ROUTES ADMIN PAGE*/
		Route::get('/admlogo', 'AdminPageController@admlogo')->name('admlogo');
		Route::post('edit_logo', ['uses' => 'AdminPageController@editLogo']);

		Route::get('/administrationlogin', 'AdminPageController@administrationlogin')->name('administrationlogin');
		Route::post('edit_login_section', ['uses' => 'AdminPageController@editLoginSection']);

		Route::get('/administrationlogin', 'AdminPageController@administrationlogin')->name('administrationlogin');
		Route::post('edit_login_section', ['uses' => 'AdminPageController@editLoginSection']);

		Route::get('/slider', 'AdminPageController@slider')->name('slider');
		Route::post('add_banner', ['uses' => 'AdminPageController@addBanner']);
		Route::post('edit_banner', ['uses' => 'AdminPageController@editBanner']);
		Route::post('delete_banner', ['uses' => 'AdminPageController@deleteBanner']);

		Route::get('/theclub', 'AdminPageController@theclub')->name('theclub');
		Route::post('edit_club_section', ['uses' => 'AdminPageController@editClubSection']);

		Route::get('/theclubdescription', 'AdminPageController@theclubdescription')->name('theclubdescription');
		Route::post('edit_club_description_section', ['uses' => 'AdminPageController@editClubSectionDescription']);

		Route::get('/membershipspage', 'AdminPageController@membershipspage')->name('membershipspage');
		Route::post('edit_membership_section', ['uses' => 'AdminPageController@editMembershipSection']);

		Route::get('/benefitspage', 'AdminPageController@benefitspage')->name('benefitspage');
		Route::post('edit_benefit_section', ['uses' => 'AdminPageController@editBenefitSection']);

		Route::get('/footertext', 'AdminPageController@footertext')->name('footertext');
		Route::post('edit_text', ['uses' => 'AdminPageController@editFooterText']);

		Route::get('/allies', 'AdminPageController@allies')->name('allies');
		Route::post('add_allie', ['uses' => 'AdminPageController@addAllie']);
		Route::post('edit_allie', ['uses' => 'AdminPageController@editAllie']);
		Route::post('delete_allie', ['uses' => 'AdminPageController@deleteAllie']);

		Route::get('/ads', 'AdminPageController@ads')->name('ads');
		Route::post('add_ad', ['uses' => 'AdminPageController@addAd']);
		Route::post('edit_ad', ['uses' => 'AdminPageController@editAd']);
		Route::post('delete_ad', ['uses' => 'AdminPageController@deleteAd']);

		Route::get('/adDetailEvent', 'AdminPageController@adDetailEvent')->name('adDetailEvent');
		Route::post('edit_ad_detail_event', ['uses' => 'AdminPageController@editAdDetailEvent']);

		Route::get('/addBannerPrincipalEvent', 'AdminPageController@addBannerPrincipalEvent')->name('addBannerPrincipalEvent');
		Route::post('edit_principal_banner_event', ['uses' => 'AdminPageController@editBannerPrincipalEvent']);
	/* END ROUTES ADMIN PAGE*/

	/* ROUTES ADMIN*/
		Route::get('/cities', 'AdminController@cities')->name('cities');
		Route::post('add_citie', ['uses' => 'AdminController@addCitie']);
		Route::post('disabled_citie', ['uses' => 'AdminController@disabledCitie']);
		Route::post('active_citie', ['uses' => 'AdminController@activeCitie']);
		Route::post('edit_citie', ['uses' => 'AdminController@editCitie']);
		Route::post('delete_citie', ['uses' => 'AdminController@deleteCitie']);

		Route::get('/eventcategories', 'AdminController@eventcategories')->name('eventcategories');
		Route::post('add_categorie', ['uses' => 'AdminController@addCategorie']);
		Route::post('edit_categorie', ['uses' => 'AdminController@editCategorie']);
		Route::post('delete_categorie', ['uses' => 'AdminController@deleteCategorie']);

		Route::get('/eventypes', 'AdminController@eventypes')->name('eventypes');
		Route::post('add_type', ['uses' => 'AdminController@addType']);
		Route::post('edit_type', ['uses' => 'AdminController@editType']);
		Route::post('delete_type', ['uses' => 'AdminController@deleteType']);

		Route::get('/events', 'AdminController@events')->name('events');
		Route::post('add_event', ['uses' => 'AdminController@addEvent']);
		Route::post('edit_event', ['uses' => 'AdminController@editEvent']);
		Route::post('delete_event', ['uses' => 'AdminController@deleteEvent']);
		Route::get('/editEvent/{id}', 'AdminController@editEventView')->name('editEvent');

		Route::get('/confirmassitence', 'AdminController@confirmassitence')->name('confirmassitence');
		Route::post('confirm_asistence_to_guest', ['uses' => 'AdminController@confirmAssitenceToGuest']);
		Route::post('/checkConfirmAssitence', 'AdminController@checkConfirmAssitence')->name('checkConfirmAssitence');

		Route::get('/universities', 'AdminController@universities')->name('universities');
		Route::post('add_universitie', ['uses' => 'AdminController@addUniversitie']);
		Route::post('edit_universitie', ['uses' => 'AdminController@editUniversitie']);
		Route::post('delete_universitie', ['uses' => 'AdminController@deleteUniversitie']);

		Route::get('/benefitcategories', 'AdminController@benefitcategories')->name('benefitcategories');
		Route::post('add_benefitcategorie', ['uses' => 'AdminController@addBenefitCategorie']);
		Route::post('edit_benefitcategorie', ['uses' => 'AdminController@editBenefitCategorie']);
		Route::post('delete_benefitcategorie', ['uses' => 'AdminController@deleteBenefitCategorie']);

		Route::get('/benefits', 'AdminController@benefits')->name('benefits');
		Route::post('add_benefit', ['uses' => 'AdminController@addBenefit']);
		Route::post('edit_benefit', ['uses' => 'AdminController@editBenefit']);
		Route::post('delete_benefit', ['uses' => 'AdminController@deleteBenefit']);
		Route::post('edit_brand_logo', ['uses' => 'AdminController@editBrandLogo']);
		Route::post('edit_promotional_image', ['uses' => 'AdminController@editPromotionalImage']);
		Route::get('/citiesToBenefit', 'AdminController@citiesToBenefit')->name('citiesToBenefit');

		Route::get('/partners', 'AdminController@partners')->name('partners');
		Route::post('edit_quiantity_recommendations', ['uses' => 'AdminController@editQuiantityRecommendations']);
		Route::post('add_partner', ['uses' => 'AdminController@addPartner']);
		Route::post('edit_partner', ['uses' => 'AdminController@editPartner']);
		Route::post('add_partner_to_excel', ['uses' => 'AdminController@addPatnerToExcel']);
		Route::post('active_partner', ['uses' => 'AdminController@activePartner']);
		Route::post('edit_membership_to_partner', ['uses' => 'AdminController@editMembershipToPartner']);
		Route::post('disabled_partner', ['uses' => 'AdminController@disabledPartner']);
		Route::post('delete_partner', ['uses' => 'AdminController@deletePartner']);
		Route::post('/searchRecommenders', 'AdminController@searchRecommenders')->name('searchRecommenders');

		Route::get('/memberships', 'AdminController@memberships')->name('memberships');
		Route::post('add_membership', ['uses' => 'AdminController@addMembership']);
		Route::post('edit_membership', ['uses' => 'AdminController@editMembership']);
		Route::post('delete_membership', ['uses' => 'AdminController@deleteMembership']);
		Route::get('/benefitsToAdd', 'AdminController@benefitsToAdd')->name('benefitsToAdd');
		Route::post('edit_benefit_membership', ['uses' => 'AdminController@editBenefitMembership']);
		Route::post('set_default', ['uses' => 'AdminController@setDefaultMembership']);

		Route::get('/concepts', 'AdminController@concepts')->name('concepts');
		Route::post('add_concept', ['uses' => 'AdminController@addConcept']);
		Route::post('edit_concept', ['uses' => 'AdminController@editConcept']);
		Route::post('delete_concept', ['uses' => 'AdminController@deleteConcept']);

		Route::get('/users', 'AdminController@users')->name('users');
		Route::post('add_user', ['uses' => 'AdminController@addUser']);
		Route::post('edit_user', ['uses' => 'AdminController@editUser']);
		Route::post('delete_user', ['uses' => 'AdminController@deleteUser']);
	/* END ROUTES ADMIN*/
});

Route::middleware(['auth','CheckPartner'])->group(function () {
	Route::get('/partner', 'PartnerController@partner')->name('partner');
	Route::post('/searchPartnerToRecommend', 'PartnerController@searchPartnerToRecommend')->name('searchPartnerToRecommend');
	Route::post('partner_recommend', ['uses' => 'PartnerController@partner_recommended']);

	Route::post('partner_invited', ['uses' => 'PartnerController@invitedEvent']);

	Route::get('/partner/benefits', 'PartnerController@benefits')->name('partnerBenefits');
	Route::get('/partner/benefit/{id}', 'PartnerController@benefitDetail')->name('partnerBenefitsDetail');

	Route::get('/partner/events', 'PartnerController@events')->name('partnerEvents');
	Route::get('/partner/event/{id}', 'PartnerController@eventDetail')->name('partnerEventsDetail');

	Route::get('/profile', 'PartnerController@profile')->name('profile');
	Route::post('edit_profile', ['uses' => 'PartnerController@editProfile']);
	Route::post('edit_profile_picture', ['uses' => 'PartnerController@editProfilePicture']);

	Route::post('/searchInvitedGuestsToEvent', 'PartnerController@searchInvitedGuestsToEvent')->name('searchInvitedGuestsToEvent');

	Route::post('delete_invitation_guest', ['uses' => 'PartnerController@deleteInvitationGuest']);
	Route::post('boot_a_table', ['uses' => 'PartnerController@bookATable']);

	Route::get('/recomendedPartner', 'PartnerController@recomendedPartner')->name('recomendedPartner');

	Route::get('/changepassword', 'PartnerController@changepassword')->name('changepassword');
	Route::post('changepasswordtopartner', ['uses' => 'PartnerController@changepasswordToPartner']);
});

Route::middleware(['auth','CheckSecurity'])->group(function () {
	Route::get('/security', 'SecurityController@security')->name('security');
	Route::post('/checkGuests', 'SecurityController@checkGuests')->name('checkGuests');
	Route::post('confirm_assitant', ['uses' => 'SecurityController@confirmAssitant']);
});

Route::middleware(['auth','CheckEmployee'])->group(function () {
	/* ROUTES EMPLOYEE */
		Route::get('/employee', 'EmployeeController@employee')->name('employee');

		Route::get('/administrationlogine', 'EmployeeController@administrationlogin')->name('administrationlogine');
		Route::post('edit_login_section_e', ['uses' => 'EmployeeController@editLoginSection']);

		Route::get('/slidere', 'EmployeeController@slider')->name('slidere');
		Route::post('add_banner_e', ['uses' => 'EmployeeController@addBanner']);
		Route::post('edit_banner_e', ['uses' => 'EmployeeController@editBanner']);
		Route::post('delete_banner_e', ['uses' => 'EmployeeController@deleteBanner']);

		Route::get('/alliese', 'EmployeeController@allies')->name('alliese');
		Route::post('add_allie_e', ['uses' => 'EmployeeController@addAllie']);
		Route::post('edit_allie_e', ['uses' => 'EmployeeController@editAllie']);
		Route::post('delete_allie_e', ['uses' => 'EmployeeController@deleteAllie']);

		Route::get('/eventse', 'EmployeeController@events')->name('eventse');
		Route::post('add_evente', ['uses' => 'EmployeeController@addEvent']);

		Route::get('/citiese', 'EmployeeController@cities')->name('citiese');
		Route::post('add_citie_e', ['uses' => 'EmployeeController@addCitie']);
		Route::post('disabled_citie_e', ['uses' => 'EmployeeController@disabledCitie']);
		Route::post('active_citie_e', ['uses' => 'EmployeeController@activeCitie']);
		Route::post('edit_citie_e', ['uses' => 'EmployeeController@editCitie']);
		Route::post('delete_citie_e', ['uses' => 'EmployeeController@deleteCitie']);
		
		Route::get('/universitiese', 'EmployeeController@universities')->name('universitiese');
		Route::post('add_universitie_e', ['uses' => 'EmployeeController@addUniversitie']);
		Route::post('edit_universitie_e', ['uses' => 'EmployeeController@editUniversitie']);
		Route::post('delete_universitie_e', ['uses' => 'EmployeeController@deleteUniversitie']);
		
		Route::get('/partnerse', 'EmployeeController@partners')->name('partnerse');
		Route::post('edit_quiantity_recommendations_e', ['uses' => 'EmployeeController@editQuiantityRecommendations']);
		Route::post('add_partner_e', ['uses' => 'EmployeeController@addPartner']);
		Route::post('edit_partner_e', ['uses' => 'EmployeeController@editPartner']);
		Route::post('add_partner_to_excel_e', ['uses' => 'EmployeeController@addPatnerToExcel']);
		Route::post('active_partner_e', ['uses' => 'EmployeeController@activePartner']);
		Route::post('edit_membership_to_partner_e', ['uses' => 'EmployeeController@editMembershipToPartner']);
		Route::post('disabled_partner_e', ['uses' => 'EmployeeController@disabledPartner']);
		Route::post('delete_partner_e', ['uses' => 'EmployeeController@deletePartner']);
		//Route::post('/searchRecommenders', 'EmployeeController@searchRecommenders')->name('searchRecommenders');

		Route::get('/membershipse', 'EmployeeController@memberships')->name('membershipse');
		Route::post('add_membership_e', ['uses' => 'EmployeeController@addMembership']);
		Route::post('edit_membership_e', ['uses' => 'EmployeeController@editMembership']);
		Route::get('/benefitsToAdde', 'EmployeeController@benefitsToAdd')->name('benefitsToAdde');
		Route::post('edit_benefit_membership_e', ['uses' => 'EmployeeController@editBenefitMembership']);
	/* END ROUTES EMPLOYEE */
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
