<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AFTER O</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('onepage/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('onepage/css/freelancer.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('onepage/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    

    @if($partner[0]->p_status == 'ACTIVO')
        <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    @if(!$logo[0]->isEmpty)
                        <a class="navbar-brand" href="#page-top">
                          <img class="logo-nav" src="{{Storage::url($logo[0]->cp_content)}}">
                        </a>
                    @endif
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="page-scroll">
                            <a href="#beneficios">BENEFICIOS</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#eventos">EVENTOS</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#invitar">INVITAR A EVENTO</a>
                        </li>
                        <li class="page-scroll">
                            <a href="#recomendar">RECOMENDAR SOCIO</a>
                        </li>
                        <li >
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-power-off"></i> SALIR
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <div class="container-fluid">

          @if(Session::has('message'))
              <div class="alert alert-fixed {{ Session::get('alert-class') }} alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
          @endif

          
              <section >
                <div class="row">
                  <div class="col-md-12">
                    @if($slideractive)
                        <div class="hero-section hero bg-primary carousel-custom" style="background:url( {{Storage::url($slideractive->cp_content)}} )">
                            @if($partner[0]->p_picture == null)
                                <img src="http://via.placeholder.com/200x200" class="img-circle">
                            @else
                                <img src="http://via.placeholder.com/200x200" class="img-circle">
                            @endif
                            <h1>{{$partner[0]->p_name}} {{$partner[0]->p_lastname}}</h1>
                            <h3>Membresía {{$partner[0]->m_name}} - {{$partner[0]->p_membership_code}}</h3>
                        </div>
                    @else
                        <div class="hero-section hero bg-primary carousel-custom">
                            @if($partner[0]->p_picture == null)
                                <img src="http://via.placeholder.com/200x200" class="img-circle">
                            @else
                                <img src="http://via.placeholder.com/200x200" class="img-circle">
                            @endif
                            <h1>{{$partner[0]->p_name}} {{$partner[0]->p_lastname}}</h1>
                            <h3>Membresía {{$partner[0]->m_name}} - {{$partner[0]->p_membership_code}}</h3>
                        </div>
                    @endif
                    </div>
                </div>
              </section>

          <!-- Beneficios Section -->
          <section id="beneficios">
              <div class="row">
                <div class="col-md-12">
                    <div class="hero-section bg-secondary">
                        <h1>Beneficios</h1>
                    </div>
                </div>
                @foreach($benefits as $benefit)
                    <div class="col-md-4">
                        <div class="hero-section hero bg-primary">
                            <h3>{{$benefit->bc_benefit_categorie}}</h3>
                            <img src="{{Storage::url($benefit->b_brand_logo)}}" class="brand-logo">
                            <h3>{{$benefit->b_name}}</h3>
                            <h5>{{$benefit->b_description}}</h5>
                        </div>
                    </div>
                @endforeach
              </div>
          </section>

          <section>
              <div class="row">
                  <div class="col-md-12">
                    <div class="hero-section bg-secondary">
                        <h1>Eventos</h1>
                    </div>
                </div>
              </div>
          </section>

          <!-- Eventos Section -->
          <section id="eventos">
              <div class="row">
                @foreach($events as $event)
                    <div class="col-md-4">
                        <div class="hero-section hero bg-primary">
                            <h1>{{$event->e_name}}</h1>
                            <h5>Fecha: {{$event->e_date_start}}</h5>
                            <h5>Hora: {{$event->e_time_start}}</h5>
                            <h5> {{$event->e_short_description}}</h5>
                            <!--<div class="text-center">
                                <a href="" class="btn btn-primary">Ver Más</a>   
                            </div>-->
                        </div>
                    </div>
                @endforeach
              </div>
          </section>

          <!-- Invitar Section -->
          <section id="invitar">
              <div class="row">
                <div class="col-md-12">
                    <div class="hero-section-no-flex hero bg-secondary">
                        <h1 class="text-center">Invitar a un evento</h1>
                        <div class="col-md-4 col-md-offset-4">
                            <form action="{{action('PartnerController@invitedEvent')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Evento: <span class="required">*</span></label>
                                    <select class="form-control" name="event">
                                        <option disabled selected value="">Seleccione un evento</option>
                                        @foreach($events as $event)
                                            <option value="{{$event->e_id}}">{{$event->e_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Ingrese Correo Electronico: <span class="required">*</span></label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                                <button type="submit" class="btn btn-primary btn-principal center-block">Invitar</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>
          </section>

          <!-- Recomendar Section -->
          <section id="recomendar">
              <div class="row">
                <div class="col-md-12">
                    <div class="hero-section-no-flex hero bg-primary">
                        <h1 class="text-center">Recomendar a un socio.</h1>
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <label>Buscar por:</label>
                                <select class="form-control" name="filter" id="filter">
                                    <option selected disabled>Seleccione un filtro de busqueda</option>
                                    <option value="Cedula">Cedula</option>
                                    <option value="Email">Correo Electronico</option>
                                </select>
                            </div>
                            <div class="form-group hidden" id="filterCedula">
                                <form id="formFilterCedula" method="POST">
                                    <label class="text-left">Cedula:</label>
                                    <input type="text" name="identification" id="identification" class="form-control" required pattern="[0-9]+">
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-secundario center-block">Buscar </button>
                                </form>
                            </div>
                            <div class="form-group hidden" id="filterCorreo" method="POST">
                                <form id="formFilterCorreo" method="POST">
                                    <label class="text-left">Correo Electronico:</label>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-secundario center-block">Buscar </button>
                                </form>
                            </div>
                            <div class="alert alert-info hidden" id="alerta"></div>
                            <div id="partner"></div>
                            <button type="button" id="btnRecomendar" class="btn center-block btn-primary btn-secundario hidden" data-toggle="modal" data-target="#inviteConfirmation" data-whatever="{{$partner[0]->p_id}}">Recomendar</button>
                        </div>
                    </div>
                </div>
              </div>

              <div class="modal fade" id="inviteConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <form action="{{action('PartnerController@partnerRecommend')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Recomendar Socio</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="modal-body">
                            <h5><strong>Confirme que desea recomendar este socio.</strong></h5>
                            <input type="hidden" name="recommendedPartnerData" id="recommendedPartnerData">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary btn-principal">Recomendar</button>
                          </div>
                        </form>
                    </div>
                  </div>
                </div>
          </section>
        </div>

        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-12">
                            @if(!$logo[0]->isEmpty)
                                <img class="img-responsive logo-footer text-center center-block" src="{{Storage::url($logo[0]->cp_content)}}">
                            @endif
                            <h3>AFTER O</h3>
                            @if(!$footer_text->isEmpty())
                                <p class="text-left">{!! $footer_text[0]->cp_content !!}</p>
                            @endif
                            <a href="https://www.instagram.com/afterocaracas/" class="btn-social btn-outline"><i class="fa fa-fw fa-instagram"></i></a>
                            <a href="mailto:contacto@afteroclub.com" class="btn-social btn-outline"><i class="fa fa-fw fa-envelope"></i></a>    
                            <p>contacto@afteroclub.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; Your Website 2016
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
            <a class="btn btn-primary" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    @elseif(($partner[0]->p_number_recommendations >= $quantity_recommendations[0]->cp_content) && $partner[0]->p_status == 'PENDIENTE_POR_ACTIVACION')
            <div class="container">
                <div class="text-center">
                    <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}">
                </div>
                <h3 class="text-uppercase text-center">Su usuario se encuentra limitado debido a alguna situación, por favor comuniquese con nosotros</h3>
                <hr>
                <h4 class="text-uppercase text-center">Presione <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Click
                </a> aqui para volver.</h4>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                    {{ csrf_field() }}
                </form>
                <div class="text-center login-footer">
                    <p class="text-muted">AFTER O © 2018</p>
                </div>
            </div>
    @else
        <div class="container">
            <div class="text-center">
                <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}">
            </div>
            <h3 class="text-uppercase text-center">Su usuario aun se encuentra en proceso de Activación debido a no poseer el total de recomendaciones solicitadas.</h3>
            <hr>
            <h5 class="text-center">
                Para activar su usuario debe poseer 
                <span class="required">"{{$quantity_recommendations[0]->cp_content}}"</span> recomendaciones.
            </h5>
            <h5 class="text-center">De las cuales posee 
                <span class="required">"{{$partner[0]->p_number_recommendations}}"</span>
            </h5>
            
            @if(!$partner_recomendations->isEmpty())
                <hr>
                <h5 class="text-center">Socios que te recomendarion:</h5>
                @foreach($partner_recomendations as $pr)
                    <h6 class="text-center">{{$pr->p_name}} {{$pr->p_lastname}}</h6>
                @endforeach
            @endif
            <hr>
            <h4 class="text-uppercase text-center">Presione <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Click
            </a> aqui para volver.</h4>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                {{ csrf_field() }}
            </form>
            <div class="text-center login-footer">
                <p class="text-muted">AFTER O © 2018</p>
            </div>
        </div>
    @endif
    <!-- Footer -->
    

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    

    <!-- jQuery -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('onepage/js/freelancer.min.js')}}"></script>
    <script src="{{asset('owlcarousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/actions/partners.js')}}"></script>
    <!-- Start of HubSpot Embed Code -->
     <script type=“text/javascript” id=“hs-script-loader” async defer src=“//js.hs-scripts.com/4936461.js”></script>
    <!-- End of HubSpot Embed Code -->
</body>

</html>