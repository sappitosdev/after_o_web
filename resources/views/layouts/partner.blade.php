<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AFTER O</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('onepage/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('onepage/css/freelancer.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('onepage/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- jQuery -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Start of HubSpot Embed Code -->
     <script type=“text/javascript” id=“hs-script-loader” async defer src=“//js.hs-scripts.com/4936461.js”></script>
    <!-- End of HubSpot Embed Code -->

</head>

<body id="page-top" class="index">
    <!-- Navigation -->
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    @if(!$logo[0]->isEmpty)
                        <a class="navbar-brand" href="{{route('partnerBenefits')}}">
                          <img class="logo-nav" src="{{Storage::url($logo[0]->cp_content)}}">
                        </a>
                    @endif
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
   
                        <li>
                            <a href="{{route('partner')}}">INICIO</a>
                        </li>
                        <li>
                            <a href="{{route('partnerBenefits')}}">BENEFICIOS</a>
                        </li>
                        <li>
                            <a href="{{route('partnerEvents')}}">EVENTOS</a>
                        </li>
                        <li>
                            <a href="{{route('recomendedPartner')}}">RECOMENDAR CANDIDATO</a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-fw fa-user"></i> Usuario
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="{{route('profile')}}"><i class="fa fa-fw fa-edit"></i>  Editar Perfil</a></li>
                              <li><a href="{{route('changepassword')}}"><i class="fa fa-fw fa-edit"></i>  Cambiar Clave</a></li>
                                <li >
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fa fa-fw fa-power-off"></i> SALIR
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

    
    <div class="container-fluid">
        @if(Session::has('message'))
              <div class="alert alert-fixed {{ Session::get('alert-class') }} alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{ Session::get('message') }}</strong>
              </div>
        @endif

        @yield('content')
    </div>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-6">
                      @if(!$logo->isEmpty())
                          @if(!$logo[0]->isEmpty)
                              <img class="img-responsive logo-footer text-center center-block" src="{{Storage::url($logo[0]->cp_content)}}">
                          @endif
                      @endif
                      @if(!$footer_title->isEmpty())
                          <h3>{{$footer_title[0]->cp_content}}</h3>
                      @endif
                      @if(!$footer_text->isEmpty())
                          <p class="text-left">{!! $footer_text[0]->cp_content !!}</p>
                      @endif
                      <a href="https://www.instagram.com/afterocaracas/" class="btn-social btn-outline"><i class="fa fa-fw fa-instagram"></i></a>
                      <a href="mailto:contacto@afteroclub.com" class="btn-social btn-outline"><i class="fa fa-fw fa-envelope"></i></a>    
                      <p>contacto@afteroclub.com</p>
                  </div>
                    <div class="footer-col col-md-6">
                        <h3>Contactanos !</h3>
                        <form>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Apellido">
                            </div>
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="5" placeholder="Mensaje"></textarea>
                            </div>
                            <button class="btn btn-primary bg-primary btn-lg">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Your Website 2016
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('onepage/js/freelancer.min.js')}}"></script>
    <script src="{{asset('owlcarousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/actions/partners.js')}}"></script>
    <script src="{{asset('js/actions/events.js')}}"></script>
    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:4
                }
            }
        });
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $( function() {
        var now=new Date(); 
        $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1950:"+now
        });
      } );
    </script>
    
</body>

</html>