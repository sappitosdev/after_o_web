<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AFTER O</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
    <div class="container">
            @if(!$logo->isEmpty())
                <div class="text-center">
                    <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}" class="img-responsive">
                </div>
            @endif
            @if(!$user->isEmpty())
                @if($user[0]->p_identification == '')
                    <h2 class="text-center">Verifique su contraseña y Datos</h2>
                    <div class="row">
                        
                            @if(Session::has('message'))
                                  <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('message') }}</strong>
                                  </div>
                            @endif
                            <form action="{{ action('PartnerController@confirmNewData') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cedula: <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="identification" class="form-control" Añadido a la lista de deseos pattern="[0-9]+" maxlength="8">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre(s): <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="name" class="form-control text-capitalize" Añadido a la lista de deseos pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Apellido(s): <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="lastname" class="form-control text-capitalize" Añadido a la lista de deseos pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sexo: <span class="Añadido a la lista de deseos">*</span></label>
                                        <select class="form-control text-capitalize" name="sex" Añadido a la lista de deseos>
                                            <option selected disabled value="">Selecciona un sexo</option>
                                            @foreach($sexs as $sex)
                                                <option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha de nacimiento: <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="dateofbirth" class="form-control datepicker" Añadido a la lista de deseos id="date1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ciudad: <span class="Añadido a la lista de deseos">*</span></label>
                                        <select class="form-control text-capitalize" name="citie" id="citie" Añadido a la lista de deseos>
                                            <option selected disabled value="">Selecciona una ciudad</option>
                                            @foreach($cities as $citie)
                                                <option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group hidden" id="writeCitie">
                                        <label>Escriba la Ciudad <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="othercitie" class="form-control text-capitalize">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Estado Civil: <span class="Añadido a la lista de deseos">*</span></label>
                                        <select class="form-control text-capitalize" name="civilstatus" Añadido a la lista de deseos>
                                            <option selected disabled value="">Selecciona un estado civil</option>
                                            @foreach($civil_status as $cs)
                                                <option value="{{$cs->ct_concept}}">{{$cs->ct_concept}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono: <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="telephone" class="form-control" maxlength="11" pattern="[0-9]+">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Colegio:</label>
                                        <input type="text" name="school" class="form-control text-capitalize">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label>Carrera Universitaria? </label>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="universidad"  value="Si">Si
                                          </label>
                                          <label>
                                            <input type="radio" name="universidad" value="No" checked>No
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Universidad </label>
                                        <select name="university" id="university" class="form-control text-capitalize" disabled>
                                            <option selected disabled value="">Selecciona una universidad</option>
                                            @foreach($universities as $universitie)
                                                <option value="{{$universitie->u_id}}">{{$universitie->u_universitie}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group hidden" id="writeUniversitie">
                                        <label>Escriba la Universidad <span class="Añadido a la lista de deseos">*</span></label>
                                        <input type="text" name="otheruniversitie" class="form-control text-capitalize">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Profesión </label>
                                        <input type="text" name="profession" id="profession" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label>Trabaja? </label>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="trabaja" value="Si">Si
                                          </label>
                                          <label>
                                            <input type="radio" name="trabaja" value="No" checked>No
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Empresa </label>
                                        <input type="text" name="company" id="company" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cargo </label>
                                        <input type="text" name="position" id="position" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Contraseña (Minimo 8 Caracteres)</label>
                                        <input id="password" type="password" class="form-control" name="password" Añadido a la lista de deseos>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password-confirm">Confirmar Contraseña (Minimo 8 Caracteres)</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" Añadido a la lista de deseos>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <input type="hidden" name="user_id" value="{{$user[0]->id}}">
                            <input type="hidden" name="p_id" value="{{$user[0]->p_id}}">
                            <button type="submit" class="btn btn-lg btn-secundario btn-login">CONFIRMAR</button>
                        </form>
                        
                    </div>
                @else
                    <h2 class="text-center">Verifique su contraseña</h2>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            @if(Session::has('message'))
                                  <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('message') }}</strong>
                                  </div>
                            @endif
                            <form class="form-login" method="POST" action="{{ action('LandingController@confirmPasswordNewPartner') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="password">Contraseña (Minimo 8 Caracteres)</label>
                                    <input id="password" type="password" class="form-control" name="password" Añadido a la lista de deseos>
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm">Confirmar Contraseña (Minimo 8 Caracteres)</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" Añadido a la lista de deseos>
                                </div>
                                <input type="hidden" name="user_id" value="{{$user[0]->id}}">
                                <button type="submit" class="btn btn-lg btn-secundario btn-login">CONFIRMAR</button>
                            </form>
                        </div>
                    </div>
                @endif
            @else
                <h3 class="text-uppercase text-center">Usted ya ha cargado las credenciales de acceso satisfactoriamente.</h3>
                <hr>
                <h4 class="text-uppercase text-center">Presione <a href="{{route('/')}}">click</a> aqui para utilizar sus credenciales.</h4>
            @endif
            <div class="text-center login-footer">
                <p class="text-muted">AFTER O © 2018</p>
            </div>
        </div>
    <script src="{{asset('dashboard/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/actions/registeraccount.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $( function() {
        var now=new Date(); 
        $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1950:"+now
        });
      } );
    </script>
</body>
</html>