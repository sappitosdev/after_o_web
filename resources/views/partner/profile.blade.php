@extends('layouts.partner')
@section('content')
	<div class="profile-section">
		<div class="container">
			@if($partner[0]->p_picture == null || $partner[0]->p_picture == '')
				<form action="{{action('PartnerController@editProfilePicture')}}" method="POST" enctype="multipart/form-data" id="formEditPicture">
    				{{ csrf_field() }}
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<label for="profilepicture" class="custom-file-upload text-center center-block">
							   <img src="{{asset('img/user-icon.png')}}" class="img-responsive center-block img-thumbnail">
							</label>
							<input type="file" name="profilepicture" id="profilepicture" class="form-control center-block hidden">
						</div>
					</div>
					<input type="hidden" name="p_id" id="p_id" value="{{$partner[0]->p_id}}">
				</form>
			@else
				<form action="{{action('PartnerController@editProfilePicture')}}" method="POST" enctype="multipart/form-data" id="formEditPicture">
	    			{{ csrf_field() }}
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<label for="profilepicture" class="custom-file-upload text-center center-block">
							   <img src="{{Storage::url($partner[0]->p_picture)}}" class="img-responsive center-block img-thumbnail">
							</label>
							<input type="file" name="profilepicture" id="profilepicture" class="form-control center-block hidden">
						</div>
					</div>
					<input type="hidden" name="p_id" id="p_id" value="{{$partner[0]->p_id}}">
				</form>
			@endif
			<hr>
			<form method="POST" action="{{action('PartnerController@editProfile')}}">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Cedula</label>
							<input type="text" class="form-control" name="identification" value="{{$partner[0]->p_identification}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Nombres</label>
							<input type="text" class="form-control" name="names" value="{{$partner[0]->p_name}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Apellidos</label>
							<input type="text" class="form-control" name="lastnames" value="{{$partner[0]->p_lastname}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Sexo</label>
							<input type="text" class="form-control" name="sex" value="{{$partner[0]->p_sex}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Fecha de nacimiento</label>
							<input type="text" class="form-control" name="dateofbirth" value="{{$partner[0]->p_date_of_birth}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Email</label>
							<input type="text" class="form-control" name="email" value="{{$partner[0]->p_email}}" readonly>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Estado Civil</label>
							<select class="form-control" id="civil_status" name="civil_status">
								<option selected disabled value="">Seleccione un estado civil</option>
								@foreach($civilstatus as $civilstatu)
									<option value="{{$civilstatu->ct_concept}}">{{$civilstatu->ct_concept}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Telefono</label>
							<input type="text" class="form-control" name="phone" value="{{$partner[0]->p_cellphone}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Escuela</label>
							<input type="text" class="form-control" name="school" value="{{$partner[0]->p_school}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Profesión</label>
							<input type="text" class="form-control" name="profession" value="{{$partner[0]->p_profession}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Empresa</label>
							<input type="text" class="form-control" name="company" value="{{$partner[0]->p_company}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Cargo</label>
							<input type="text" class="form-control" name="position" value="{{$partner[0]->p_positio}}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Ciudad</label>
							<select class="form-control" name="citie" id="citie">
								<option selected disabled value="">Seleccione una ciudad</option>
								@foreach($cities as $citie)
									<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div id="div_citie_name" hidden>
						<div class="col-md-4">
							<div class="form-group">
								<label>Nombre Ciudad</label>
								<input type="text" class="form-control" name="citie_name" id="citie_name" value="{{$partner[0]->p_citie_name}}">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Universidad</label>
							<select class="form-control" name="universitie" id="universitie">
								<option selected disabled value="">Seleccione una universidad</option>
								@foreach($universities as $universitie)
									<option value="{{$universitie->u_id}}">{{$universitie->u_universitie}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div id="div_universitie_name" hidden>
						<div class="col-md-4">
							<div class="form-group">
								<label>Nombre Universidad</label>
								<input type="text" class="form-control" name="university_name" value="{{$partner[0]->p_university_name}}">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<button class="btn btn-primary btn-secundario center-block">Editar</button>
				<input type="hidden" name="p_id" id="p_id" value="{{$partner[0]->p_id}}">
			</form>
		</div>
	</div>

	<div class="modal fade" id="editPicture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Foto de Perfil</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea cambiar su foto de perfil.</strong></h5>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
	        <button type="button" class="btn btn-primary btn-principal" id="yes">SI</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script type="text/javascript">
		$('#profilepicture').change(function(){
			$('#editPicture').modal("show")
		});

		$('#yes').click(function(){
			$('#formEditPicture').submit();
		});

		$('#citie').val({!! $partner[0]->citie_c_id !!});
		$('#universitie').val({!! $partner[0]->universitie_u_id !!});
		$('#sex').val('{!! $partner[0]->p_sex !!}');
		$('#civil_status').val('{!! $partner[0]->p_civil_status !!}');

		if(	'{!! $partner[0]->c_citie !!}'  == 'Otra'){
			$('#div_citie_name').removeAttr('hidden');
		}

		if(	'{!! $partner[0]->u_universitie !!}'  == 'Otra'){
			$('#div_universitie_name').removeAttr('hidden');
		}

		$('#citie').change(function(){
			if($("#citie option:selected").text()=='Otra'){
				$('#div_citie_name').removeAttr('hidden');
				$('#citie_name').attr('required','required');
			}else{
				$('#div_citie_name').attr('hidden','hidden');
				$('#citie_name').val('');
			}
		});

		$('#universitie').change(function(){
			if($("#universitie option:selected").text()=='Otra'){
				$('#div_universitie_name').removeAttr('hidden');
				$('#university_name').attr('required','required');
			}else{
				$('#div_universitie_name').attr('hidden','hidden');
				$('#university_name').val('');
			}
		});

	</script>
@endsection