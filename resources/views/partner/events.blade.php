@extends('layouts.partner')
@section('content')

  @if(!$banner->isEmpty())
    <section>
      <div class="header-partner-event">
          <img src="{{Storage::url($banner[0]->cp_content)}}" class="img-partner">
          <!--<button class="btn btn-primary btn-principal btn-partner-event btn-lg">Boton</button>-->
      </div>
    </section>
  @endif
@php
  $key=0;
@endphp
  @foreach($events as $key => $event)
    @if(json_encode(\Carbon\Carbon::parse(date('m/d/Y'))->greaterThan(\Carbon\Carbon::parse($event->e_date_end))) == 'false')
      @php
        $key=$key+1
      @endphp
      @if($key%2 != 0)
        <section>
            <div class="row">
              <div class="col-md-4">
                  <div class="hero-section hero bg-primary">
                      <h1>{{$event->e_name}}</h1>
                      <h5>{{date('d/m/Y', strtotime($events[0]->e_date_start))}}</h5>
                      <h5>{{date('h:i a', strtotime($events[0]->e_time_start))}}</h5>
                      <a href="{{route('partnerEventsDetail',['id' => $event->e_id])}}" class="btn btn-primary btn-secundario">Ver más</a>
                  </div>
              </div>
              <div class="col-md-8">
                  <div class="hero-section hero" style="background: url( {{Storage::url($event->e_picture)}} );"></div>
              </div>
            </div>
        </section>
      @else
        <section>
            <div class="row">
              <div class="col-md-8">
                  <div class="hero-section hero" style="background: url( {{Storage::url($event->e_picture)}} );"></div>
              </div>
              <div class="col-md-4">
                  <div class="hero-section hero bg-primary">
                      <h1>{{$event->e_name}}</h1>
                      <h5>{{$event->e_date_start}}</h5>
                      <h5>{{date('h:i a', strtotime($events[0]->e_time_start))}}</h5>
                      <a href="{{route('partnerEventsDetail',['id' => $event->e_id])}}" class="btn btn-primary btn-secundario">Ver más</a>
                  </div>
              </div>
            </div>
        </section>
      @endif
    @endif
  @endforeach
  @if($key==0)
    <section>
        <div class="row">
          <div class="container">
            <div class="alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
              <strong>NO EXISTEN EVENTOS</strong>
            </div>
          </div>
        </div>
    </section>
  @endif
@endsection
