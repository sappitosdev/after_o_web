@extends('layouts.partner')
@section('content')

<!-- Recomendar Section -->
          <section class="section-principal">
              <div class="row">
                <div class="col-md-12">
                    <div class="hero-section-no-flex hero bg-secondary">
                        <h1 class="text-center">Recomendar a un socio.</h1>
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label>Buscar por:</label>
                                <select class="form-control" name="filter" id="filter">
                                    <option selected disabled>Seleccione un filtro de busqueda</option>
                                    <option value="Cedula">Cedula</option>
                                    <option value="Email">Correo Electronico</option>
                                </select>
                            </div>
                            <div class="form-group hidden" id="filterCedula">
                                <form id="formFilterCedula" method="POST">
                                    <label class="text-left">Cedula:</label>
                                    <input type="text" name="identification" id="identification" class="form-control" required pattern="[0-9]+">
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-principal center-block">Buscar </button>
                                </form>
                            </div>
                            <div class="form-group hidden" id="filterCorreo" method="POST">
                                <form id="formFilterCorreo" method="POST">
                                    <label class="text-left">Correo Electronico:</label>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-principal center-block">Buscar </button>
                                </form>
                            </div>

                            <div class="alert alert-success alert-dismissable hidden" id="alerta"></div>

                            <div id="partner"></div>

                            <button type="button" id="btnRecomendar" class="btn center-block btn-primary btn-principal hidden" data-toggle="modal" data-target="#inviteConfirmation" data-whatever="{{$partner[0]}}">Recomendar</button>
                        </div>
                    </div>
                </div>
              </div>

              <div class="modal fade" id="inviteConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <form action="{{action('PartnerController@partner_recommended')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                          <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Recomendar Socio</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="modal-body">
                            <h5><strong>Confirme que desea recomendar este socio.</strong></h5>
                            <input type="hidden" name="recommendedPartnerData" id="recommendedPartnerData">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary btn-principal">Recomendar</button>
                          </div>
                        </form>
                    </div>
                  </div>
                </div>
          </section>

@endsection