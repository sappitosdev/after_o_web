@extends('layouts.partner')
@section('content')
	<div class="profile-section">
		<div class="container">
			<form method="POST" action="{{action('PartnerController@changepasswordToPartner')}}">
				{{ csrf_field() }}
				<h2>Editar su contraseña</h2>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Nueva Contraseña</label>
							<input type="password" class="form-control" name="password">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Confirmar Nueva Contraseña</label>
							<input type="password" class="form-control" name="confirm_password">
						</div>
					</div>
				</div>
				<hr>
				<button class="btn btn-primary btn-secundario center-block">Editar</button>
				<input type="hidden" name="p_id" id="p_id" value="{{$partner[0]->p_id}}">
			</form>
		</div>
	</div>

@endsection