@extends('layouts.partner')
@section('content')
    <div class="container">
          <section class="section-principal">
            <div class="row">
              <div class="col-md-8">
                <h3>{{$event[0]->e_name}}</h3>
                <h3>{{$event[0]->ec_categorie}}</h3>
                <h3><i class="fa fa-map-marker"></i> Dirección</h3>
                <h3><i class="fa fa-calendar"></i> {{date('d/m/Y', strtotime($event[0]->e_date_start))}}</h3>
                <h3><i class="fa fa-clock-o"></i> {{date('h:i a', strtotime($event[0]->e_time_start))}}</h3>
              </div>
              <div class="col-md-4">
                <img class="img-responsive center-block" src="{{Storage::url($event[0]->e_picture)}}" alt="">
                <div class="row">
                  <div class="col-md-6">
                    <button type="button" class="center-block btn btn-primary btn-principal btn-lg btn-detail" data-toggle="modal" data-target="#firmarInvitados">Ver Invitados</button>
                  </div>
                  <div class="col-md-6">
                    <button type="button" class="center-block btn btn-primary btn-tercero btn-lg btn-detail" data-toggle="modal" data-target="#Invitar">Invitar</button>
                  </div>
                </div>
                <button type="button" class="center-block btn btn-primary btn-secundario btn-lg btn-detail" data-toggle="modal" data-target="#reservarMesa">Reservar Spot</button>
              </div>
            </div>
            <hr>
            <div class="row">
              <h3 class="title-row">Detalles del evento</h3>
              <div class="col-md-12">
                <p class="text-justify">{{$event[0]->e_description}}</p>
              </div>
            </div>
          </section>
          <hr>
          <section>
              <div id="map" style="height:400px;"></div>
          </section>
          @if(!$ad->isEmpty())
            <hr>
            <section>
                <a href="{{$ad[0]->cp_title}}" target="_blank">
                  <img src="{{Storage::url($ad[0]->cp_content)}}" class="img-responsive">
                </a>
            </section>
          @endif()

          <div class="modal fade" id="firmarInvitados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Invitados</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div id="guests"></div>
                    <input type="hidden" name="e_id" id="e_id" value="{{$event[0]->e_id}}">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="Invitar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Invitar al evento.</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    <div class="invita">
                        <h4 class="text-center">Invitar a un evento</h4>
                        <h5 class="text-center">Evento: {{$event[0]['e_name']}} - {{date('d/m/Y', strtotime($event[0]->e_date_start))}}</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <form action="{{action('PartnerController@invitedEvent')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Ingrese Correo Electronico: <span class="required">*</span></label>
                                        <input type="text" name="email" class="form-control">
                                        <input type="hidden" name="event" value="{{$event[0]->e_id}}">
                                        <input type="hidden" name="url" value="event">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-principal center-block">Invitar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="reservarMesa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{action('PartnerController@bookATable')}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Reservar Mesa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <label>Numero de personas *</label>
                        <input class="form-control text-capitalize" type="text" name="numberpersons" required>
                      </div>
                      <div class="form-group">
                        <label>Que desea para su mesa? *</label>
                        <textarea class="form-control" name="wish" rows="5" required></textarea>
                      </div>
                      <input type="hidden" name="membership_code" value="{{$partner[0]->p_membership_code}}">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
            
          <script type="text/javascript">
                var map;        
                function initMap() {
                    geocoder = new google.maps.Geocoder();
                    var position = { lat: {{$event[0]['e_latitud']}} , lng: {{$event[0]['e_longitud']}} };

                    map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 12,
                      center: position,
                    });

                    var marker = new google.maps.Marker({
                      position: position,
                      map: map
                    });
                }
          </script>

          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg7sqXHUdMNHLt7uhiJUcP0nV5nHylsfo&callback=initMap"
          async defer></script>
    </div>
@endsection