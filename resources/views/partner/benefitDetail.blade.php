@extends('layouts.partner')
@section('content')
    <div class="container">
          <section class="section-principal">
                <div class="row">
                  <div class="col-md-8">
                    <h3>{{$benefit[0]->b_name}}</h3>
                    <img class="img-responsive center-block img-thumbnail" src="{{Storage::url($benefit[0]->b_brand_logo)}}" alt="" style="height: 50px;">
                    <h3>{{$benefit[0]->bc_benefit_categorie}}</h3>
                    <h3><i class="fa fa-calendar"></i> {{$benefit[0]->b_validity  }}</h3>
                  </div>
                  <div class="col-md-4">
                    <img class="img-responsive center-block img-thumbnail" src="{{Storage::url($benefit[0]->b_promotional_picture)}}" alt="">
                    <!--<button class="center-block btn btn-primary btn-principal btn-lg btn-detail">Boton</button>-->
                  </div>
                </div>
          </section>

            <hr>

            <section>
                <div class="row">
                  <h3 class="title-row">Detalles del beneficio</h3>
                  <div class="col-md-12">
                    <p class="text-justify">{{$benefit[0]->b_description}}</p>
                  </div>
                </div>
            </section>

            <!--<hr>

            <section>
                <div class="row">
                  <h3 class="title-row">Sobre el aliado</h3>
                  <div class="col-md-4">
                    <img class="img-responsive center-block img-circle" src="http://via.placeholder.com/200x200" alt="">
                  </div>
                  <div class="col-md-8">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                </div>
            </section>

            <hr>

            <section>
                <div class="row">
                  <h3 class="title-row">Terminos y condiciones</h3>
                  <div class="col-md-12">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                </div>
            </section>-->
    </div>
@endsection