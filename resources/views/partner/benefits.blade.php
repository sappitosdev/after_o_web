@extends('layouts.partner')
@section('content')
    <div class="container">
      <br><br><br><br><br><br><br>
          <section>
            <div class="profile-tabs">
                <div class="nav-align-center">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="active">
                            <a href="#all" role="tab" data-toggle="tab">Todos</a>
                        </li>
                        @foreach($categories as $categorie)
                          <li>
                            <a href="#{{str_replace(' ', '', $categorie->bc_benefit_categorie)}}" role="tab" data-toggle="tab">{{$categorie->bc_benefit_categorie}}</a>
                          </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div id="all" class="tab-pane fade in active">
                  <div class="row">
                      @if(!$benefits->isEmpty())
                        @foreach($benefits as $benefit)
                          @if(json_encode(\Carbon\Carbon::parse(date('m/d/Y'))->greaterThan(\Carbon\Carbon::parse($benefit->b_validity))) == 'false')
                          <div class="col-md-4">
                              <div class="card">
                                  <a href="{{route('partnerBenefitsDetail',['id' => $benefit->b_id])}}">
                                    <img src="{{Storage::url($benefit->b_brand_logo)}}" class="img-responsive benefit-brand-logo" alt="Avatar">
                                    <img src="{{Storage::url($benefit->b_promotional_picture)}}" class="img-responsive promotional-picture" alt="Avatar">
                                    <div class="card-container">
                                      <h4><b>{{$benefit->bc_benefit_categorie}}</b></h4> 
                                      <p>{{$benefit->b_name}}</p> 
                                    </div>
                                  </a>
                              </div>
                          </div>
                          @endif
                        @endforeach
                      @else
                        <div class="col-md-12">
                              <br><br>
                              <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>No existen beneficios asociados.</strong>
                              </div>
                        </div>
                      @endif
                    </div>
                </div>
                @foreach($categories as $categorie)
                  <div id="{{str_replace(' ', '', $categorie->bc_benefit_categorie)}}" class="tab-pane fade in">
                    <div class="row">
                        @if(!$benefits->isEmpty())
                          @foreach($benefits->where('benefit_categories_bc_id',$categorie->bc_id) as $benefit)
                            @if(json_encode(\Carbon\Carbon::parse(date('m/d/Y'))->greaterThan(\Carbon\Carbon::parse($benefit->b_validity))) == 'false')
                            <div class="col-md-4">
                                <div class="card">
                                    <a href="{{route('partnerBenefitsDetail',['id' => $benefit->b_id])}}">
                                      <img src="{{Storage::url($benefit->b_brand_logo)}}" class="img-responsive benefit-brand-logo" alt="Avatar">
                                      <img src="{{Storage::url($benefit->b_promotional_picture)}}" class="img-responsive promotional-picture" alt="Avatar">
                                      <div class="card-container">
                                        <h4><b>{{$benefit->bc_benefit_categorie}}</b></h4> 
                                        <p>{{$benefit->b_name}}</p> 
                                      </div>
                                    </a>
                                </div>
                            </div>
                            @endif
                          @endforeach
                        @else
                          <div class="col-md-12">
                                <br><br>
                                <div class="alert alert-success alert-dismissable">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>No existen beneficios asociados.</strong>
                                </div>
                          </div>
                        @endif
                      </div>
                  </div>
                @endforeach
            </div>
          </section>
    </div>
@endsection