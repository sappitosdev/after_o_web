<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AFTER O</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('onepage/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('onepage/css/freelancer.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('onepage/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
  <div class="container-fluid">
        

    @if($partner[0]->p_status == 'ACTIVO')
          <!-- Navigation -->
          <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header page-scroll">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                      </button>
                      @if(!$logo[0]->isEmpty)
                          <a class="navbar-brand" href="{{route('partnerBenefits')}}">
                            <img class="logo-nav" src="{{Storage::url($logo[0]->cp_content)}}">
                          </a>
                      @endif
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
     
                          <li>
                              <a href="{{route('partner')}}">INICIO</a>
                          </li>
                          <li>
                              <a href="{{route('partnerBenefits')}}">BENEFICIOS</a>
                          </li>
                          <li>
                              <a href="{{route('partnerEvents')}}">EVENTOS</a>
                          </li>
                          <li>
                              <a href="{{route('recomendedPartner')}}">RECOMENDAR CANDIDATO</a>
                          </li>
                          <li class="dropdown">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-fw fa-user"></i> Usuario
                              <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="{{route('profile')}}"><i class="fa fa-fw fa-edit"></i>  Editar Perfil</a></li>
                                <li><a href="{{route('changepassword')}}"><i class="fa fa-fw fa-edit"></i>  Cambiar Clave</a></li>
                                  <li >
                                      <a href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                          <i class="fa fa-fw fa-power-off"></i> SALIR
                                      </a>

                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                          {{ csrf_field() }}
                                      </form>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <!-- /.navbar-collapse -->
              </div>
              <!-- /.container-fluid -->
          </nav>

          @if(Session::has('message'))
                <div class="alert alert-fixed {{ Session::get('alert-class') }} alert-dismissable">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>{{ Session::get('message') }}</strong>
                </div>
          @endif

          <section>
                <div id="carousel-example-generic" class="carousel slide carousel-custom" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    @if($slideractive)
                        @if(!$slideractive->isEmpty)
                            <div class="item active">
                              <img src="{{Storage::url($slideractive->cp_content)}}" alt="">
                              <div class="carousel-caption">
                                @if($slideractive)
                                  <h1>{{$slideractive->cp_title}}</h1>
                                  <p class="lead">{{$slideractive->cp_text}}</p>
                                @endif
                              </div>
                            </div>
                        @endif
                    @endif
                    @if($sliders)
                        @if(!$sliders->isEmpty())
                            @foreach($sliders as $slider)
                                <div class="item">
                                  <img src="{{Storage::url($slider->cp_content)}}" alt="">
                                  <div class="carousel-caption">
                                    @if($slider)
                                      <h1>{{$slider->cp_title}}</h1>
                                      <p class="lead">{{$slider->cp_text}}</p>
                                    @endif
                                  </div>
                                </div>
                            @endforeach
                        @endif
                    @endif
                  </div>
                </div>
          </section>

          <section>
                <div id="carousel-example-generic" class="carousel slide carousel-custom" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    @if($adactive)
                        @if(!$adactive->isEmpty)
                            <div class="item active">
                              <img src="{{Storage::url($adactive->cp_content)}}" alt="">
                            </div>
                        @endif
                    @endif
                    @if($ads)
                        @if(!$ads->isEmpty())
                            @foreach($ads as $slider)
                                <div class="item">
                                  <img src="{{Storage::url($slider->cp_content)}}" alt="">
                                </div>
                            @endforeach
                        @endif
                    @endif
                  </div>
                </div>
          </section>
          <!-- Beneficios Section -->
          <section>
                <div class="hero-section bg-secondary">
                    <h1>Beneficios</h1>
                </div>
                <div class="container">
                    <div class="row">
                        @foreach($benefits as $benefit)
                              @if(json_encode(\Carbon\Carbon::parse(date('m/d/Y'))->greaterThan(\Carbon\Carbon::parse($benefit->b_validity))) == 'false')
                              <div class="col-md-4">
                                  <div class="card">
                                      <a href="{{route('partnerBenefitsDetail',['id' => $benefit->b_id])}}">
                                        <img src="{{Storage::url($benefit->b_brand_logo)}}" class="img-responsive benefit-brand-logo" alt="Avatar">
                                        <img src="{{Storage::url($benefit->b_promotional_picture)}}" class="img-responsive promotional-picture" alt="Avatar">
                                        <div class="card-container">
                                          <h4><b>{{$benefit->bc_benefit_categorie}}</b></h4> 
                                          <p>{{$benefit->b_name}}</p> 
                                          <p>{{$benefit->b_validity}}</p>
                                        </div>
                                      </a>
                                  </div>
                              </div>
                              @endif
                        @endforeach
                    </div>
                </div>
          </section>

          <!-- Cartelera Section -->
          <section>
                <div class="hero-section bg-secondary">
                    <h1>Cartelera</h1>
                </div>
                  <div class="owl-carousel owl-theme cartelera">
                      @foreach($partners_cartelera as $partner)
                        @if($partner->p_picture == null)
                          <div class="item">
                              <img src="{{asset('img/user-icon.png')}}" class="img-responsive center-block billboard img-thumbnail">
                              <h6 class="text-muted text-center">{{$partner->p_name}} {{$partner->p_lastname}} - {{$partner->p_membership_code}}</h6>
                          </div>
                        @else
                          <div class="item">
                              <img src="{{Storage::url($partner->p_picture)}}" class="img-responsive center-block billboard img-thumbnail">
                              <h6 class="text-muted text-center">{{$partner->p_name}} {{$partner->p_lastname}} - {{$partner->p_membership_code}}</h6>
                          </div>
                        @endif
                      @endforeach
                  </div>
          </section>

          <!-- Invitar Section -->
          <section>
            <div class="bg-secondary invitar">
                <h3 class="text-center">Invitar a un evento</h3>
                <hr>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <form action="{{action('PartnerController@invitedEvent')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Evento: <span class="required">*</span></label>
                                <select class="form-control" name="event">
                                    <option disabled selected value="">Seleccione un evento</option>
                                    @foreach($events as $event)
                                        <option value="{{$event->e_id}}">{{$event->e_name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="url" value="partner">
                            </div>
                            <div class="form-group">
                                <label>Ingrese Correo Electronico: <span class="required">*</span></label>
                                <input type="text" name="email" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary btn-principal center-block">Invitar</button>
                        </form>
                    </div>
                </div>
            </div>
          </section>

          <!-- Footer -->
          <footer class="text-center">
              <div class="footer-above">
                  <div class="container">
                      <div class="row">
                          <div class="footer-col col-md-6">
                              @if(!$logo->isEmpty())
                                  @if(!$logo[0]->isEmpty)
                                      <img class="img-responsive logo-footer text-center center-block" src="{{Storage::url($logo[0]->cp_content)}}">
                                  @endif
                              @endif
                              @if(!$footer_title->isEmpty())
                                  <h3>{{$footer_title[0]->cp_content}}</h3>
                              @endif
                              @if(!$footer_text->isEmpty())
                                  <p class="text-left">{!! $footer_text[0]->cp_content !!}</p>
                              @endif
                              <a href="https://www.instagram.com/afterocaracas/" class="btn-social btn-outline"><i class="fa fa-fw fa-instagram"></i></a>
                              <a href="mailto:contacto@afteroclub.com" class="btn-social btn-outline"><i class="fa fa-fw fa-envelope"></i></a>    
                              <p>contacto@afteroclub.com</p>
                          </div>
                          <div class="footer-col col-md-6">
                              <h3>Contactanos !</h3>
                              <form>
                                  <div class="form-group">
                                      <input type="text" name="" class="form-control" placeholder="Nombre">
                                  </div>
                                  <div class="form-group">
                                      <input type="text" name="" class="form-control" placeholder="Apellido">
                                  </div>
                                  <div class="form-group">
                                      <input type="text" name="" class="form-control" placeholder="Email">
                                  </div>
                                  <div class="form-group">
                                      <textarea class="form-control" rows="5" placeholder="Mensaje"></textarea>
                                  </div>
                                  <button class="btn btn-primary bg-primary btn-lg">Enviar</button>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="footer-below">
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-12">
                              Copyright &copy; Your Website 2016
                          </div>
                      </div>
                  </div>
              </div>
          </footer>

    @elseif(($partner[0]->p_number_recommendations >= $quantity_recommendations[0]->cp_content) && $partner[0]->p_status == 'PENDIENTE_POR_ACTIVACION')

          <div class="container">
              <div class="text-center">
                  <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}">
              </div>
              <h3 class="text-uppercase text-center">Su usuario se encuentra limitado debido a alguna situación, por favor comuniquese con nosotros.</h3>
              <hr>
              <h4 class="text-uppercase text-center">Presione <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  Click
              </a> aqui para volver.</h4>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                  {{ csrf_field() }}
              </form>
              <div class="text-center login-footer">
                  <p class="text-muted">AFTER O © 2018</p>
              </div>
          </div>
        
    @else
    
    <div class="container">
        <div class="text-center">
            <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}">
        </div>
        <h3 class="text-uppercase text-center">Tu usuario aún se encuentra en proceso de activación por no presentar el número de recomendaciones necesarias para la Membresía AFTER O</h3>
        <hr>
        <h5 class="text-center">
            Para activar su usuario debe poseer 
            <span class="required">"{{$quantity_recommendations[0]->cp_content}}"</span> recomendaciones.
        </h5>
        <h5 class="text-center">De las cuales posee 
            <span class="required">"{{$partner[0]->p_number_recommendations}}"</span>
        </h5>
        
        @if(!$partner_recomendations->isEmpty())
            <hr>
            <h5 class="text-center">Socios que te recomendarion:</h5>
            @foreach($partner_recomendations as $pr)
                <h6 class="text-center">{{$pr->p_name}} {{$pr->p_lastname}}</h6>
            @endforeach
        @endif
        <hr>
        <h4 class="text-uppercase text-center">Presione <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Click
        </a> aqui para volver.</h4>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" >
            {{ csrf_field() }}
        </form>
        <div class="text-center login-footer">
            <p class="text-muted">AFTER O © 2018</p>
        </div>
    </div>
    
    @endif
  </div>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    

    <!-- jQuery -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('onepage/js/freelancer.min.js')}}"></script>
    <script src="{{asset('owlcarousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/actions/partners.js')}}"></script>
    <script src="{{asset('js/actions/events.js')}}"></script>
    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
    </script>

    
</body>

</html>