<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AFTER O</title>

        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="display: -webkit-box;">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    

                    <div class="text-center">
                        <img class="logo" src="{{asset('img/logo.png')}}" class="img-responsive">
                    </div>

                    <h2 class="text-center">Confirmar contraseña</h2>
                    @if(Session::has('message'))
                          <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('message') }}</strong>
                          </div>
                    @endif
                    <hr>

                    <div class="col-md-10 col-md-offset-1">
                        <form  method="POST" action="{{ action('ResetPasswordController@reset_this_password') }}">
                            
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="email" class="control-label">Contraseña</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>

                            <div class="form-group">
                                <label for="email" class="control-label">Confirmar Contraseña</label>
                                <input id="confirm_password" type="password" class="form-control" name="confirm_password" required>
                            </div>

                            <input type="hidden" name="token_auth" value="{{$token}}">

                            <button type="submit" class="btn btn-primary btn-lg btn-principal btn-login">RESTABLECER</button>
                        </form>
                    </div>
                    <div class="text-center login-footer">
                        <p class="text-muted">AFTER O © 2018</p>
                    </div>
                </div>
                <div class="hidden-xs col-sm-6 col-md-8 col-lg-8 background-login" style="background-image: url( {{asset('img/background.jpg')}} );"></div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    </body>
</html>

