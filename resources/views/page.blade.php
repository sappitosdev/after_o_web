<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AFTER O</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('onepage/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('onepage/css/freelancer.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('onepage/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    @if(Session::has('message'))
          <div class="alert alert-fixed {{ Session::get('alert-class') }} alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>{{ Session::get('message') }}</strong>
          </div>
      @endif
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                @if(!$logo->isEmpty())
                    @if(!$logo[0]->isEmpty)
                        <a class="navbar-brand" href="#page-top">
                          <img class="logo-nav" src="{{Storage::url($logo[0]->cp_content)}}">
                        </a>
                    @endif
                @endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#club">EL CLUB</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#membresia">MEMBRESÍAS</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#beneficios">BENEFICIOS</a>
                    </li>
                    @if(!$allies->isEmpty())
                        <li class="page-scroll">
                            <a href="#aliados">ALIADOS</a>
                        </li>
                    @endif
                    <li class="page-scroll">
                        <a href="{{route('/')}}"><i class="fa fa-fw fa-sign-in"></i> ENTRAR</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <div class="container-fluid">
      <!-- Header -->
      <section>
            <div id="home" class="carousel slide carousel-custom" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                @if($slideractive)
                    @if(!$slideractive->isEmpty)
                        <div class="item active">
                          <img src="{{Storage::url($slideractive->cp_content)}}" alt="">
                          <div class="carousel-caption hidden-xs hidden-sm">
                            @if($slideractive)
                              <h1>{{$slideractive->cp_title}}</h1>
                              <p class="lead">{{$slideractive->cp_text}}</p>
                            @endif
                          </div>
                        </div>
                    @endif
                @endif
                @if($sliders)
                    @if(!$sliders->isEmpty())
                        @foreach($sliders as $slider)
                            <div class="item">
                              <img src="{{Storage::url($slider->cp_content)}}" alt="">
                              <div class="carousel-caption hidden-xs hidden-sm">
                                @if($slider)
                                  <h1>{{$slider->cp_title}}</h1>
                                  <p class="lead">{{$slider->cp_text}}</p>
                                @endif
                              </div>
                            </div>
                        @endforeach
                    @endif
                @endif
              </div>
              <!-- Left and right controls -->
              <a class="left carousel-control" href="#home" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#home" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
      </section>

      <!-- Club Section -->
      <section id="club">
        @if(!$titleclub->isEmpty() && !$textclub->isEmpty() && !$imageclub->isEmpty())
          <div class="row">
            <div class="col-md-4">
                <div class="hero-section hero bg-primary">
                    <h1>{{$titleclub[0]->cp_content}}</h1>
                    <h5>{{$textclub[0]->cp_content}}</h5>
                </div>
            </div>
            <div class="col-md-8">
                <div class="hero-section hero" style="background: url( {{ Storage::url($imageclub[0]->cp_content) }} );"></div>
            </div>
          </div>
        @endif
        @if(!$titleclubdescription->isEmpty() && !$textclubdescription->isEmpty() && !$imageclubdescription->isEmpty())
          <div class="row row-m-top">
            <div class="col-md-8">
                <div class="hero-section hero" style="background: url( {{ Storage::url($imageclubdescription[0]->cp_content) }} );"></div>
            </div>
            <div class="col-md-4">
                <div class="hero-section hero bg-secondary">
                    <h1>{{$titleclubdescription[0]->cp_content}}</h1>
                    <h5>{{$textclubdescription[0]->cp_content}}</h5>
                </div>
            </div>
          </div>
        @endif
      </section>

      <!-- Membresias Section -->
      <section id="membresia">
        @if(!$titlemembership->isEmpty() && !$textmembership->isEmpty() && !$imagemembership->isEmpty())
          <div class="row">
            <div class="col-md-4">
                <div class="hero-section hero  bg-primary">
                    <h1>{{$titlemembership[0]->cp_content}}</h1>
                    <h5>{{$textmembership[0]->cp_content}}</h5>
                    <a href="{{route('membersregistration')}}" class="btn btn-outline-primary">Registrate</a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="hero-section hero" style="background: url({{ Storage::url($imagemembership[0]->cp_content) }});"></div>
            </div>
          </div>
        @endif
      </section>

      <!-- Beneficios Section -->
      <section id="beneficios">
        @if(!$titlebenefit->isEmpty() && !$textbenefit->isEmpty() && !$imagebenefit->isEmpty())
          <div class="row">
            <div class="col-md-8">
                <div class="hero-section hero" style="background: url({{ Storage::url($imagebenefit[0]->cp_content) }});"></div>
            </div>
            <div class="col-md-4">
                <div class="hero-section hero bg-secondary">
                    <h1>{{$titlebenefit[0]->cp_content}}</h1>
                    <h5>{{$textbenefit[0]->cp_content}}</h5>
                </div>
            </div>
          </div>
        @endif
      </section>

      <!-- Aliados Section -->
      
      @if(!$allies->isEmpty())
          <section id="aliados">
              <div class="row">
                  <div class="owl-carousel owl-theme">
                    @foreach($allies as $allie)
                        <div class="item">
                            <!--<p class="text-center text-uppercase text-muted" style=" word-break: break-all;word-wrap: break-word;">sdsdsdasdasdassdsdasddsdasdasdasdsadasd</p>-->
                            <img src="{{Storage::url($allie->cp_content)}}" class="img-responsive text-center center-block">

                        </div>
                    @endforeach
                  </div>
            </div>
          </section>
      @endif
    </div>
    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-6">
                        @if(!$logo->isEmpty())
                            @if(!$logo[0]->isEmpty)
                                <img class="img-responsive logo-footer text-center center-block" src="{{Storage::url($logo[0]->cp_content)}}">
                            @endif
                        @endif
                        @if(!$footer_title->isEmpty())
                            <h3>{{$footer_title[0]->cp_content}}</h3>
                        @endif
                        @if(!$footer_text->isEmpty())
                            <p class="text-left">{!! $footer_text[0]->cp_content !!}</p>
                        @endif
                        <a href="https://www.instagram.com/afterocaracas/" class="btn-social btn-outline"><i class="fa fa-fw fa-instagram"></i></a>
                        <a href="mailto:contacto@afteroclub.com" class="btn-social btn-outline"><i class="fa fa-fw fa-envelope"></i></a>    
                        <p>contacto@afteroclub.com</p>
                    </div>
                    <div class="footer-col col-md-6">
                        <h3>Contacto</h3>
                        <form action="{{action('LandingController@contactEmail')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input type="text" name="lastname" class="form-control" placeholder="Apellido">
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="5" placeholder="Mensaje" name="message"></textarea>
                            </div>
                            <button class="btn btn-primary bg-primary btn-lg">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; AFTER O® 2018
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- jQuery -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('onepage/js/freelancer.min.js')}}"></script>
    <script src="{{asset('owlcarousel/dist/owl.carousel.min.js')}}"></script>

    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });
    </script>
<!--Start of Zendesk Chat Script-->
<!--<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?5vvudTfj9hFX7qPTfKuY9OywyV0k5ZGP";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>-->
<!--End of Zendesk Chat Script-->

<!-- Start of HubSpot Embed Code -->
 <script type=“text/javascript” id=“hs-script-loader” async defer src=“//js.hs-scripts.com/4936461.js”></script>
<!-- End of HubSpot Embed Code -->
</body>

</html>
