<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AFTER O</title>

        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body class="content-register">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="overlay join text-uppercase text-center">
                        @if(Session::has('message'))
                              <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('message') }}</strong>
                              </div>
                        @endif
                        <div class="text-right">
                            <a href="{{route('public')}}" class="return">Inicio</a>
                            <a href="{{route('/')}}" class="return">Login</a>
                        </div>
                        <h2 class="overlay-title"> <strong>REGISTRO DE NUEVOS MIEMBROS</strong></h2>
                        <br>
                        <div class="text-left">
                            <h5><strong>Pasos para la obtención de Membresías AFTER O:</strong></h5>
                            <ul>
                                <li>Completar el formulario de Solicitud de Membresías.</li>
                                <li>Obtener el número de recomendaciones requeridas por Miembros AFTER O activos. </li>
                                <li>Aprobar la revisión del Comité de Membresías.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="overlay">
                        <h3 class="overlay-title text-center text-uppercase"><strong>Formulario de Solicitud de Membresía AFTER O</strong></h3>
                        <hr>
                        <form action="{{action('LandingController@registerAccount')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cedula: <span class="required">*</span></label>
                                        <input type="text" name="identification" class="form-control" required pattern="[0-9]+" maxlength="8">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre(s): <span class="required">*</span></label>
                                        <input type="text" name="name" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Apellido(s): <span class="required">*</span></label>
                                        <input type="text" name="lastname" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sexo: <span class="required">*</span></label>
                                        <select class="form-control text-capitalize" name="sex" required>
                                            <option selected disabled value="">Selecciona un sexo</option>
                                            @foreach($sexs as $sex)
                                                <option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Fecha de nacimiento: <span class="required">*</span></label>
                                        <input type="text" name="dateofbirth" class="form-control datepicker" required id="date1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Correo Electronico: <span class="required">*</span></label>
                                        <input type="email" name="email" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ciudad: <span class="required">*</span></label>
                                        <select class="form-control text-capitalize" name="citie" id="citie" required>
                                            <option selected disabled value="">Selecciona una ciudad</option>
                                            @foreach($cities as $citie)
                                                <option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group hidden" id="writeCitie">
                                        <label>Escriba la Ciudad <span class="required">*</span></label>
                                        <input type="text" name="othercitie" class="form-control text-capitalize">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Estado Civil: <span class="required">*</span></label>
                                        <select class="form-control text-capitalize" name="civilstatus" required>
                                            <option selected disabled value="">Selecciona un estado civil</option>
                                            @foreach($civil_status as $cs)
                                                <option value="{{$cs->ct_concept}}">{{$cs->ct_concept}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Telefono: <span class="required">*</span></label>
                                        <input type="text" name="telephone" class="form-control" maxlength="11" pattern="[0-9]+">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Colegio:</label>
                                        <input type="text" name="school" class="form-control text-capitalize">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label>Carrera Universitaria? </label>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="universidad"  value="Si">Si
                                          </label>
                                          <label>
                                            <input type="radio" name="universidad" value="No" checked>No
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Universidad </label>
                                        <select name="university" id="university" class="form-control text-capitalize" disabled>
                                            <option selected disabled value="">Selecciona una universidad</option>
                                            @foreach($universities as $universitie)
                                                <option value="{{$universitie->u_id}}">{{$universitie->u_universitie}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group hidden" id="writeUniversitie">
                                        <label>Escriba la Universidad <span class="required">*</span></label>
                                        <input type="text" name="otheruniversitie" class="form-control text-capitalize">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Profesión </label>
                                        <input type="text" name="profession" id="profession" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label>Trabaja? </label>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="trabaja" value="Si">Si
                                          </label>
                                          <label>
                                            <input type="radio" name="trabaja" value="No" checked>No
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Empresa </label>
                                        <input type="text" name="company" id="company" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Cargo </label>
                                        <input type="text" name="position" id="position" class="form-control text-capitalize" disabled>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-principal center-block">Registrarse</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <script src="{{asset('dashboard/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/actions/registeraccount.js')}}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $( function() {
        var now=new Date(); 
        $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1950:"+now
        });
      } );
    </script>
    </body>
</html>
