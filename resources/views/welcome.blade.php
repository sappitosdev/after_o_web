<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AFTER O</title>

        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="display: -webkit-box;">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                    @if(!$logo->isEmpty())
                        <div class="text-center">
                            <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}" class="img-responsive">
                        </div>
                    @endif
                    @if(Session::has('message'))
                          <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('message') }}</strong>
                          </div>
                    @endif
                    <h2 class="text-center">Iniciar Sesión</h2>

                    <h5 class="text-center"><a href="{{route('membersregistration')}}">Crear una cuenta</a></h5>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-login" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                        <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Password</label>
                                        <input id="password" type="password" class="form-control input-lg" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            
                                <h5 class="text-right"><a href="/password/reset">¿Olvidó su clave?</a></h5>
                                <button type="submit" class="btn btn-primary btn-lg btn-principal btn-login">INICIAR SESIÓN</button>
                                <a href="{{route('public')}}" class="btn btn-primary btn-lg btn-secundario btn-login">INFORMACIÓN PARA TERCEROS</a>
                            </form>
                        </div>
                    </div>

                    <div class="text-center login-footer">
                        <p class="text-muted">AFTER O © 2018</p>
                    </div>
                </div>
                @if(!$linklogin->isEmpty() || !$imagelogin->isEmpty())
                    @if($linklogin[0]->cp_content)
                        <a href="{{$linklogin[0]->cp_content}}" target="_blank">
                            <div class="hidden-xs col-sm-6 col-md-8 col-lg-8 background-login" style="background-image: url( {{Storage::url($imagelogin[0]->cp_content)}} );">
                                <div class="text">
                                    <h1 class="text-capitalize text-center">{{$titlelogin[0]->cp_content}}</h1>
                                    <h4 class="text-capitalize text-center">{{$textlogin[0]->cp_content}}</h4>
                                </div>
                            </div>
                        </a>
                    @else
                        <div class="hidden-xs col-sm-6 col-md-8 col-lg-8 background-login" style="background-image: url( {{Storage::url($imagelogin[0]->cp_content)}} );">
                            <div class="text">
                                <h1 class="text-capitalize text-center">{{$titlelogin[0]->cp_content}}</h1>
                                <h4 class="text-capitalize text-center">{{$textlogin[0]->cp_content}}</h4>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>
        <!-- jQuery -->
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    </body>
</html>
