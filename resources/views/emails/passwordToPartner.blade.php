<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Confirmación de registro.</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <style type="text/css">
    	body {
		    font-family: 'Open Sans', sans-serif;
		    background-color: #2C3E50;
		    color:#FFFFFF;
		}

		p {
		    font-size: 20px;
		}

		a{
			color:#d81e3f;
		}

		hr{
			height:2px; 
			border:none; 
			color:#eee; 
			background-color:#eee;
		}

		.header .text{
			margin-top: 50px;
		}

		.logo {
		    height: 250px;
		    margin-top: 5%;
		    margin-bottom: 10px;
		}

    	h1, h2, h3, h4, h5, h6 {
		    font-family: 'Comfortaa', cursive;
		    text-transform: uppercase;
		}
		/*Boostrap Styles*/
	    	.container {
			  padding-right: 15px;
			  padding-left: 15px;
			  margin-right: auto;
			  margin-left: auto;
			}
			@media (min-width: 768px) {
			  .container {
			    width: 750px;
			  }
			}
			@media (min-width: 992px) {
			  .container {
			    width: 970px;
			  }
			}
			@media (min-width: 1200px) {
			  .container {
			    width: 1170px;
			  }
			}
			.center-block{
				display: block;
				margin-right: auto;
				margin-left: auto;
			}	
			.text-center{
				text-align: center;
			}
			.text-right{
				text-align: right;
			}

			footer{
				border-top:2px solid #eeeeee;
			}

			footer .icons a{
				color: #FFFFFF;
			}
		/* End Boostrap Styles*/
    </style>
  </head>
  <body class="">
	<div class="container">
	  	<div class="header">
	  		<img src="{{asset('img/logo.png')}}" class="img-responsive logo center-block">
	  		<div class="text text-center">
	  			<h2>Registro Exitoso</h2>
	    		<p>Para verificar su cuenta de Email, haz click <a href="{{$url}}">aquí</a>.</p>
	    	</div>
	  	</div>
	  	<hr>
  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

  		<footer class="text-center">
  			<h4>Av. Principal de las Mercedes.Caracas, Venezuela</h4>
  			<div class="icons center-block text-center">
	  			<a href="" class=""><i class="fab fa-instagram fa-2x"></i></a>
	  			<a href="" class=""><i class="far fa-envelope fa-2x"></i></a>
  			</div>
  			<h4>contacto@afteroclub.com</h4>
  		</footer>
    </div>
  </body>
</html>



