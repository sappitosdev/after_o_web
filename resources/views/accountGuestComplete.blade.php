<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AFTER O</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
    <div class="container">
            @if(!$logo->isEmpty())
                <div class="text-center">
                    <img class="logo" src="{{Storage::url($logo[0]->cp_content)}}" class="img-responsive">
                </div>
            @endif
            @if(!$guest->isEmpty())
                <h2 class="text-center">Complete sus datos</h2>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        @if(Session::has('message'))
                              <div class="alert {{ Session::get('alert-class') }} alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('message') }}</strong>
                              </div>
                        @endif
                        <form class="form-login" method="POST" action="{{action('LandingController@confirmDataToGuest')}}">
                            {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="password">Nombre: <span class="required">*</span></label>
                                    <input id="name" type="text" class="form-control text-capitalize" name="name" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                </div>
                                <div class="form-group">
                                    <label for="password">Apellido: <span class="required">*</span></label>
                                    <input id="lastname" type="text" class="form-control text-capitalize" name="lastname" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                                </div>
                                <div class="form-group">
                                    <label for="password">Cedula de Identidad (Solo Numeros): <span class="required">*</span></label>
                                    <input id="identification" type="text" class="form-control" name="identification" required maxlength="9" pattern="[0-9]+">
                                </div>
                                <div class="form-group">
                                    <label for="password">Fecha de Nacimiento: <span class="required">*</span></label>
                                    <input id="dateofbirth" type="text" class="form-control datepicker" name="dateofbirth" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Sexo: <span class="required">*</span></label>
                                    <select class="form-control" name="sex" id="sex" required>
                                        <option value="" selected disabled>Seleccione un sexo</option>
                                        @foreach($sexs as $sex)
                                            <option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="password">Ciudad: <span class="required">*</span></label>
                                    <select class="form-control" name="citie" id="citie" required>
                                        <option value="" selected disabled>Seleccione una ciudad</option>
                                        @foreach($cities as $citie)
                                            <option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            <input type="hidden" name="g_id" value="{{$guest[0]->g_id}}">
                            <button type="submit" class="btn btn-lg btn-secundario btn-login">GUARDAR</button>
                        </div>
                        </form>
                    </div>
                </div>
            @else
                <h3 class="text-uppercase text-center">Sus datos ya fueron cargados exitosamente</h3>
                <hr>
                <h4 class="text-uppercase text-center">Presione <a href="{{route('public')}}">click</a> para volver.</h4>
            @endif
            <div class="text-center login-footer">
                <p class="text-muted">AFTER O © 2018</p>
            </div>
        </div>

        <!-- jQuery -->
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
          $( function() {
            $( ".datepicker" ).datepicker();
          } );
        </script>
</body>
</html>