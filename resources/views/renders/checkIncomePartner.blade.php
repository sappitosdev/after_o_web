@if($partner)
    <h3 class="text-center">Socio - Membresía ( {{$partner[0]->p_membership_code}} )</h3>
    <h4 class="text-center">Nombre: {{$partner[0]->p_name}} {{$partner[0]->p_lastname}}</h4>
    <h4 class="text-center">Sexo: {{$partner[0]->p_sex}}</h4>
    <h4 class="text-center">Fecha de Nacimiento: {{$partner[0]->p_date_of_birth}}</h4>
    <h4 class="text-center">Edad: {{ date('Y') }}


    </h4>
    <h4 class="text-center">Email: {{$partner[0]->p_email}}</h4>
    <h4 class="text-center">Telefono: {{$partner[0]->p_cellphone}}</h4>
    <h4 class="text-center">Estado Civil: {{$partner[0]->p_civil_status}}</h4>
@endif
@if($event)
	<hr>
	<h3 class="text-center">Datos del Evento</h3>
	<h4 class="text-center">Nombre: {{$event[0]->e_name}}</h4>
	<h4 class="text-center">Fecha: {{$event[0]->e_date_start}}</h4>
@endif
<hr>
<div class="text-center">
	<button data-toggle="modal" data-target="#confirm" class="btn btn-primary btn-principal">Confirmar</button>
	<button class="btn btn-primary btn-cancelar">Cancelar</button>
</div>

<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="{{action('SecurityController@confirmAssitant')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Confirmación</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <h5 class="text-center"><strong>Desea dar entrada al evento a la persona seleccionada?</strong></h5>
            <hr>
            <div class="row">
                @if($event[0]->e_total_guests == 0)
                    <div class="col-md-12">
                        <h5 class="text-center"><strong>Evento sin limite.</strong></h5>
                    </div>
                @else
                    <div class="col-md-6">
                        <h5 class="text-center"><strong>Limite del evento</strong></h5>
                        <h5 class="text-center"><strong>{{$event[0]->e_total_guests}}</strong></h5>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-center"><strong>Total de ingreso</strong></h5>
                        @if( $count_event_assitants > $event[0]->e_total_guests)
                            <h5 class="text-center required"><strong>{{$count_event_assitants}}</strong></h5>
                        @else
                            <h5 class="text-center"><strong>{{$count_event_assitants}}</strong></h5>
                        @endif
                    </div>
                @endif
            </div>
            <hr>
            <div class="form-group text-center">
                <label>Socio posee invitado? </label>
                <div class="radio">
                  <label>
                    <input type="radio" name="partnerguest"  value="Si">Si
                  </label>
                  <label>
                    <input type="radio" name="partnerguest" value="No" checked>No
                  </label>
                </div>
            </div>
            <div class="form-group hidden" id="divEmailGuest">
                <label>Ingrese email del invitado </label>
                <input class="form-control" type="email" name="emailguest" id="emailguest">
            </div>
            <input type="hidden" name="e_id" id="e_id" value="{{$event[0]->e_id}}">
            <input type="hidden" name="p_id" id="p_id" value="{{$partner[0]->p_id}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary btn-principal">Confirmar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('input:radio[name=partnerguest]').click(function(){
        if(this.value == 'Si'){
          $('#divEmailGuest').removeClass('hidden');
          $('#emailguest').attr('required','required');
        }else{
          $('#divEmailGuest').addClass('hidden');
          $('#emailguest').removeAttr('required');
          $('#emailguest').val('');
        }
    });
</script>