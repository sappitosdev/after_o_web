@if($guests->isEmpty())
	<h3 class="text-center text-uppercase">No existen invitados por usted en este evento.</h3>
@else
	<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center">Socio</th>
					<th class="text-center">Fecha de Nacimiento</th>
					<th class="text-center">Sexo</th>
					<th class="text-center">Correo Electronico</th>
					<th class="text-center">Eliminar Invitación</th>
				</tr>
			</thead>
			<tbody>
				@foreach($guests as $guest)
					<tr class="text-center">
						<td>
							@if($guest->g_name && $guest->g_lastname && $guest->g_identification)
								{{$guest->g_name}} {{$guest->g_lastname}} - V-{{$guest->g_identification}}
							@elseif($guest->p_name && $guest->p_lastname && $guest->p_identification)
								{{$guest->p_name}} {{$guest->p_lastname}} - V-{{$guest->p_identification}}
							@endif
						</td>
						<td>
							@if($guest->g_date_of_birth)
								{{$guest->g_date_of_birth}}
							@elseif($guest->p_date_of_birth)
								{{$guest->p_date_of_birth}}
							@else
								-
							@endif
						</td>
						<td class="text-capitalize">
							@if($guest->g_sex)
								{{$guest->g_sex}}
							@elseif($guest->p_sex)
								{{$guest->p_sex}}
							@endif
						</td>
						<td>
							@if($guest->g_email)
								{{$guest->g_email}}
							@elseif($guest->p_email)
								{{$guest->p_email}}
							@endif
						</td>
						<td>
							<form method="POST" action="{{action('PartnerController@deleteInvitationGuest')}}">
								{{ csrf_field() }}
								<button type="submit" class="btn btn-danger"><i class="fa fa-eraser"></i></button>
								<input type="hidden" name="e_id" id="e_id" value="">
								<input type="hidden" name="eg_id" id="eg_id" value="{{$guest->eg_id}}">
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
	</table>
@endif