@if($partners->isEmpty())
	<h3 class="text-center text-uppercase">El Socio no posee recomendaciones.</h3>
@else
	<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center">Socio</th>
					<th class="text-center">Fecha de Nacimiento</th>
					<th class="text-center">Sexo</th>
					<th class="text-center">Correo Electronico</th>
					<th class="text-center">Membresía</th>
				</tr>
			</thead>
			<tbody>
				@foreach($partners as $partner)
					<tr class="text-center">
						<td>
							{{$partner->p_name}} {{$partner->p_lastname}} - V-{{$partner->p_identification}}
						</td>
						<td>
							{{$partner->p_date_of_birth}}
						</td>
						<td class="text-capitalize">
							{{$partner->p_sex}}
						</td>
						<td>
							{{$partner->p_email}}
						</td>
						<td>
							{{$partner->m_name}}
						</td>
					</tr>
				@endforeach
			</tbody>
	</table>
@endif