<div class="alert  alert-success alert-dismissable">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	<strong>No puede se puede dar acceso a esta persona, debido a que sus datos no se encuentran registrados en el sistema. Puede ser una persona desconocida o estar invitado al evento y no proporciono sus datos en el correo enviado.</strong>
</div>