@if($guest && !$guest_invited->isEmpty())
    <h3 class="text-center">Invitado de {{$guest_invited[0]->p_name}} {{$guest_invited[0]->p_lastname}} - {{$guest_invited[0]->p_membership_code}}</h3>
    <h4 class="text-center">Nombre: {{$guest[0]->g_name}} {{$guest[0]->g_lastname}}</h4>
    <h4 class="text-center">Sexo: {{$guest[0]->g_sex}}</h4>
    <h4 class="text-center">Cedula: {{$guest[0]->g_identification}}</h4>
    <h4 class="text-center">Fecha de Nacimiento: {{$guest[0]->g_date_of_birth}}</h4>
    <h4 class="text-center">Email: {{$guest[0]->g_email}}</h4>
@else
    <h1 class="text-center">Esta persona no ha sido invitada a este evento.</h1>
@endif
@if($event)
	<hr>
	<h3 class="text-center">Datos del Evento</h3>
	<h4 class="text-center">Nombre: {{$event[0]->e_name}}</h4>
	<h4 class="text-center">Fecha: {{$event[0]->e_date_start}}</h4>
@endif
<hr>
<div class="text-center">
	<button data-toggle="modal" data-target="#confirm" class="btn btn-primary btn-principal">Confirmar</button>
	<button class="btn btn-primary btn-cancelar">Cancelar</button>
</div>

<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form action="{{action('SecurityController@confirmAssitant')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Confirmación</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <h5 class="text-center"><strong>Desea dar entrada al evento a la persona seleccionada?</strong></h5>
            <hr>
            <div class="row">
                @if($event[0]->e_total_guests == 0)
                    <div class="col-md-12">
                        <h5 class="text-center"><strong>Evento sin limite.</strong></h5>
                    </div>
                @else
                    <div class="col-md-6">
                        <h5 class="text-center"><strong>Limite del evento</strong></h5>
                        <h5 class="text-center"><strong>{{$event[0]->e_total_guests}}</strong></h5>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-center"><strong>Total de ingreso</strong></h5>
                        @if( $count_event_assitants > $event[0]->e_total_guests)
                            <h5 class="text-center required"><strong>{{$count_event_assitants}}</strong></h5>
                        @else
                            <h5 class="text-center"><strong>{{$count_event_assitants}}</strong></h5>
                        @endif
                    </div>
                @endif
            </div>
            <input type="hidden" name="e_id" id="e_id" value="{{$event[0]->e_id}}">
            <input type="hidden" name="g_id" id="g_id" value="{{$guest[0]->g_id}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary btn-principal">Confirmar</button>
          </div>
        </form>
    </div>
  </div>
</div>