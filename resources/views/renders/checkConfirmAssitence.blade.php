@if($invitation->isEmpty())
	<h1 class="text-center">Esta persona no ha sido invitada a este evento.</h1>
@else
	<h3 class="text-center">Invitado de {{$invitation[0]->p_name}} {{$invitation[0]->p_lastname}} - {{$invitation[0]->p_membership_code}}</h3>
    <h4 class="text-center">Nombre: {{$invitation[0]->g_name}} {{$guest[0]->g_lastname}}</h4>
    <h4 class="text-center">Sexo: {{$invitation[0]->g_sex}}</h4>
    <h4 class="text-center">Cedula: {{$invitation[0]->g_identification}}</h4>
    <h4 class="text-center">Fecha de Nacimiento: {{$invitation[0]->g_date_of_birth}}</h4>
    <h4 class="text-center">Email: {{$invitation[0]->g_email}}</h4>
@endif