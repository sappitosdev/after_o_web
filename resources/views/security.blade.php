<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AFTER O</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('onepage/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('onepage/css/freelancer.css')}}" rel="stylesheet">
    <link href="{{asset('css/styles.css')}}" rel="stylesheet">
    <link href="{{asset('css/security.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('owlcarousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('onepage/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                @if(!$logo[0]->isEmpty)
                    <a class="navbar-brand" href="#page-top">
                      <img class="logo-nav" src="{{Storage::url($logo[0]->cp_content)}}">
                    </a>
                @endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li >
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-fw fa-power-off"></i> SALIR
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <div class="container-fluid">

      @if(Session::has('message'))
          <div class="alert alert-fixed {{ Session::get('alert-class') }} alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>{{ Session::get('message') }}</strong>
          </div>
      @endif
      <!-- Beneficios Section -->
      <section id="ingreso">
        <div class="hero-section-no-flex hero-security bg-secondary carousel-custom">
          <div class="row">
            <div class="col-md-12">
                    <form method="POST" id="checkGuests">
                        <h1 class="text-center hidden-xs">Verificar Ingreso</h1>
                        <h3 class="text-center visible-xs">Verificar Ingreso</h3>
                        <div class="col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                        	<div class="form-group">
                        		<label>Evento: (Eventos del día)</label>
                        		<select class="form-control" required name="event">
                        			<option disabled selected value="">Seleccione un evento</option>
                                    @foreach($events as $event)
                                        <option value="{{$event->e_id}}">{{$event->e_name}}</option>
                                    @endforeach
                        		</select>
                        	</div>
                            <div class="form-group">
                                <label>Ingrese la cedula:</label>
                                <div class="input-group">
                                    <input type="text" name="identification" class="form-control" placeholder="Search&hellip;" required maxlength="9">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
          </div>

          <div class="row">
            <div class="loader" hidden></div>
            <div class="container">
              <hr>
              <div id="data">
                  
              </div>

            </div>
          </div>
        </div>
          <hr>
      </section>
    </div>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- jQuery -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/actions/security.js')}}"></script>
    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
</body>

</html>