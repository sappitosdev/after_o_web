@extends('layouts.admin')
@section('content')
	<h4>Confirmar Asistencia</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Confirmar Asistencia</li>
	</ol>
	<form method="POST" action="{{action('AdminController@confirmAssitenceToGuest')}}" id="formConfirmAssitence">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Evento:</label>
					<select class="form-control" name="event" required>
						<option selected disabled value="">Seleccione un evento</option>
						@foreach($events as $event)
							<option value="{{$event->e_id}}">{{$event->e_name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Cedula:</label>
					<div class="input-group">
		                <input type="text" class="form-control" placeholder="Search" required maxlength="9" pattern="[0-9]+" name="identification">
		                <span class="input-group-btn">
		                    <button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-search"></i></button>
		                </span>
		            </div>
				</div>
			</div>
		</div>
	</form>

	<style type="text/css">
	 	.loader{
	 		left: 5% ;
	 		bottom: 5% ;
	 		top: inherit;
	 	}
	 </style>

     <div>
     	 <div class="loader" hidden></div>
		 <div id="data">
		          
		 </div>
	 </div>

	 

@endsection