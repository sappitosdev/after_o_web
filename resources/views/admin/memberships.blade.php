@extends('layouts.admin')
@section('content')
	<h4>Administrar Membresias</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Membresias</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newMembership"><i class="fa fa-plus"></i> Nueva Membresia</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Nombre</th>
				<th class="text-center">Default</th>
				<th class="text-center">Editar Membresía</th>
				<th class="text-center">Administrar Beneficios</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($memberships as $membership)
				<tr class="text-center">
					<td>
						{{$membership->m_name}}
					</td>
					<td>
						@if($membership->m_default)
							<i class="fa fa-check fa-2x"></i>
						@else
							<form method="POST" action="{{action('AdminController@setDefaultMembership')}}">
								{{ csrf_field() }}
								<button type="submit" class="btn btn-success">Default</button>
								<input type="hidden" name="m_id" id="m_id" value="{{$membership->m_id}}">
							</form>
						@endif
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editMembership" data-whatever="{{$membership}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#addBenefit" data-whatever="{{$membership->m_id}}">Administrar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteMembership" data-whatever="{{$membership->m_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$memberships->links()}}

<div class="modal fade" id="newMembership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addMembership')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nueva Membresía</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Nombre:</label>
	        	<input type="text" name="name" id="name" class="form-control" required>
	        </div>
	        <div class="form-group">
	        	<label>Beneficio:</label>
	        	<select class="selectpicker form-control" multiple required data-selected-text-format="count > 10" multiple data-actions-box="true" title="Seleccione los Beneficios" name="benefits[]" data-header="<h4 class='label label-principal'>Seleccione los Beneficios</h4>">
		          @foreach($categories as $categorie)
			          <optgroup label="{{$categorie->bc_benefit_categorie}}">
			          	@foreach($benefits->where('benefit_categories_bc_id', $categorie->bc_id) as $benefit)
		        			<option value="{{$benefit->b_id}}">{{$benefit->b_name}}</option>
		        		@endforeach
			          </optgroup>
		          @endforeach
				</select>
	        </div>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="addBenefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editBenefitMembership')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nueva Membresía</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Beneficio:</label>
	        	<select id="benefitsToAdd" class="selectpicker form-control" multiple required data-selected-text-format="count > 10" multiple data-actions-box="true" title="Seleccione los Beneficios" name="benefits[]" data-header="<h4 class='label label-principal'>Seleccione los Beneficios</h4>">
				</select>
	        </div>
	        <input type="hidden" name="m_id" id="m_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editMembership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editMembership')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nueva Membresía</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Nombre:</label>
	        	<input type="text" name="name" id="name" class="form-control" required>
	        	<input type="hidden" name="m_id" id="m_id">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteMembership" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteMembership')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Membresiía</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar la membresía seleccionada seleccionado?</strong></h5>
	        <input type="hidden" name="m_id" id="m_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection