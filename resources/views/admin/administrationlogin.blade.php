@extends('layouts.admin')
@section('content')
	
	<h4>Administrar Login</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Login</li>
	</ol>
	<form action="{{action('AdminPageController@editLoginSection')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		@if(!$title->isEmpty() || !$text->isEmpty() || !$image->isEmpty())
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Sección</button>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Título</label>
						<input type="text" name="title" class="form-control" value="{{$title[0]->cp_content}}">
					</div>
					<div class="form-group">
						<label>Texto Descriptivo</label>
						<textarea class="form-control" name="text" rows="10">{{$text[0]->cp_content}}</textarea>
					</div>
					<div class="form-group">
						<label>Link de Redireccion</label>
						<input type="text" name="link" class="form-control" value="{{$link[0]->cp_content}}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Imagen de Fondo - (1280 x 860)</label>
						<input type="file" name="image" class="form-control">
					</div>
					<label class="text-center center-block">Fondo Actual</label>
					<img src="{{Storage::url($image[0]->cp_content)}}" class="img-responsive img-thumbnail center-block" style="height: 220px;">
				</div>
			</div>
		@else
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-plus"></i> Agregar Sección</button>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Titulo</label>
						<input type="text" name="title" class="form-control">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Imagen de Fondo - (1280 x 860)</label>
						<input type="file" name="image" class="form-control" required>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Texto Descriptivo</label>
				<textarea class="form-control" name="text" rows="5"></textarea>
			</div>
			<div class="form-group">
				<label>Link de Redireccion</label>
				<input type="text" name="link" class="form-control">
			</div>
		@endif

	</form>
	

@endsection