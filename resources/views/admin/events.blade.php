@extends('layouts.admin')
@section('content')
		<h4>Administrar Eventos</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Eventos</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newEvent"><i class="fa fa-plus"></i> Nuevo Evento</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Evento</th>
				<th class="text-center">Tipo Evento</th>
				<th class="text-center">Fecha de Inicio</th>
				<th class="text-center">Fecha de Culminación</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($events as $event)
				<tr class="text-center">
					<td class="text-capitalize">
						{{$event->e_name}}
					</td>
					<td class="text-capitalize">
						{{$event->et_type}}
					</td>
					<td>
						{{$event->e_date_start}}
					</td>
					<td>
						{{$event->e_date_end}}
					</td>
					<td>
						<a class="btn btn-primary btn-secundario" href="{{route('editEvent',['id' => $event->e_id])}}">Editar</a>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteEvent" data-whatever="{{$event->e_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$events->links()}}

<div class="modal fade" id="newEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addEvent')}}" method="POST" enctype="multipart/form-data" id="formAddEvent">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Evento</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <button type="submit" class="btn btn-primary btn-principal guardar" style="margin-top:20px;" disabled>Guardar</button>
	      </div>
	      <div class="modal-body">

	      	<div class="row">
	      		<div class="col-md-4">
		      		<div class="form-group">
			        	<label>Nombre: *</label>
			        	<input type="text" name="name" id="name" class="form-control text-capitalize" required>
			        </div>
		        </div>
		        <div class="col-md-4">
		      		<div class="form-group">
			        	<label>Tipo de Evento: *</label>
			        	<select class="form-control text-capitalize" name="eventType" id="eventType" required>
			        		<option selected disabled value="">Seleccione un tipo de evento</option>
			        		@foreach($eventTypes as $eventType)
			        			<option value="{{$eventType->et_id}}">{{$eventType->et_type}}</option>
			        		@endforeach
			        	</select>
			        </div>
		        </div>
		        <div class="col-md-4">
		      		<div class="form-group">
			        	<label>Categoria Evento: *</label>
			        	<select class="form-control text-capitalize" name="eventCategorie" id="eventCategorie" required>
			        		<option selected disabled value="">Seleccione una categoria de evento</option>
			        		@foreach($eventCategories as $eventCategorie)
			        			<option value="{{$eventCategorie->ec_id}}">{{$eventCategorie->ec_categorie}}</option>
			        		@endforeach
			        	</select>
			        </div>
		        </div>
		        <div class="col-md-3">
		      		<div class="form-group">
			        	<label>Fecha de Inicio: *</label>
			        	<input type="text" name="dateStart" id="dateStart" class="form-control datepicker" required>
			        </div>
		        </div>
		        <div class="col-md-3">
		      		<div class="form-group">
			        	<label>Hora de Inicio: *</label>
			        	<input type="time" name="timeStart" id="dateStart" class="form-control" required>
			        </div>
		        </div>
		        <div class="col-md-3">
		      		<div class="form-group">
			        	<label>Fecha de Culminación: *</label>
			        	<input type="text" name="dateEnd" id="dateEnd" class="form-control datepicker" required>
			        </div>
		        </div>
		        <div class="col-md-3">
		      		<div class="form-group">
			        	<label>Hora de Culminación: *</label>
			        	<input type="time" name="timeEnd" id="dateEnd" class="form-control" required>
			        </div>
		        </div>
		        <div class="col-md-6">
		      		<div class="form-group">
			        	<label>Descripción corta: *</label>
			        	<textarea class="form-control" rows="5" name="shortdescription" id="shortdescription" required></textarea>
			        </div>
		        </div>
		        <div class="col-md-6">
		      		<div class="form-group">
			        	<label>Descripción: *</label>
			        	<textarea class="form-control" rows="5"  name="description" id="description" required></textarea>
			        </div>
		        </div>
		        <div class="col-md-12">
		      		<div class="form-group">
			        	<label>Membresias: *</label>
			        	<select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las Membresias" name="memberships[]" id="memberships" data-header="<h4 class='label label-principal'>Seleccione las membresias</h4>">
			        		@foreach($memberships as $membership)
			        			<option value="{{$membership->m_id}}">{{$membership->m_name}}</option>
			        		@endforeach
			        	</select>
			        </div>
		        </div>
		        <div class="col-md-12">
		        	<div class="row hidden">
		        		<div class="col-md-6">
				      		<div class="form-group">
					        	<label>Latitud: *</label>
					        	<input type="text" name="latitud" id="latitud" class="form-control" required hidden>
					        </div>
				        </div>
				        <div class="col-md-6">
				      		<div class="form-group">
					        	<label>Longitud: *</label>
					        	<input type="text" name="longitud" id="longitud" class="form-control" required hidden>
					        </div>
				        </div>
			        </div>
			        <hr>
			        <h5 class="text-center"><strong>Debe seleccionar un punto obligatoriamente, sino no podra guardar el evento.</strong></h5>
			        <hr>
			        <div class="form-group">
				        <label>Ingrese una palabra clave</label>
                        <div class="input-group">
                            <input type="text" name="address" id="address" class="form-control text-capitalize" placeholder="Search&hellip;">
                            <span class="input-group-btn">
                                <button id="searchAddress" type="button" class="btn btn-primary btn-principal"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
		            </div>
			        
			        <div id="map" style="height:300px;"></div>
			        <hr>
			        <div class="form-group">
			        	<label>Ingrese una dirección exacta</label>
			        	<textarea name="address_location" class="form-control" row="5" required></textarea>
			        </div>
		        </div>
	      	</div>
	      	<hr>
	      	<div class="restriction">
		      	<h4>Restricciones Asistentes</h4>
		      	<div class="row">
		      		<div class="col-md-4">
			      		<div class="form-group">
				        	<label>Total Asistentes: *</label>
				        	<input type="text"  name="totalguests" id="totalguests" class="form-control" required value="0">
				        </div>
			        </div>
			        <div class="col-md-4">
	                    <div class="form-group text-center">
	                        <label>Invitados por miembro? </label>
	                        <div class="radio">
	                          <label>
	                            <input type="radio" name="invxmiembros"  value="Si">Si
	                          </label>
	                          <label>
	                            <input type="radio" name="invxmiembros" value="No" checked>No
	                          </label>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group text-center">
	                        <label>Restricción de edad para socios? </label>
	                        <div class="radio">
	                          <label>
	                            <input type="radio" name="redad"  value="Si">Si
	                          </label>
	                          <label>
	                            <input type="radio" name="redad" value="No" checked>No
	                          </label>
	                        </div>
	                    </div>
	                </div>
		      	</div>
		      	<div class="row">
		      		<div class="col-md-4">
	                    <label>Edad Minima de Asistentes: *</label>
				        <input type="text" name="minimumage" id="minimumage" class="form-control" required disabled maxlength="3" pattern="[0-9]+">
	                </div>
	                <div class="col-md-4">
	                    <label>Sexo: *</label>
				        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione los sexos" name="sexs[]" id="sexs" data-header="<h4 class='label label-principal'>Seleccione los sexos</h4>">
				        	@foreach($sexs as $sex)
			        			<option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
			        		@endforeach
				        </select>
	                </div>
	                <div class="col-md-4">
	                    <label>Ciudad: *</label>
				        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las ciudades" name="cities[]" id="cities" data-header="<h4 class='label label-principal'>Seleccione las ciudades</h4>">
				        	@foreach($cities as $citie)
			        			<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
			        		@endforeach
				        </select>
	                </div>
		      	</div>
	      	</div>
	      	<hr>
	      	<div class="restriction">
		      	<h4>Restricciones para Invitaciones</h4>
		      	<div class="row">
		      		<div class="col-md-4">
			      		<div class="form-group">
				        	<label>N° de Invitados Por Miembro:</label>
				        	<input type="text" name="numberGuestxMember" id="numberGuestXMember" class="form-control" required disabled>
				        </div>
			        </div>
			        <div class="col-md-8">
	                    <div class="form-group">
	                        <label>Membresia con invitados: </label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las membresias" name="membershipsguest[]" id="membersguests" data-header="<h4 class='label label-principal'>Seleccione las membresias</h4>" disabled>
	                        	@foreach($memberships as $membership)
				        			<option value="{{$membership->m_id}}">{{$membership->m_name}}</option>
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group">
	                        <label>Sexo Miembros:</label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione los sexos" name="sexsmembers[]" id="sexsmembers" data-header="<h4 class='label label-principal'>Seleccione los sexos</h4>" disabled>
	                        	@foreach($sexs as $sex)
				        			<option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group">
	                        <label>Ciudad Miembros:</label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las ciudades" name="citiesmembers[]" id="citiesmembers" data-header="<h4 class='label label-principal'>Seleccione las ciudades</h4>" disabled>
	                        	@foreach($cities as $citie)
				        			<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <label>Edad Minima Miembros:</label>
				        <input type="text" name="minimumagemembers" id="minimumagemembers" class="form-control" required disabled>
	                </div>
		      	</div>
	      	</div>
	      	<hr>
	      	<div class="row">
	      		<div class="col-md-12">
	      			<label>Foto Promocional: * - (300 x 300)</label>
				    <input type="file" name="promotionalpicture" id="promotionalpicture" class="form-control">
	      		</div>
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal guardar" disabled>Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>


<div class="modal fade" id="deleteEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteEvent')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Evento</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el Evento seleccionado?</strong></h5>
	        <input type="hidden" name="e_id" id="e_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<script type="text/javascript">
	var map;
	var markers = [];
	var geocoder;        
	function initMap() {
		geocoder = new google.maps.Geocoder();
	    var haightAshbury = {lat: 10.4805937, lng: -66.90360629999998};

	    map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 12,
	      center: haightAshbury,
	    });

	    // This event listener will call addMarker() when the map is clicked.
	    map.addListener('click', function(event) {
	      addMarker(event.latLng);
	      $('#latitud').val(event.latLng.lat());
	      $('#longitud').val(event.latLng.lng());
	      $( ".guardar" ).each(function() {
			  $( this ).removeAttr( "disabled" );
		  });
	    });

	}

	function addMarker(location) {
		deleteMarkers();
	    var marker = new google.maps.Marker({
	      position: location,
	      map: map
	    });
	    markers.push(marker);
	    $('#address').val('');
	}

	// Sets the map on all markers in the array.
	  function setMapOnAll(map) {
	    for (var i = 0; i < markers.length; i++) {
	      markers[i].setMap(map);
	    }
	  }

	  // Removes the markers from the map, but keeps them in the array.
	  function clearMarkers() {
	    setMapOnAll(null);
	  }

	  // Shows any markers currently in the array.
	  function showMarkers() {
	    setMapOnAll(map);
	  }

	  // Deletes all markers in the array by removing references to them.
	  function deleteMarkers() {
	    clearMarkers();
	    markers = [];
	  }

	  function codeAddress() {
	  	deleteMarkers();
	    var address = document.getElementById('address').value;
	    geocoder.geocode( { 'address': address}, function(results, status) {
	      if (status == 'OK') {
	        map.setCenter(results[0].geometry.location);
	        var marker = new google.maps.Marker({
	            map: map,
	            position: results[0].geometry.location
	        });
	        markers.push(marker);
	        $('#latitud').val(results[0].geometry.location.lat());
	      	$('#longitud').val(results[0].geometry.location.lng());
	      	$('#address').val('');
	      } else {
	        alert('Geocode was not successful for the following reason: ' + status);
	      }
	    });
	  }

      $('#searchAddress').click(function(){
      	codeAddress();
      	$( ".guardar" ).each(function() {
			$( this ).removeAttr( "disabled" );
		});
      });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg7sqXHUdMNHLt7uhiJUcP0nV5nHylsfo&callback=initMap"
async defer></script>
@endsection