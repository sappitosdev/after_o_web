@extends('layouts.admin')
@section('content')
	<h4>Administrar Logotipo</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Logotipo</li>
	</ol>
	<form action="{{action('AdminPageController@editLogo')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		@if(!$logo->isEmpty())
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Logo</button>
			</div>
			<div class="form-group">
				<label>Logo * - (872 x 872) "Se recomienda medidas iguales tanto de ancho y alto." </label>
				<input type="file" name="logo" class="form-control" required>
			</div>
			<img src="{{Storage::url($logo[0]->cp_content)}}" class="img-responsive img-thumbnail text-center center-block logo-preview">
		@else
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-plus"></i> Agregar Logo</button>
			</div>
			<div class="form-group">
				<label>Logo *</label>
				<input type="file" name="logo" class="form-control" required>
			</div>
		@endif
	</form>
	
@endsection