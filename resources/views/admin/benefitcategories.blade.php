@extends('layouts.admin')
@section('content')
	<h4>Administrar Categorias de Beneficios</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Categorias de Beneficios</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newBenefitCategorie"><i class="fa fa-plus"></i> Nueva Categoria de Beneficios</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Categoria de Beneficio</th>
				<th class="text-center">Icono</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($benefitcategories as $categorie)
				<tr class="text-center">
					<td>
						{{$categorie->bc_benefit_categorie}}
					</td>
					<td>
						<img src="{{Storage::url($categorie->bc_icon)}}" class="img-responsive icon center-block img-thumbnail">
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editBenefitCategorie" data-whatever="{{$categorie}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteBenefitCategorie" data-whatever="{{$categorie->bc_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$benefitcategories->links()}}

<div class="modal fade" id="newBenefitCategorie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addBenefitCategorie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nueva Categoria de Beneficios</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Categoria:</label>
	        	<input type="text" name="categorie" id="categorie" class="form-control" required>
	        </div>
	        <div class="form-group">
	        	<label>Icono: - (30 x 30)</label>
	        	<input type="file" name="icon" id="icon" class="form-control" required>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>


<div class="modal fade" id="editBenefitCategorie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editBenefitCategorie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Categoria de Beneficio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Categoria:</label>
	        	<input type="text" name="categorie" id="categorie" class="form-control" required>
	        </div>
	        <div class="form-group">
	        	<label>Icono: (30 x 30)</label>
	        	<input type="file" name="icon" id="editIcon" class="form-control">
	        </div>
	        <div class="form-group">
	        	<hr>
	        	<label class="text-center center-block">Icono Actual</label>
	        	<img src="" id="currentIcon" class="img-responsive img-thumbnail text-center center-block icon">
	        	<input type="hidden" name="bc_id" id="bc_id">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>


<div class="modal fade" id="deleteBenefitCategorie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteBenefitCategorie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Categoria de Beneficio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el tipo de evento seleccionada?</strong></h5>
	        <input type="hidden" name="bc_id" id="bc_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection