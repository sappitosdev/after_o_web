@extends('layouts.admin')
@section('content')
	<h4>Administrar Texto Pie de Pagina</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Texto Pie de Pagina</li>
	</ol>
	<form action="{{action('AdminPageController@editFooterText')}}" method="POST">
		{{ csrf_field() }}
		<div class="div-btns-principal">
			<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Sección</button>
		</div>
		<div class="form-group">
			<label>Titulo: </label>
			@if(!$titlefooter->isEmpty())
				<input type="text" name="title" class="form-control" value="{{$titlefooter[0]->cp_content}}">
			@else
				<input type="text" name="title" class="form-control">
			@endif
		</div>
		<div class="form-group">
			<label>Texto: </label>
			<textarea class="editorHTML" name="footertext">
				@if(!$textfooter->isEmpty())
					{{$textfooter[0]->cp_content}}
				@endif
			</textarea>
		</div>
	</form>
	
@endsection