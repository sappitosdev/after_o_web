@extends('layouts.admin')
@section('content')	
	
	<h4>Administrar Seccion Eventos</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Seccion Eventos</li>
	</ol>
	<form action="{{action('AdminPageController@editBannerPrincipalEvent')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		@if(!$banner->isEmpty())
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Banner</button>
			</div>
			<div class="form-group">
				<label>Banner - (1600 x 700)</label>
				<input type="file" name="image" class="form-control">
			</div>
			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" class="form-control" value="{{$banner[0]->cp_title}}">
			</div>
			<label class="text-center center-block">Banner Actual</label>
			<img src="{{Storage::url($banner[0]->cp_content)}}" class="img-responsive img-thumbnail center-block" style="height: 220px;">
		@else
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-plus"></i> Agregar Banner</button>
			</div>
			<div class="form-group">
				<label>Banner - (1600 x 700)</label>
				<input type="file" name="image" class="form-control" required>
			</div>
			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" class="form-control">
			</div>
		@endif
	</form>
	

@endsection