@extends('layouts.admin')
@section('content')
	
	<h4>Administrar Seccion Membresías</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Seccion Membresías</li>
	</ol>
	<form action="{{action('AdminPageController@editAdDetailEvent')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		@if(!$ad->isEmpty())
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Anuncio</button>
			</div>
			<div class="form-group">
				<label>Imagen de Anuncio - (1500 x 630)</label>
				<input type="file" name="image" class="form-control">
			</div>
			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" class="form-control" value="{{$ad[0]->cp_title}}">
			</div>
			<label class="text-center center-block">Anuncio Actual</label>
			<img src="{{Storage::url($ad[0]->cp_content)}}" class="img-responsive img-thumbnail center-block" style="height: 220px;">
		@else
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-plus"></i> Agregar Anuncio</button>
			</div>
			<div class="form-group">
				<label>Imagen de Anuncio - (1200 x 300)</label>
				<input type="file" name="image" class="form-control" required>
			</div>
			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" class="form-control">
			</div>
		@endif
	</form>
	

@endsection