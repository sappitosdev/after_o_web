@extends('layouts.admin')
@section('content')
		<h4>Administrar Usuarios</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Usuarios</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newUser"><i class="fa fa-plus"></i> Nuevo Usuario</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Nombre</th>
				<th class="text-center">Email</th>
				<th class="text-center">Rol</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr class="text-center">
					<td class="text-capitalize">
						{{$user->name}}
					</td>
					<td>
						{{$user->email}}
					</td>
					<td class="text-capitalize">
						{{$user->rol}}
					</td>
					<td>
						<button class="btn btn-danger btn-secundario" data-toggle="modal" data-target="#editUser" data-whatever="{{$user}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteUser" data-whatever="{{$user->id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$users->links()}}

<div class="modal fade" id="newUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addUser')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Usuario</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Nombre:</label>
	        	<input type="text" name="name" class="form-control text-capitalize" required>
	        </div>
	        <div class="form-group">
	        	<label>Correo Electronico:</label>
	        	<input type="email" name="email" class="form-control" required>
	        </div>
	        <div class="form-group">
	        	<label>Contraseña:</label>
	        	<input type="password" name="password" class="form-control text-capitalize" required>
	        </div>
	        <div class="form-group">
	        	<label>Rol:</label>
	        	<select class="form-control" required name="rol">
	        		<option selected disabled value="">Seleccione un rol</option>
	        		<option value="ADMIN">ADMINISTRADOR</option>
	        		<option value="SECURITY">SEGURIDAD</option>
	        		<option value="EMPLOYEE">EMPLEADO</option>
	        	</select>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editUser')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Nombre:</label>
	        	<input type="text" name="name" id="name" class="form-control text-capitalize" required>
	        </div>
	        <div class="form-group">
	        	<label>Correo Electronico:</label>
	        	<input type="email" name="email" id="email" class="form-control" required>
	        </div>
	        <div class="form-group">
	        	<label>Rol:</label>
	        	<select class="form-control" required name="rol" id="rol">
	        		<option selected disabled value="">Seleccione un rol</option>
	        		<option value="ADMIN">ADMINISTRADOR</option>
	        		<option value="SECURITY">SEGURIDAD</option>
	        		<option value="EMPLOYEE">EMPLEADO</option>
	        	</select>
	        </div>
	        <input type="hidden" name="u_id" id="u_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteUser')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Concepto</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el usuario seleccionado?</strong></h5>
	        <input type="hidden" name="id" id="id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection