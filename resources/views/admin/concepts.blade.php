@extends('layouts.admin')
@section('content')
		<h4>Administrar Conceptos</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Conceptos</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newConcept"><i class="fa fa-plus"></i> Nuevo Concepto</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Nombre</th>
				<th class="text-center">Categoria</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($concepts as $concept)
				<tr class="text-center">
					<td class="text-capitalize">
						{{$concept->ct_concept}}
					</td>
					<td class="text-capitalize">
						{{$concept->ct_description}}
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteConcept" data-whatever="{{$concept->ct_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$concepts->links()}}

<div class="modal fade" id="newConcept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addConcept')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Concepto</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Nombre:</label>
	        	<input type="text" name="concept" id="concept" class="form-control text-capitalize" required>
	        </div>
	        <div class="form-group">
	        	<label>Categoria:</label>
	        	<select class="form-control" required name="description">
	        		<option selected disabled value="">Seleccione una categoria de concepto</option>
	        		<option value="SEXO">SEXO</option>
	        		<option value="ESTADO_CIVIL">ESTADO CIVIL</option>
	        	</select>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteConcept" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteConcept')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Concepto</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el concepto seleccionado?</strong></h5>
	        <input type="hidden" name="ct_id" id="ct_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection