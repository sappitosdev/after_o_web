@extends('layouts.admin')
@section('content')

	<h4>Administrar Aliados</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Aliados</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newAllie"><i class="fa fa-plus"></i> Nuevo Aliado</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Imagen Aliado</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($allies as $allie)
				<tr class="text-center">
					<td>
						<img src="{{Storage::url($allie->cp_content)}}" class="img-responsive img-thumbnail" style="height:80px;">
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editAllie" data-whatever="{{$allie}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteAllie" data-whatever="{{$allie->cp_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$allies->links()}}

<div class="modal fade" id="newAllie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminPageController@addAllie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Aliado</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Imagen: * - (872 x 872) "Se recomienda medidas iguales tanto de ancho y alto."</label>
	        	<input type="file" name="allie" id="allie" class="form-control">
	        </div>
	        <img src="" id="preview" class="img-responsive img-thumbnail text-center center-block" style="height: 150px;">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editAllie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminPageController@editAllie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Aliado</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label class="text-center center-block">Aliado Actual:</label>
	        	<img src="" id="allieactual" class="img-responsive img-thumbnail text-center center-block" style="height:150px;">
	        </div>
	      	<div class="form-group">
	        	<label>Imagen: * - (872 x 872) "Se recomienda medidas iguales tanto de ancho y alto."</label>
	        	<input type="file" name="allie" id="allie2" class="form-control">
	        </div>
	        <img src="" id="preview2" class="img-responsive img-thumbnail text-center center-block" style="height:150px;">
	        <input type="hidden" name="cp_id" id="cp_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteAllie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminPageController@deleteAllie')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Aliado</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el Aliado seleccionado?</strong></h5>
	        <input type="hidden" name="cp_id" id="cp_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

@endsection