@extends('layouts.admin')
@section('content')
	<h4>Administrar Beneficios</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Beneficios</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newBenefit"><i class="fa fa-plus"></i> Nuevo Beneficio</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Nombre</th>
				<th class="text-center">Descripción</th>
				<th class="text-center">Descripción Corta</th>
				<th class="text-center">Logo de Marca</th>
				<th class="text-center">Imagen Promocional</th>
				<th class="text-center">Categoria</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($benefits as $benefit)
				<tr class="text-center">
					<td>
						{{$benefit->b_name}}
					</td>
					<td>
						{{$benefit->b_description}}
					</td>
					<td>
						{{$benefit->b_short_description}}
					</td>
					<td>
						<img src="{{Storage::url($benefit->b_brand_logo)}}" class="img-responsive icon center-block img-thumbnail">
						<button class="btn btn-primary btn-secundario btn-xs margin-top-sm" data-toggle="modal" data-target="#editBrandLogo" data-whatever="{{$benefit->b_id}}">
							<i class="fa fa-edit" data-toggle="tooltip" data-placement="bottom" title="Editar Logo de Marca"></i>
						</button>
					</td>
					<td>
						<img src="{{Storage::url($benefit->b_promotional_picture)}}" class="img-responsive icon center-block img-thumbnail">
						<button class="btn btn-primary btn-secundario btn-xs margin-top-sm" data-toggle="modal" data-target="#editPromotionalImage" data-whatever="{{$benefit->b_id}}">
							<i class="fa fa-edit" data-toggle="tooltip" data-placement="bottom" title="Editar Imagen Promocional"></i>
						</button>
					</td>
					<td>
						{{$benefit->bc_benefit_categorie}}
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editBenefit" data-whatever="{{$benefit}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteBenefit" data-whatever="{{$benefit->b_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$benefits->links()}}

<div class="modal fade" id="newBenefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addBenefit')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Beneficio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>

	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Categoria de Beneficio: *</label>
			        	<select name="categorie" class="form-control" required>
			        		<option selected disabled value="">Seleccione una categoria de evento.</option>
			        		@foreach($categories as $categorie)
			        			<option value="{{$categorie->bc_id}}">{{$categorie->bc_benefit_categorie}}</option>
			        		@endforeach
			        	</select>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Nombre: *</label>
			        	<input type="text" name="name" class="form-control" required>
			        </div>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Descripción: *</label>
			        	<textarea name="descripction" class="form-control" rows="5"></textarea>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Descripción Corta: *</label>
			        	<textarea name="shortDescripction" class="form-control" rows="5"></textarea>
			        </div>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Ciudad: *</label>
			        	<select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las Ciudades" name="cities[]" id="cities" data-header="<h4 class='label label-principal'>Seleccione una ciudad.</h4>">
			        		@foreach($cities as $citie)
			        			<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
			        		@endforeach
			        	</select>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Vigencia:</label>
			        	<input type="text" name="validity" class="form-control datepicker">
			        </div>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Logo de la marca: * - (50 x 50)</label>
			        	<input type="file" name="brandlogo" class="form-control" required>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Foto Promocional: * - (500 x 300)</label>
			        	<input type="file" name="promotionalpicture" class="form-control" required>
			        </div>
	      		</div>
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editBenefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editBenefit')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Beneficio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Categoria de Beneficio: *</label>
			        	<select name="categorie" id="categorie" class="form-control" required>
			        		<option selected disabled value="">Seleccione una categoria de evento.</option>
			        		@foreach($categories as $categorie)
			        			<option value="{{$categorie->bc_id}}">{{$categorie->bc_benefit_categorie}}</option>
			        		@endforeach
			        	</select>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Nombre: *</label>
			        	<input type="text" name="name" id="name" class="form-control" required>
			        </div>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Descripción: *</label>
			        	<textarea name="descripction" id="descripction" class="form-control" rows="5"></textarea>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Descripción Corta: *</label>
			        	<textarea name="shortDescripction" id="shortDescripction" class="form-control" rows="5"></textarea>
			        </div>
	      		</div>
	      	</div>
	      	<div class="row">
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Ciudad: *</label>
			        	<select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las Ciudades" name="cities[]" id="cities" data-header="<h4 class='label label-principal'>Seleccione una ciudad.</h4>">
			        		@foreach($cities as $citie)
			        			<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
			        		@endforeach
			        	</select>
			        </div>
	      		</div>
	      		<div class="col-md-6">
	      			<div class="form-group">
			        	<label>Vigencia: *</label>
			        	<input type="text" name="validity" id="validity" class="form-control datepicker">
			        </div>
	      		</div>
	      		<input type="hidden" name="b_id" id="b_id">
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteBenefit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteBenefit')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Categoria de Beneficio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el beneficio seleccionada?</strong></h5>
	        <input type="hidden" name="b_id" id="b_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editBrandLogo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editBrandLogo')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Logo de Marca</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	      		<label>Logo de Marca: * - (50 x 50)</label>
	      		<input type="file" name="brandlogo" class="form-control" required>
	      	</div>
	        <input type="hidden" name="b_id" id="b_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editPromotionalImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editPromotionalImage')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Imagen Promocional</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	      		<label>Imagen Promocional: * - (500 x 300)</label>
	      		<input type="file" name="promotionalpicture" class="form-control" required>
	      	</div>
	        <input type="hidden" name="b_id" id="b_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection