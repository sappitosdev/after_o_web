@extends('layouts.admin')
@section('content')
		<h4>Administrar Tipos de Eventos</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Tipos de Eventos</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newCategorie"><i class="fa fa-plus"></i> Nuevo Tipo</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Tipo de Evento</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($eventypes as $eventype)
				<tr class="text-center">
					<td class="text-uppercase">
						{{$eventype->et_type}}
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editType" data-whatever="{{$eventype}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteType" data-whatever="{{$eventype->et_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$eventypes->links()}}

<div class="modal fade" id="newCategorie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@addType')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Tipo de Evento</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Tipo de Evento:</label>
	        	<input type="text" name="type" id="type" class="form-control text-capitalize" required>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>


<div class="modal fade" id="editType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@editType')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Tipo de Evento</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	      		<label>Tipo de Evento</label>
	      		<input type="text" name="type" id="type" class="form-control text-capitalize" required>
	        	<input type="hidden" name="et_id" id="et_id">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>


<div class="modal fade" id="deleteType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('AdminController@deleteType')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Eliminar Tipo de Evento</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el tipo de evento seleccionada?</strong></h5>
	        <input type="hidden" name="et_id" id="et_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection