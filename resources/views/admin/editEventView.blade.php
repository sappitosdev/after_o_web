@extends('layouts.admin')
@section('content')
		<h4>Editar Evento</h4>
		<hr>
		<ol class="breadcrumb">
		  <li><a href="#">Admin</a></li>
		  <li><a href="#">Administrar</a></li>
		  <li class="active">Editar Evento</li>
		</ol>
		<form method="POST" action="{{ action('AdminController@editEvent') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal"><i class="fa fa-edit"></i> Editar Evento</button>
			</div>
			<hr>
			<div class="row">
		      		<div class="col-md-4">
			      		<div class="form-group">
				        	<label>Nombre: *</label>
				        	<input type="text" name="name" id="name" class="form-control text-capitalize" required  value="{{$event[0]->e_name}}">
				        </div>
			        </div>
			        <div class="col-md-4">
			      		<div class="form-group">
				        	<label>Tipo de Evento: *</label>
				        	<select class="form-control text-capitalize" name="eventType" id="eventType" required>
				        		<option selected disabled value="">Seleccione un tipo de evento</option>
				        		@foreach($eventTypes as $eventType)
				        			<option value="{{$eventType->et_id}}">{{$eventType->et_type}}</option>
				        		@endforeach
				        	</select>
				        </div>
			        </div>
			        <div class="col-md-4">
			      		<div class="form-group">
				        	<label>Categoria Evento: *</label>
				        	<select class="form-control text-capitalize" name="eventCategorie" id="eventCategorie" required>
				        		<option selected disabled value="">Seleccione una categoria de evento</option>
				        		@foreach($eventCategories as $eventCategorie)
				        			<option value="{{$eventCategorie->ec_id}}">{{$eventCategorie->ec_categorie}}</option>
				        		@endforeach
				        	</select>
				        </div>
			        </div>
			        <div class="col-md-3">
			      		<div class="form-group">
				        	<label>Fecha de Inicio: *</label>
				        	<input type="text" name="dateStart" id="dateStart" class="form-control datepicker" required value="{{$event[0]->e_date_start}}">
				        </div>
			        </div>
			        <div class="col-md-3">
			      		<div class="form-group">
				        	<label>Hora de Inicio: *</label>
				        	<input type="time" name="timeStart" id="dateStart" class="form-control" required value="{{$event[0]->e_time_start}}">
				        </div>
			        </div>
			        <div class="col-md-3">
			      		<div class="form-group">
				        	<label>Fecha de Culminación: *</label>
				        	<input type="text" name="dateEnd" id="dateEnd" class="form-control datepicker" required value="{{$event[0]->e_date_end}}">
				        </div>
			        </div>
			        <div class="col-md-3">
			      		<div class="form-group">
				        	<label>Hora de Culminación: *</label>
				        	<input type="time" name="timeEnd" id="dateEnd" class="form-control" required value="{{$event[0]->e_time_end}}">
				        </div>
			        </div>
			        <div class="col-md-6">
			      		<div class="form-group">
				        	<label>Descripción corta: *</label>
				        	<textarea class="form-control" rows="5" name="shortdescription" id="shortdescription" required>{{$event[0]->e_short_description}}</textarea>
				        </div>
			        </div>
			        <div class="col-md-6">
			      		<div class="form-group">
				        	<label>Descripción: *</label>
				        	<textarea class="form-control" rows="5"  name="description" id="description" required >{{$event[0]->e_description}}</textarea>
				        </div>
			        </div>
			        <div class="col-md-12">
			      		<div class="form-group">
				        	<label>Membresias: *</label>
				        	<select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las Membresias" name="memberships[]" id="memberships" data-header="<h4 class='label label-principal'>Seleccione las membresias</h4>">
				        		@foreach($memberships_all as $membership)
				        			@if($membership->checked==true)
				        				<option value="{{$membership->m_id}}" selected="">{{$membership->m_name}}</option>
				        			@else
				        				<option value="{{$membership->m_id}}">{{$membership->m_name}}</option>
				        			@endif
				        		@endforeach
				        	</select>
				        </div>
			        </div>
			        <div class="col-md-12">
			        	<div class="row hidden">
			        		<div class="col-md-6">
					      		<div class="form-group">
						        	<label>Latitud: *</label>
						        	<input type="text" name="latitud" id="latitud" class="form-control" required hidden value="{{$event[0]->e_latitud}}">
						        </div>
					        </div>
					        <div class="col-md-6">
					      		<div class="form-group">
						        	<label>Longitud: *</label>
						        	<input type="text" name="longitud" id="longitud" class="form-control" required hidden value="{{$event[0]->e_longitud}}">
						        </div>
					        </div>
				        </div>
				        <hr>
				        <div class="form-group">
					        <label>Ingrese una palabra clave</label>
	                        <div class="input-group">
	                            <input type="text" name="address" id="address" class="form-control text-capitalize" placeholder="Search&hellip;">
	                            <span class="input-group-btn">
	                                <button id="searchAddress" type="button" class="btn btn-primary btn-principal"><i class="fa fa-search"></i></button>
	                            </span>
	                        </div>
			            </div>
				        
				        <div id="map" style="height:300px;"></div>

				        <div class="form-group">
				        	<label>Ingrese una dirección exacta</label>
				        	<textarea name="address_location" class="form-control" row="5" required value="{{$event[0]->e_address_location}}"></textarea>
				        </div>
			        </div>
		    </div>
	      	<hr>
	      	<div class="restriction">
		      	<h4>Restricciones Asistentes</h4>
		      	<div class="row">
		      		<div class="col-md-4">
			      		<div class="form-group">
				        	<label>Total Asistentes: *</label>
				        	<input type="text"  name="totalguests" id="totalguests" class="form-control" required value="{{$event[0]->e_total_guests}}">
				        </div>
			        </div>
			        <div class="col-md-4">
	                    <div class="form-group text-center">
	                        <label>Invitados por miembro? </label>
	                        <div class="radio">
	                          <label>
	                            <input type="radio" name="invxmiembros"  value="Si">Si
	                          </label>
	                          <label>
	                            <input type="radio" name="invxmiembros" value="No" checked>No
	                          </label>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group text-center">
	                        <label>Restricción de edad para socios? </label>
	                        <div class="radio">
	                          <label>
	                            <input type="radio" name="redad"  value="Si">Si
	                          </label>
	                          <label>
	                            <input type="radio" name="redad" value="No" checked>No
	                          </label>
	                        </div>
	                    </div>
	                </div>
		      	</div>
		      	<div class="row">
		      		<div class="col-md-4">
	                    <label>Edad Minima de Asistentes: *</label>
				        <input type="text" name="minimumage" id="minimumage" class="form-control" required disabled maxlength="3" pattern="[0-9]+" value="{{$event[0]->e_minimum_age}}">
	                </div>
	                <div class="col-md-4">
	                    <label>Sexo: *</label>
				        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione los sexos" name="sexs[]" id="sexs" data-header="<h4 class='label label-principal'>Seleccione los sexos</h4>">
				        	@foreach($sexs_all as $sex)
				        		@if($sex->checked==true)
			        				<option value="{{$sex->ct_concept}}" selected>{{$sex->ct_concept}}</option>
			        			@else
			        				<option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
			        			@endif
			        		@endforeach
				        </select>
	                </div>
	                <div class="col-md-4">
	                    <label>Ciudad: *</label>
				        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las ciudades" name="cities[]" id="cities" data-header="<h4 class='label label-principal'>Seleccione las ciudades</h4>">
				        	@foreach($cities_all as $citie)
			        			@if($citie->checked==true)
			        				<option value="{{$citie->c_id}}" selected>{{$citie->c_citie}}</option>
			        			@else
			        				<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
			        			@endif
			        		@endforeach
				        </select>
	                </div>
		      	</div>
	      	</div>
	      	<hr>
	      	<div class="restriction">
		      	<h4>Restricciones para Invitaciones</h4>
		      	<div class="row">
		      		<div class="col-md-4">
			      		<div class="form-group">
				        	<label>N° de Invitados Por Miembro:</label>
				        	<input type="text" name="numberGuestxMember" id="numberGuestXMember" class="form-control" required disabled value="{{$event[0]->e_number_guests_x_members}}">
				        </div>
			        </div>
			        <div class="col-md-8">
	                    <div class="form-group">
	                        <label>Membresia con invitados: </label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las membresias" name="membershipsguest[]" id="membersguests" data-header="<h4 class='label label-principal'>Seleccione las membresias</h4>" disabled>
	                        	@foreach($memberships_all_guests as $membership)
				        			@if($membership->checked==true)
				        				<option value="{{$membership->m_id}}" selected="">{{$membership->m_name}}</option>
				        			@else
				        				<option value="{{$membership->m_id}}">{{$membership->m_name}}</option>
				        			@endif
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group">
	                        <label>Sexo Miembros:</label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione los sexos" name="sexsmembers[]" id="sexsmembers" data-header="<h4 class='label label-principal'>Seleccione los sexos</h4>" disabled>
	                        	@foreach($sexs_all_guests as $sex)
				        			@if($sex->checked==true)
			        					<option value="{{$sex->ct_concept}}" selected>{{$sex->ct_concept}}</option>
				        			@else
				        				<option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
				        			@endif
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <div class="form-group">
	                        <label>Ciudad Miembros:</label>
	                        <select class="selectpicker form-control text-capitalize" multiple required data-selected-text-format="count > 10" data-actions-box="true" title="Seleccione las ciudades" name="citiesmembers[]" id="citiesmembers" data-header="<h4 class='label label-principal'>Seleccione las ciudades</h4>" disabled>
	                        	@foreach($cities_all_guests as $citie)
				        			@if($citie->checked==true)
				        				<option value="{{$citie->c_id}}" selected>{{$citie->c_citie}}</option>
				        			@else
				        				<option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
				        			@endif
				        		@endforeach
	                        </select>
	                    </div>
	                </div>
	                <div class="col-md-4">
	                    <label>Edad Minima Miembros:</label>
				        <input type="text" name="minimumagemembers" id="minimumagemembers" class="form-control" value="{{$event[0]->e_minimum_age_x_members}}" required disabled>
	                </div>
		      	</div>
	      	</div>
	      	<hr>
	      	<div class="row">
	      		@if($event[0]->e_picture != null)
		      		<label class="text-center">Foto Promocional Actual</label>
		      		<img src="{{Storage::url($event[0]->e_picture)}}" class="img-responsve img-thumbnail center-block" style="height:100px;">
	      			<hr>
	      		@endif
	      		<div class="col-md-12">
	      			<label>Foto Promocional: * - (300 x 300)</label>
				    <input type="file" name="promotionalpicture" id="promotionalpicture" class="form-control">
	      		</div>
	      	</div>
	      	<hr>
	      	<div class="div-btns-principal">
				<button type="submit" class="btn btn-primary btn-principal pull-right"><i class="fa fa-edit"></i> Editar Evento</button>
			</div>
			<input type="hidden" name="e_id" value="{{$event[0]->e_id}}" class="form-control">
      	</form>

<script type="text/javascript">
	var map;
	var markers = [];
	var geocoder;        
	function initMap() {
		geocoder = new google.maps.Geocoder();
	    var position = { lat: {!! $event[0]['e_latitud'] !!} , lng: {!! $event[0]['e_longitud'] !!} };

        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: position,
        });

        var marker = new google.maps.Marker({
          position: position,
          map: map
        });

        markers.push(marker);

	    // This event listener will call addMarker() when the map is clicked.
	    map.addListener('click', function(event) {
	      addMarker(event.latLng);
	      $('#latitud').val(event.latLng.lat());
	      $('#longitud').val(event.latLng.lng());
	      $( ".guardar" ).each(function() {
			  $( this ).removeAttr( "disabled" );
		  });
	    });

	}

	function addMarker(location) {
		deleteMarkers();
	    var marker = new google.maps.Marker({
	      position: location,
	      map: map
	    });
	    markers.push(marker);
	    $('#address').val('');
	}

	// Sets the map on all markers in the array.
	  function setMapOnAll(map) {
	    for (var i = 0; i < markers.length; i++) {
	      markers[i].setMap(map);
	    }
	  }

	  // Removes the markers from the map, but keeps them in the array.
	  function clearMarkers() {
	    setMapOnAll(null);
	  }

	  // Shows any markers currently in the array.
	  function showMarkers() {
	    setMapOnAll(map);
	  }

	  // Deletes all markers in the array by removing references to them.
	  function deleteMarkers() {
	    clearMarkers();
	    markers = [];
	  }

	  function codeAddress() {
	  	deleteMarkers();
	    var address = document.getElementById('address').value;
	    geocoder.geocode( { 'address': address}, function(results, status) {
	      if (status == 'OK') {
	        map.setCenter(results[0].geometry.location);
	        var marker = new google.maps.Marker({
	            map: map,
	            position: results[0].geometry.location
	        });
	        markers.push(marker);
	        $('#latitud').val(results[0].geometry.location.lat());
	      	$('#longitud').val(results[0].geometry.location.lng());
	      	$('#address').val('');
	      } else {
	        alert('Geocode was not successful for the following reason: ' + status);
	      }
	    });
	  }

      $('#searchAddress').click(function(){
      	codeAddress();
      	$( ".guardar" ).each(function() {
			$( this ).removeAttr( "disabled" );
		});
      });

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg7sqXHUdMNHLt7uhiJUcP0nV5nHylsfo&callback=initMap"
async defer></script>

<script type="text/javascript">
	var Si='Si';
	var No='No';
	if( {!!$event[0]->e_minimum_age!!} != null){
		$("input[name=redad][value='Si']").attr("checked","checked");
		$("input[name=redad][value='No']").removeAttr("checked");
	}


	if( {!!$event[0]->e_guest_member_question!!} == 'Si'){
		$("input[name=invxmiembros][value='Si']").attr("checked","checked");
		$("input[name=invxmiembros][value='No']").removeAttr("checked");
	}

	if($("input[name=redad]:checked").val() == 'Si'){
		$('#minimumage').removeAttr('disabled');
	}

	if($("input[name=invxmiembros]:checked").val() == 'Si'){
		$('#numberGuestXMember').removeAttr('disabled');
		$('#membersguests').removeAttr('disabled');
		$('#sexsmembers').removeAttr('disabled');
		$('#citiesmembers').removeAttr('disabled');
		$('#minimumagemembers').removeAttr('disabled');
	}

	$('#eventType').val({!!$event[0]->event_type_et_id!!});
	$('#eventCategorie').val({!!$event[0]->event_categorie_ec_id!!});
</script>
@endsection