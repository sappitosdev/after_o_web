@extends('layouts.employee')
@section('content')
	<h4>Administrar Socios</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Socios</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newPartner"><i class="fa fa-plus"></i> Nuevo Socio</button>
		<button class="btn btn-success" data-toggle="modal" data-target="#importExcel"><i class="fa fa-edit"></i> Importar Excel</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Socio</th>
				<th class="text-center">Fecha de Nacimiento</th>
				<th class="text-center">Sexo</th>
				<th class="text-center">Correo Electronico</th>
				<th class="text-center">Membresía</th>
				<th class="text-center">Editar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($partners as $partner)
				<tr class="text-center">
					<td>
						{{$partner->p_name}} {{$partner->p_lastname}} - V-{{$partner->p_identification}}
					</td>
					<td>
						{{$partner->p_date_of_birth}}
					</td>
					<td class="text-capitalize">
						{{$partner->p_sex}}
					</td>
					<td>
						{{$partner->p_email}}
					</td>
					<td>
                        {{$partner->m_name}}
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editPartner" data-whatever="{{$partner}}">Editar</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$partners->links()}}

<div class="modal fade" id="newPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    	<form action="{{action('EmployeeController@addPartner')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Crear Nuevo Socio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cedula: <span class="required">*</span></label>
                        <input type="text" name="identification" class="form-control" required pattern="[0-9]+" maxlength="8">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nombre(s): <span class="required">*</span></label>
                        <input type="text" name="name" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Apellido(s): <span class="required">*</span></label>
                        <input type="text" name="lastname" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Sexo: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="sex" required>
                            <option selected disabled value="">Selecciona un sexo</option>
                            @foreach($sexs as $sex)
                                <option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Fecha de nacimiento: <span class="required">*</span></label>
                        <input type="text" name="dateofbirth" class="form-control datepicker" required id="date1">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Correo Electronico: <span class="required">*</span></label>
                        <input type="email" name="email" class="form-control" required>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ciudad: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="citie" id="citie" required>
                            <option selected disabled value="">Selecciona una ciudad</option>
                            @foreach($cities as $citie)
                                <option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group hidden" id="writeCitie">
                        <label>Escriba la Ciudad <span class="required">*</span></label>
                        <input type="text" name="othercitie" class="form-control text-capitalize">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Estado Civil: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="civilstatus" required>
                            <option selected disabled value="">Selecciona un estado civil</option>
                            @foreach($civil_status as $cs)
                                <option value="{{$cs->ct_concept}}">{{$cs->ct_concept}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Telefono: <span class="required">*</span></label>
                        <input type="text" name="telephone" class="form-control" maxlength="11" pattern="[0-9]+">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Colegio:</label>
                        <input type="text" name="school" class="form-control text-capitalize">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group text-center">
                        <label>Carrera Universitaria? </label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="universidad"  value="Si">Si
                          </label>
                          <label>
                            <input type="radio" name="universidad" value="No" checked>No
                          </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Universidad </label>
                        <select name="university" id="university" class="form-control text-capitalize" disabled>
                            <option selected disabled value="">Selecciona una universidad</option>
                            @foreach($universities as $universitie)
                                <option value="{{$universitie->u_id}}">{{$universitie->u_universitie}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group hidden" id="writeUniversitie">
                        <label>Escriba la Universidad <span class="required">*</span></label>
                        <input type="text" name="otheruniversitie" class="form-control text-capitalize">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Profesión </label>
                        <input type="text" name="profession" id="profession" class="form-control text-capitalize" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group text-center">
                        <label>Trabaja? </label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="trabaja" value="Si">Si
                          </label>
                          <label>
                            <input type="radio" name="trabaja" value="No" checked>No
                          </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Empresa </label>
                        <input type="text" name="company" id="company" class="form-control text-capitalize" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cargo </label>
                        <input type="text" name="position" id="position" class="form-control text-capitalize" disabled>
                    </div>
                </div>
            </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('EmployeeController@addPatnerToExcel')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Importar Excel de Socios</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	      		<label>Archivo Excel:</label>
	      		<input type="file" name="excel" class="form-control">
	      	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-success">Importar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="searchRecommenders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form action="{{action('EmployeeController@addPatnerToExcel')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Socios que Recomendaron</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div id="searchRecommendersTable"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form action="{{action('EmployeeController@editPartner')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Editar Socio</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cedula: <span class="required">*</span></label>
                        <input type="text" name="identification" id="identification" class="form-control" required pattern="[0-9]+" maxlength="8">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nombre(s): <span class="required">*</span></label>
                        <input type="text" name="name" id="name" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Apellido(s): <span class="required">*</span></label>
                        <input type="text" name="lastname" id="lastname" class="form-control text-capitalize" required pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Sexo: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="sex" id="sex"required>
                            <option selected disabled value="">Selecciona un sexo</option>
                            @foreach($sexs as $sex)
                                <option value="{{$sex->ct_concept}}">{{$sex->ct_concept}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Fecha de nacimiento: <span class="required">*</span></label>
                        <input type="text" name="dateofbirth" class="form-control datepicker" required id="dateofbirth">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Correo Electronico: <span class="required">*</span></label>
                        <input type="email" name="email" id="email" class="form-control" required>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ciudad: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="citieEdit" id="citieEdit"  required>
                            <option selected disabled value="">Selecciona una ciudad</option>
                            @foreach($cities as $citie)
                                <option value="{{$citie->c_id}}">{{$citie->c_citie}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group hidden" id="writeCitieEdit">
                        <label>Escriba la Ciudad <span class="required">*</span></label>
                        <input type="text" name="othercitie" id="othercitie" class="form-control text-capitalize">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Estado Civil: <span class="required">*</span></label>
                        <select class="form-control text-capitalize" name="civilstatus" id="civilstatus" required>
                            <option selected disabled value="">Selecciona un estado civil</option>
                            @foreach($civil_status as $cs)
                                <option value="{{$cs->ct_concept}}">{{$cs->ct_concept}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Telefono: <span class="required">*</span></label>
                        <input type="text" name="telephone" id="telephone" class="form-control" maxlength="11" pattern="[0-9]+">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Colegio:</label>
                        <input type="text" name="school" id="school" class="form-control text-capitalize">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group text-center">
                        <label>Carrera Universitaria? </label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="universidad"  value="Si">Si
                          </label>
                          <label>
                            <input type="radio" name="universidad" value="No" checked>No
                          </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Universidad </label>
                        <select name="universityEdit" id="universityEdit" class="form-control text-capitalize" disabled>
                            <option selected disabled value="">Selecciona una universidad</option>
                            @foreach($universities as $universitie)
                                <option value="{{$universitie->u_id}}">{{$universitie->u_universitie}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group hidden" id="writeUniversitieEdit">
                        <label>Escriba la Universidad <span class="required">*</span></label>
                        <input type="text" name="otheruniversitie" id="otheruniversitie" class="form-control text-capitalize">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Profesión </label>
                        <input type="text" name="profession" id="professionEdit" class="form-control text-capitalize" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group text-center">
                        <label>Trabaja? </label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="trabaja" value="Si">Si
                          </label>
                          <label>
                            <input type="radio" name="trabaja" value="No" checked>No
                          </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Empresa </label>
                        <input type="text" name="company" id="companyEdit" class="form-control text-capitalize" disabled>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Cargo </label>
                        <input type="text" name="position" id="positionEdit" class="form-control text-capitalize" disabled>
                    </div>
                </div>
                <input type="hidden" name="p_id" id="p_id">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
          </div>
        </form>
    </div>
  </div>
</div>

@endsection