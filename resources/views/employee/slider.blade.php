@extends('layouts.employee')
@section('content')
	<h4>Administrar Slider Principal</h4>
	<hr>
	<ol class="breadcrumb">
	  <li><a href="#">Admin</a></li>
	  <li><a href="#">Administrar</a></li>
	  <li class="active">Slider</li>
	</ol>
	<div class="div-btns-principal">
		<button class="btn btn-primary btn-principal" data-toggle="modal" data-target="#newBanner"><i class="fa fa-plus"></i> Nuevo Banner</button>
	</div>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th class="text-center">Banner</th>
				<th class="text-center">Editar</th>
				<th class="text-center">Remover</th>
			</tr>
		</thead>
		<tbody>
			@foreach($sliders as $slider)
				<tr class="text-center">
					<td>
						<img src="{{Storage::url($slider->cp_content)}}" class="img-responsive img-thumbnail" style="height:80px;">
					</td>
					<td>
						<button class="btn btn-primary btn-secundario" data-toggle="modal" data-target="#editBanner" data-whatever="{{$slider}}">Editar</button>
					</td>
					<td>
						<button class="btn btn-danger" data-toggle="modal" data-target="#deleteBanner" data-whatever="{{$slider->cp_id}}">Remover</button>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{$sliders->links()}}

<div class="modal fade" id="newBanner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('EmployeeController@addBanner')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Agregar Nuevo Banner</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label>Title:</label>
	        	<input type="text" name="title" id="title" class="form-control">
	        </div>
	        <div class="form-group">
	        	<label>Text:</label>
	        	<textarea class="form-control" name="text" id="text"></textarea>
	        </div>
	      	<div class="form-group">
	        	<label>Imagen: * - (1600 x 700)</label>
	        	<input type="file" name="banner" id="banner" class="form-control" required>
	        </div>
	        <img src="" id="preview" class="img-responsive img-thumbnail text-center center-block">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editBanner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<form action="{{action('EmployeeController@editBanner')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Banner</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label class="text-center center-block">Banner Actual:</label>
	        	<img src="" id="banneractual" class="img-responsive img-thumbnail text-center center-block" style="height:150px;">
	        </div>
	        <div class="form-group">
	        	<label>Title:</label>
	        	<input type="text" name="title" id="title" class="form-control">
	        </div>
	        <div class="form-group">
	        	<label>Text:</label>
	        	<textarea class="form-control" name="text" id="text"></textarea>
	        </div>
	      	<div class="form-group">
	        	<label>Imagen: * - (1600 x 700)</label>
	        	<input type="file" name="banner" id="banner2" class="form-control">
	        </div>
	        <img src="" id="preview2" class="img-responsive img-thumbnail text-center center-block" style="height:150px;">
	        <input type="hidden" name="cp_id" id="cp_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteBanner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<form action="{{action('EmployeeController@deleteBanner')}}" method="POST" enctype="multipart/form-data">
    		{{ csrf_field() }}
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel">Editar Banner</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	      	<h5><strong>Desea eliminar el banner seleccionado?</strong></h5>
	        <input type="hidden" name="cp_id" id="cp_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary btn-principal">Guardar</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
@endsection