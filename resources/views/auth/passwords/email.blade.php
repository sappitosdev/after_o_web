<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AFTER O</title>

        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="display: -webkit-box;">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="text-center">
                        <img class="logo" src="{{asset('img/logo.png')}}" class="img-responsive">
                    </div>

                    <h2 class="text-center">Recuperar contraseña</h2>

                    <h5 class="text-center"><a href="{{route('membersregistration')}}">Crear una cuenta</a></h5>

                    <hr>

                    <div class="col-md-10 col-md-offset-1">
                        <form  method="POST" action="{{ action('ResetPasswordController@reset') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">Correo Electronico</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-principal btn-login">RECUPERAR CONTRASEÑA</button>
                                    <a href="{{route('public')}}" class="btn btn-primary btn-lg btn-secundario btn-login">INFORMACIÓN PARA TERCEROS</a>
                        </form>
                    </div>
                    <div class="text-center login-footer">
                        <p class="text-muted">AFTER O © 2018</p>
                    </div>
                </div>
                <div class="hidden-xs col-sm-6 col-md-8 col-lg-8 background-login" style="background-image: url( {{asset('img/background.jpg')}} );"></div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('onepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    </body>
</html>

