<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AFTER O</title>

        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="display: -webkit-box;">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                    <div class="text-center">
                        <img class="logo" src="{{asset('img/logo.png')}}" class="img-responsive" style="margin-top:120px">
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <h3 class="text-center text-uppercase"><strong>Usted no posee los permisos necesarios para acceder a esta acción</strong></h3>
                            <a href="/" class="btn btn-lg btn-login">VOLVER</a>
                        </div>
                    </div>

                    <div class="text-center login-footer">
                        <p class="text-muted">AFTER O © 2018</p>
                    </div>
                </div>
                <div class="hidden-xs col-sm-6 col-md-8 col-lg-8 background-login">
                </div>
            </div>
        </div>
    </body>
</html>

